#define _DEFAULT_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <fontconfig/fontconfig.h>
#include <utf.h>
#include <X11/Xlib.h>
#include <X11/Xft/Xft.h>

#include "def.h"
#include "text.h"
#include "otf.h"

void
die(const char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}
	
	abort();
}

static Text*
get_file(int fd)
{
	Text *txt;
	char buf[8*1024];
	int rd;
	txt = text_new();
	for (;;) {
		rd = read(fd, buf, sizeof(buf));
		if (rd == 0)
			return txt;
		else if (rd < 0)
			return NULL;
		text_append(txt, buf, rd);
	}
}

static size_t
strtorunes(OTGlyph *rs, size_t bn, const char *str, size_t len)
{
	size_t i, ri;
	int n;
	i = ri = 0;
	while (i < len && ri < bn) {
		n = charntorune(rs + ri, str + i, len - i);
		if (n == 0)
			break;
		i += n;
		ri++;
	}
	return ri;
}

static XftFont*
font_load_single(Display *disp, FcPattern *p)
{
	XftPattern *match;
	XftResult r;
	FcConfigSubstitute(NULL, p, FcMatchPattern);
	FcDefaultSubstitute(p);
	match = XftFontMatch(disp, XDefaultScreen(disp), p, &r);
	if (r != XftResultMatch)
		return NULL;
	return XftFontOpenPattern(disp, match);
}

int
main(int argc, char *argv[])
{
	Display *disp;
	Text *f;
	FcPattern *p;
	char *fmt;
	OTGlyph *gs;
	XftFont *font;
	OTFont otf;
	struct timespec t1, t2;
	double gps;
	size_t len;
	int fd;
	
	p = FcNameParse((const FcChar8*) argv[1]);
	if (!p)
		die("invalid font name: %s\n", argv[1]);
	disp = XOpenDisplay(NULL);
	if (!disp)
		die("can't open X display\n");
	font = font_load_single(disp, p);
	XCloseDisplay(disp);
	FcPatternGetString(font->pattern, FC_FONTFORMAT, 0, (FcChar8**) &fmt);
	if (strcmp(fmt, "TrueType") != 0)
		die("%s: not an OpenType font.\n", argv[1]);
	
	otf = OT_load_FcPattern(font->pattern);
	if (otf.error == OT_SYSTEM)
		die("%s\n", strerror(otf.sys_error));
	if (otf.error)
		die("%s: %s\n", argv[1], OT_strerror(otf.error));
	if (!otf.loaded)
		die("%s: font not loaded\n", argv[1]);
	
	fd = open(argv[2], O_RDONLY);
	if (fd < 0) {
		printf("%s: %s", argv[2], strerror(errno));
		return 1;
	}
	
	f = get_file(fd);
	if (!f)
		die("%s:", argv[2]);
	
	len = text_len(f);
	gs = malloc(len * 2 * sizeof(gs[0]));
	if (!gs)
		die("out of memory\n");
	len = strtorunes(gs, len, text_all(f), len);

	clock_gettime(CLOCK_MONOTONIC, &t1);
	OT_map_glyphs(&otf, gs, len*2, len);
	clock_gettime(CLOCK_MONOTONIC, &t2);
	
	gps = len / 1e6;
	gps /= (t2.tv_sec - t1.tv_sec) * 1.0 + (t2.tv_nsec - t1.tv_nsec) * 1e-9;
	printf("%s: %f Mglyphs/s\n", argv[1], gps);
	return 0;
}
