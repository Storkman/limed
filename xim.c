#define _DEFAULT_SOURCE
#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xresource.h>
#include <X11/Xutil.h>

#include "def.h"
#include "xim.h"

static Display *disp;
static XIM xim;
static bool preedit_ok;
static bool want_preedit = false;

void
xim_deinit(Xim *ctx)
{
	if (ctx->ic)
		XDestroyIC(ctx->ic);
	if (ctx->icattr)
		XFree(ctx->icattr);
}

void
xim_focus_in(Xim *ctx)
{
	if (ctx->ic)
		XSetICFocus(ctx->ic);
}

void
xim_focus_out(Xim *ctx)
{
	if (ctx->ic)
		XUnsetICFocus(ctx->ic);
}

static int
_edit_draw(XIC ic, XPointer ctx, XPointer call)
{
	/* Xim *x = ctx; */
	XIMPreeditDrawCallbackStruct *d = (XIMPreeditDrawCallbackStruct*) call;
	if (!d->text) {
		printf("del %d:%d\n", d->chg_first, d->chg_first+d->chg_length);
		return 0;
	}
	assert(!d->text->encoding_is_wchar);
	printf("ins %d+%d ", d->chg_first, d->chg_length);
	fwrite(d->text->string.multi_byte, 1, d->text->length, stdout);
	printf("\n");
	return 0;
}

static int
_edit_caret(XIC ic, XPointer ctx, XPointer call)
{
	return 0;
}

static int
_edit_done(XIC ic, XPointer ctx, XPointer call)
{
	fprintf(stderr, "start editing\n");
	return 0;
}

static int
_edit_start(XIC ic, XPointer ctx, XPointer call)
{
	fprintf(stderr, "done editing\n");
	return -1;
}

static int
_ic_destroyed(XIC ic, XPointer ctx, XPointer call)
{
	((Xim*) ctx)->ic = NULL;
	return 0;
}

static XIC
_ic_create(Xim *ctx)
{
	XICCallback cdestroy = {(XPointer) ctx, _ic_destroyed};
	XIMStyle style = (preedit_ok && want_preedit) ? XIMPreeditCallbacks : XIMPreeditNothing;
	return XCreateIC(xim,
		XNInputStyle, style | XIMStatusNothing,
		XNClientWindow, ctx->win,
		XNPreeditAttributes, ctx->icattr,
		XNDestroyCallback, &cdestroy,
		NULL);
}

int
xim_init(Xim *ctx, Window w)
{
	int i;
	if (!xim) {
		fprintf(stderr, "xim_init: XIM not open\n");
		return 1;
	}
	memset(ctx, 0, sizeof(*ctx));
	ctx->win = w;
	if (!preedit_ok || !want_preedit) {
		ctx->icattr = XVaCreateNestedList(0,
			XNSpotLocation, &ctx->spot,
			NULL);
	} else {
		for (i = 0; i < SIZE(ctx->cb); i++)
			ctx->cb[i].client_data = (XPointer) ctx;
		ctx->cb[0].callback = _edit_start;
		ctx->cb[1].callback = _edit_draw;
		ctx->cb[2].callback = _edit_caret;
		ctx->cb[3].callback = _edit_done;
		ctx->icattr = XVaCreateNestedList(0,
			XNPreeditStartCallback, ctx->cb+0,
			XNPreeditDrawCallback, ctx->cb+1,
			XNPreeditCaretCallback, ctx->cb+2,
			XNPreeditDoneCallback, ctx->cb+3,
			XNSpotLocation, &ctx->spot,
			NULL);
	}
	if (!ctx->icattr) {
		fprintf(stderr, "xim_init: out of memory\n");
		return 1;
	}
	ctx->ic = _ic_create(ctx);
	if (!ctx->ic) {
		fprintf(stderr, "xim_init: couldn't create XIM input context\n");
		return 1;
	}
	return 0;
}

int
xim_key(Xim *ctx, XKeyEvent *ev, char *buf, size_t cap, KeySym *ks)
{
	Status status;
	int len;
	if (!ctx || !ctx->ic)
		return XLookupString(ev, buf, cap, ks, NULL);
	*ks = NoSymbol;
	if (ev->type != KeyPress)
		return 0;
	len = Xutf8LookupString(ctx->ic, ev, buf, cap, ks, &status);
	if (status == XBufferOverflow)
		return 0;
	return len;
}

bool
xim_need_cursor(Xim *x)
{
	return x && x->ic;
}

void
xim_open(Display *display, char *name, char *class)
{
	XIMStyles *styles;
	int i;
	bool position_ok;
	disp = display;
	xim = XOpenIM(disp, XrmGetDatabase(disp), name, class);
	if (!xim) {
		fprintf(stderr, "couldn't open XIM\n");
		return;
	}
	
	if (XGetIMValues(xim, XNQueryInputStyle, &styles, NULL)) {
		fprintf(stderr, "xim_open: failed to query valid input styles\n");
		XCloseIM(xim);
		xim = NULL;
		return;
	}
	
	for (i = 0; i < styles->count_styles; i++) {
		switch (styles->supported_styles[i]) {
		case XIMPreeditCallbacks | XIMStatusNothing:
			preedit_ok = true;
			break;
		case XIMPreeditPosition | XIMStatusNothing:
			position_ok = true;
			break;
		}
	}
	XFree(styles);
	if (!position_ok && !preedit_ok) {
		fprintf(stderr, "xim_open: IM doesn't support any suitable input styles.\n");
		XCloseIM(xim);
		xim = NULL;
	}
}

void
xim_set_cursor(Xim *ctx, int x, int y)
{
	ctx->spot.x = x;
	ctx->spot.y = y;
	XSetICValues(ctx->ic, XNPreeditAttributes, ctx->icattr, NULL);
}
