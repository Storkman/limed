#include <assert.h>
#include <errno.h>
#include <setjmp.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <fontconfig/fontconfig.h>

#include "otf.h"

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

#define SIZE(X)	(sizeof(X)/sizeof((X)[0]))

#define TABLE_cmap ((u32) 0x636d6170)
#define TABLE_GSUB ((u32) 0x47535542)
#define TABLE_name ((u32) 0x6e616d65)

#define SIGNATURE_TTF ((u32) 0x00010000)
#define SIGNATURE_OTF ((u32) 0x4F54544F)
#define SIGNATURE_TTC ((u32) 0x74746366)

static const char *error_desc[] = {
	[OT_OK]	=	"(no error)",
	[OT_ENCODING]	=	"Font name uses an unsupported encoding.",
	[OT_INTERNAL]	=	"Internal error. This is a bug in the OTF parser.",
	[OT_INVALID]	=	"Table field has an invalid value.",
	[OT_LONG_STRING]	=	"String too long.",
	[OT_NAME]	=	"File is a collection but no font name was given.",
	[OT_NOMEM]	=	"Out of memory.",
	[OT_NOTFOUND]	=	"Font file not found.",
	[OT_OFFSET]	=	"Offset out of bounds.",
	[OT_SIGNATURE]	= 	"Unrecognized file signature. Not a TTF/OTF file.",
	[OT_SYSTEM]	=	"System error; check sys_error for errno.",
	[OT_UNNAMED]	=	"Font does not contain a name entry.",
	[OT_VERSION]	=	"Unsupported data structure version.",
};

typedef struct CMap CMap;
typedef struct Coverage Coverage;
typedef struct Feature Feature;
typedef struct FontFile FontFile;
typedef struct FontInternal FontInternal;
typedef struct GlyphPage GlyphPage;
typedef struct GlyphRange GlyphRange;
typedef struct GlyphSeq GlyphSeq;
typedef struct Lookup Lookup;
typedef struct LookupMulti LookupMulti;
typedef struct LookupSingle LookupSingle;
typedef struct MemChunk MemChunk;
typedef union Subtable Subtable;
typedef struct Tables Tables;

struct GlyphPage {
	u32 glyphs[256];
};

struct CMap {
	GlyphPage	*pages[0x1FF + 1];
};

/* Coverage encodes an ordered list of input glyphs for a lookup,
 * either as a vector of glyph IDs or as a vector of glyph ranges.
 * Lookups reference valid input glyphs by their Coverage indices. */
struct Coverage {
	u16	count;
	u16	*glyphs;
	GlyphRange	*ranges;
};

struct Feature {
	char	name[5];
	bool	enabled;
	u16	selection;
	u16	nlookups;
	Lookup	*lookups[2];
	Lookup	**more_lookups;
};

struct FontFile {
	const u8	*data;
	off_t	pos, len;
	
	const char *full_name;
	
	jmp_buf *jmpbuf;
	int error, sys_error;
};

struct GlyphRange {
	u16	first, last;
	u16	first_idx;
};

struct GlyphSeq {
	u16	count;
	u16	glyphs[3];
	u16	*more_glyphs;
};

struct FontInternal {
	MemChunk	*mem;
	CMap	*cmap;
	
	Feature *ccmp;
	
	u16	nfeatures;
	Feature	*features;
};

enum {
	LT_UNSUPPORTED = -1,
	LT_SINGLE = 1,
	LT_MULTIPLE = 2,
	LT_ALTERNATE = 3,
	LT_LIGATURE = 4,
	LT_CONTEXT = 5,
	LT_CHAIN_CTX = 6,
	LT_EXTENSION = 7,
	LT_REV_CHAIN = 8,
};

struct LookupMulti {
	Coverage	from;
	
	u16	count;
	GlyphSeq	*subst;
};

struct LookupSingle {
	Coverage	from;
	i16	delta;	/* Added to each input glyph ID. Set to 0 if using a vector of substitute glyphs. */
	
	u16	count;	/* set to 0 if using delta */
	u16	*subst;	/* vector of substitute glyphs, indexed by Coverage index */
};

struct Lookup {
	i8	type;
	u16	nsubt;
	Subtable	*subt;
};

struct MemChunk {
	MemChunk *next;
	size_t len;
};

union Subtable {
	LookupSingle	single;
	LookupMulti	multi;
};

struct Tables {
	bool	name_found, name_matched;
	off_t	gsub_at;
	off_t	cmap_at;
	
	Lookup	*lookups;
	u16	nlookups;
};

static void*	allocate(FontFile*, MemChunk*, size_t);
static MemChunk*	allocate_first(void);
static void	check_len(FontFile*, off_t len);
static int	coverage_match(Coverage*, u32);
static void	destroy_all(MemChunk*);
static void	enable_features(FontInternal*, OTFeature*);
static FontFile	map_file(const char*);

static int	map_glyphs(Feature*, OTGlyph*, int buflen, int len);
static OTGlyph	map_lookup_alternate(OTGlyph, LookupMulti*, u16 sel);
static int	map_lookup_multi(OTGlyph *out, int cap, OTGlyph in, LookupMulti*);
static OTGlyph	map_lookup_single(OTGlyph, LookupSingle*);
static void	map_lookups_alternate(OTGlyph*, int, Subtable*, u16 ns, u16 sel);
static void	map_lookups_multi(OTGlyph*, int cap, int len, Subtable*, u16 ns);
static void	map_lookups_single(OTGlyph*, int, Subtable*, u16 ns);

static void	raise_error(FontFile*, int err);

static void	read_coverage(FontFile*, MemChunk*, Coverage*);
static void	read_file(FontFile, OTFont*);
static void	read_gsub_table(FontFile*, MemChunk*, OTFont*, Tables*);
static void	read_gsub_features(FontFile*, MemChunk*, FontInternal*, Tables*);
static void	read_gsub_lookup(FontFile*, MemChunk*, Lookup*);
static void	read_gsub_lookups(FontFile*, MemChunk*, OTFont*, Tables*);
static void	read_header_collection(FontFile*, OTFont*);
static void	read_header_ttf(FontFile*, OTFont*);

static i16	read_i16(FontFile*);

static int	read_name_record(FontFile*, FontFile *strings, char *full_name, size_t buflen);
static void	read_string(FontFile*, off_t str_offset, size_t str_len, char *buf, size_t buf_len);
static void	read_subtable_multi(FontFile*, MemChunk*, LookupMulti*);
static void	read_subtable_single(FontFile*, MemChunk*, LookupSingle*);
static void	read_table(FontFile*, MemChunk*, OTFont*, Tables*);
static void	read_table_cmap(FontFile*, MemChunk*, CMap*);
static void	read_table_name(FontFile*, MemChunk*, OTFont*, Tables*);

static void	read_tag(FontFile*, char[5]);
static u16	read_u16(FontFile*);
static u16	read_u16_at(FontFile*, off_t);
static u32	read_u32(FontFile*);
static u32	read_u32_at(FontFile*, off_t);
static void	seek_to(FontFile*, off_t);
static void	set_glyph_map(FontFile*, MemChunk*, CMap*, u32 i, u32 g);
static void	skip(FontFile*, off_t);
static void	unmap_file(FontFile);

static OTFont font_cache[16];

static GlyphPage null_page = {{0,}};

void*
allocate(FontFile *F, MemChunk *m, size_t s)
{
	MemChunk *new;
	void *p = malloc(sizeof(*new) + s);
	if (!p)
		raise_error(F, OT_NOMEM);
	new = p;
	new->len = s;
	p = p + sizeof(*new);
	if (!m) {
		new->next = NULL;
		return p;
	}
	new->next = m->next;
	m->next = new;
	return p;
}

MemChunk*
allocate_first(void)
{
	MemChunk *m = malloc(sizeof(*m));
	if (!m)
		return NULL;
	memset(m, 0, sizeof(*m));
	return m;
}

void
check_len(FontFile *f, off_t len)
{
	if (f->pos + len >= f->len)
		raise_error(f, OT_OFFSET);
}

int
coverage_match(Coverage *c, u32 g)
{
	GlyphRange *r;
	u16 i;
	
	if (c->glyphs) {
		for (i = 0; i < c->count; i++) {
			if (c->glyphs[i] > g)
				return -1;
			if (c->glyphs[i] == g)
				return i;
		}
		return -1;
	}
	
	for (r = c->ranges; r < c->ranges + c->count; r++) {
		if (g > r->last)
			return -1;
		if (g >= r->first)
			return r->first_idx + g - r->first;
	}
	return -1;
}

void
destroy_all(MemChunk *root)
{
	MemChunk *m, *next;
	m = root;
	while (m) {
		next = m->next;
		free(m);
		m = next;
	}
}

void
enable_features(FontInternal *f, OTFeature *fs)
{
	Feature *af;
	OTFeature *ef;
	for (af = f->features; af < f->features + f->nfeatures; af++) {
		af->enabled = false;
		af->selection = 0;
		for (ef = fs; ef->name[0]; ef++) {
			if (memcmp(af->name, ef->name, 4) == 0) {
				af->enabled = true;
				af->selection = ef->alt > 0 ? ef->alt : 0;
				break;
			}
		}
	}
}

FontFile
map_file(const char *path)
{
	FontFile file = {};
	struct stat st;
	int fd;
	off_t len = 0;
	void *p = NULL;
	fd = open(path, O_RDONLY);
	if (fd < 0) {
		file.error = OT_SYSTEM;
		file.sys_error = errno;
		return file;
	}
	
	if (fstat(fd, &st) < 0) {
		file.error = OT_SYSTEM;
		file.sys_error = errno;
		close(fd);
		return file;
	}
	len = st.st_size;
	
	p = mmap(NULL, len, PROT_READ, MAP_SHARED, fd, 0);
	if (p == MAP_FAILED) {
		file.error = OT_SYSTEM;
		file.sys_error = errno;
		close(fd);
		return file;
	}
	close(fd);
	file.data = p;
	file.len = len;
	return file;
}

OTFeature
OT_feature_parse(const char *feat)
{
	char *end;
	long alt;
	OTFeature feature = {{0, 0, 0, 0}, 0};
	if (strlen(feat) == 4) {
		memmove(feature.name, feat, 4);
		return feature;
	}
	if (feat[4] != ' ')
		return feature;
	alt = strtol(feat + 5, &end, 10);
	if (*end)
		return feature;;
	if (alt < 0 || alt > USHRT_MAX)
		return feature;;
	memmove(feature.name, feat, 4);
	feature.alt = alt;
	return feature;
}

OTFont
OT_load(const char *path, const char *name, OTOptions *opts)
{
	OTFont font = {};
	OTFeature *feat;
	FontFile file;
	FontInternal *fi = {NULL};
	MemChunk *mem;
	jmp_buf jmpbuf;
	int i, err;
	
	if (!name)
		name = path;
	for (i = 0; i < SIZE(font_cache); i++) {
		if (!font_cache[i].full_name)
			break;
		if (strcmp(font_cache[i].full_name, name) == 0)
			return font_cache[i];
	}
	
	file = map_file(path);
	if (file.error) {
		font.error = file.error;
		font.sys_error = file.sys_error;
		return font;
	}
	font.full_name = name;
	if (opts && opts->features) {
		for (feat = opts->features; feat->name[0]; feat++) {
			char tag[5] = {};
			memmove(tag, feat->name, 4);
		}
	}
	
	mem = allocate_first();
	if (!mem) {
		unmap_file(file);
		font.error = OT_NOMEM;
		return font;
	}
	
	if ((err = setjmp(jmpbuf))) {
		unmap_file(file);
		destroy_all(mem);
		font.error = err;
		return font;
	}
	
	file.jmpbuf = &jmpbuf;
	font.internal = fi = allocate(&file, mem, sizeof(*fi));
	memset(fi, 0, sizeof(*fi));
	fi->mem = mem;
	
	read_file(file, &font);
	unmap_file(file);
	if (opts && opts->features)
		enable_features(fi, opts->features);
	
	if (i < SIZE(font_cache))
		font_cache[i] = font;
	return font;
}

OTFont
OT_load_FcPattern(FcPattern *pat)
{
	OTFeature features[256];
	OTOptions opt = {};
	OTFont errfont = {NULL};
	char *path = NULL, *name = NULL, *feat = NULL;
	int i;
	
	FcPatternGetString(pat, FC_FULLNAME, 0, (FcChar8**) &name);
	FcPatternGetString(pat, FC_FILE, 0, (FcChar8**) &path);
	if (!path) {
		errfont.error = OT_NOTFOUND;
		return errfont;
	}
	
	i = 0;
	while (i < SIZE(features) - 1) {
		if (FcPatternGetString(pat, FC_FONT_FEATURES, i, (FcChar8**) &feat))
			break;
		features[i] = OT_feature_parse(feat);
		if (!features[i].name[0])
			continue;
		i++;
	}
	memset(features[i].name, 0, 4);
	features[i].alt = 0;
	i++;
	
	opt.features = features;
	return OT_load(path, name, &opt);
}

int
OT_map_glyphs(OTFont *font, OTGlyph *gs, int cap, int len)
{
	FontInternal *f = font->internal;
	Feature *feat;
	OTGlyph r;
	int i;
	if (len > cap)
		return 0;
	for (i = 0; i < len; i++) {
		r = gs[i];
		if (r <= 0x1FFFF)
			gs[i] = f->cmap->pages[(r & 0x1FF00) >> 8]->glyphs[r & 0xFF];
		else {
			gs[i] = 0;
			continue;
		}
	}
	
	if (f->ccmp)
		len = map_glyphs(f->ccmp, gs, cap, len);
	for (feat = f->features; feat < f->features + f->nfeatures; feat++)
		if (feat->enabled)
			len = map_glyphs(feat, gs, cap, len);
	return len;
}

const char*
OT_strerror(int error)
{
	if (error < 0 || error >= SIZE(error_desc))
		return "Invalid error value.";
	return error_desc[error];
}

size_t
OT_total_mem(OTFont *font)
{
	FontInternal *f = font->internal;
	MemChunk *m;
	size_t total = 0;
	for (m = f->mem; m; m = m->next)
		total += m->len;
	return total;
}

void
OT_unload(OTFont *font)
{
	FontInternal *f = font->internal;
	destroy_all(f->mem);
}

int
map_glyphs(Feature *f, OTGlyph *gs, int buflen, int len)
{
	Lookup **ls, **l;
	if (f->nlookups <= SIZE(f->lookups))
		ls = f->lookups;
	else
		ls = f->more_lookups;
	for (l = ls; l < ls + f->nlookups; l++) {
		switch ((*l)->type) {
		case LT_SINGLE:
			map_lookups_single(gs, len, (*l)->subt, (*l)->nsubt);
			break;
		case LT_MULTIPLE:
			map_lookups_multi(gs, buflen, len, (*l)->subt, (*l)->nsubt);
			break;
		case LT_ALTERNATE:
			if (f->selection <= 0)
				break;
			map_lookups_alternate(gs, len, (*l)->subt, (*l)->nsubt, f->selection);
			break;
		default:
			return len;
		}
	}
	return len;
}

u32
map_lookup_alternate(u32 g, LookupMulti *m, u16 sel)
{
	GlyphSeq *s;
	int ci = coverage_match(&m->from, g);
	if (ci < 0 || ci >= m->count)
		return g;
	s = m->subst + ci;
	if (sel > s->count)
		return g;
	if (s->count <= SIZE(s->glyphs))
		return s->glyphs[sel - 1];
	return s->more_glyphs[sel - 1];
}

int
map_lookup_multi(OTGlyph *out, int cap, OTGlyph in, LookupMulti *m)
{
	GlyphSeq seq;
	int ci = coverage_match(&m->from, in);
	if (ci < 0 || ci >= m->count) {
		out[0] = in;
		return 1;
	}
	seq = m->subst[ci];
	if (seq.count <= SIZE(seq.glyphs))
		memmove(out, seq.glyphs, seq.count * sizeof(seq.glyphs[0]));
	else
		memmove(out, seq.more_glyphs, seq.count * sizeof(seq.more_glyphs[0]));
	return seq.count;
}

u32
map_lookup_single(OTGlyph g, LookupSingle *s)
{
	int ci = coverage_match(&s->from, g);
	if (ci < 0)
		return g;
	if (s->delta)
		return g + s->delta;
	if (ci >= s->count)
		return g;
	return s->subst[ci];
}

void
map_lookups_alternate(OTGlyph *gs, int len, Subtable *ss, u16 ns, u16 sel)
{
	Subtable *s;
	int i;
	for (s = ss; s < ss + ns; s++)
		for (i = 0; i < len; i++)
			gs[i] = map_lookup_alternate(gs[i], &s->multi, sel);
}

void
map_lookups_multi(OTGlyph *gs, int cap, int len, Subtable *ss, u16 ns)
{
	OTGlyph out[16];
	Subtable *s;
	int i, n;
	for (s = ss; s < ss + ns; s++) {
		i = 0;
		while (i < len) {
			n = map_lookup_multi(out, SIZE(out), gs[i], &s->multi);
			if (n > cap - i)
				return;
			if (n == 1)
				gs[i] = out[0];
			else {
				memmove(gs + i + n, gs + i + 1, (len - i - 1) * sizeof(*gs));
				memmove(gs + i, out, n * sizeof(*gs));
			}
			i += n;
			len += n - 1;
		}
	}
}

void
map_lookups_single(OTGlyph *gs, int len, Subtable *ss, u16 ns)
{
	Subtable *s;
	int i;
	for (s = ss; s < ss + ns; s++)
		for (i = 0; i < len; i++)
			gs[i] = map_lookup_single(gs[i], &s->single);
}

void
raise_error(FontFile *F, int err)
{
	longjmp(*F->jmpbuf, err);
}

void
read_coverage(FontFile *F, MemChunk *mem, Coverage *c)
{
	u16 *gids;
	GlyphRange *ranges;
	u16 fmt, len, i;
	
	fmt = read_u16(F);
	if (fmt != 1 && fmt != 2)
		raise_error(F, OT_VERSION);
	len = read_u16(F);
	
	if (fmt == 1) {
		gids = allocate(F, mem, len * sizeof(*gids));
		for (i = 0; i < len; i++)
			gids[i] = read_u16(F);
		c->count = len;
		c->glyphs = gids;
		c->ranges = NULL;
		return;
	}
	if (fmt == 2) {
		ranges = allocate(F, mem, len * sizeof(*ranges));
		for (i = 0; i < len; i++) {
			ranges[i].first = read_u16(F);
			ranges[i].last = read_u16(F);
			ranges[i].first_idx = read_u16(F);
		}
		c->count = len;
		c->glyphs = NULL;
		c->ranges = ranges;
		return;
	}
}

void
read_gsub_table(FontFile *F, MemChunk *mem, OTFont *font, Tables *tables)
{
	off_t gsub_off = F->pos;
	u16 vmajor, vminor;
	u16 feat_off, lookup_off;
	
	vmajor = read_u16(F);
	vminor = read_u16(F);
	if (vmajor != 1 || (vminor != 0 && vminor != 1))
		raise_error(F, OT_VERSION);
	
	skip(F, 2);
	feat_off = read_u16(F);
	lookup_off = read_u16(F);
	
	seek_to(F, gsub_off + lookup_off);
	read_gsub_lookups(F, mem, font, tables);
	
	seek_to(F, gsub_off + feat_off);
	read_gsub_features(F, mem, font->internal, tables);
}

void
read_gsub_features(FontFile *F, MemChunk *mem, FontInternal *font, Tables *tables)
{
	FontFile feat = *F;
	off_t feats_pos = F->pos;
	bool which = true;
	u16 nl, n, i, j;
	
	n = read_u16(F);
	font->nfeatures = n;
	font->features = allocate(F, mem, n * sizeof(font->features[0]));
	for (i = 0; i < n; i++) {
		Feature *feature = font->features + i;
		Lookup **ls;
		read_tag(F, feature->name);
		if (i > 0 && strcmp(feature->name, font->features[i-1].name) < 0)
			raise_error(F, OT_INVALID);
		seek_to(&feat, feats_pos + read_u16(F));
		
		skip(&feat, 2);
		nl = read_u16(&feat);
		feature->nlookups = nl;
		ls = feature->lookups;
		if (nl > SIZE(feature->lookups))
			feature->more_lookups = ls = allocate(F, mem, nl * sizeof(*ls));
		
		for (j = 0; j < nl; j++) {
			u16 idx = read_u16(&feat);
			if (idx >= tables->nlookups)
				raise_error(&feat, OT_OFFSET);
			ls[j] = tables->lookups + idx;
		}
		
		which = !which;
	}
}

static void
_lookup_unsupported(Lookup *l)
{
	l->type = LT_UNSUPPORTED;
	l->nsubt = 0;
	l->subt = NULL;
}

void
read_gsub_lookup(FontFile *F, MemChunk *mem, Lookup *lookup)
{
	FontFile S = *F;
	Subtable *subt;
	u16 ltype, flag, subn;
	u16 realtype = 0;
	off_t lookup_pos = F->pos;
	off_t subt_pos;
	int i;
	
	ltype = read_u16(F);
	flag = read_u16(F);
	subn = read_u16(F);
	
	if (flag)
		return _lookup_unsupported(lookup);
	
	subt = allocate(F, mem, subn * sizeof(*subt));
	
	for (i = 0; i < subn; i++) {
		subt_pos = lookup_pos + read_u16(F);
		seek_to(&S, subt_pos);
		if (ltype != LT_EXTENSION)
			realtype = ltype;
		else {
			u16 newtype;
			if (read_u16(&S) != 1)
				raise_error(&S, OT_VERSION);
			newtype = read_u16(&S);
			/* Extension subtables may not point to other extension subtables. */
			if (newtype == LT_EXTENSION)
				raise_error(&S, OT_INVALID);
			/* If a lookup table uses extension subtables, all subtables must have the same actual type. */
			if (i == 0)
				realtype = newtype;
			if (newtype != realtype)
				raise_error(&S, OT_INVALID);
			seek_to(&S, subt_pos + read_u32(&S));
		}
		
		switch (realtype) {
		case LT_SINGLE:
			read_subtable_single(&S, mem, &subt[i].single);
			break;
		case LT_MULTIPLE:
		case LT_ALTERNATE:
			read_subtable_multi(&S, mem, &subt[i].multi);
			break;
		case LT_LIGATURE:
		case LT_CONTEXT:
		case LT_CHAIN_CTX:
			return _lookup_unsupported(lookup);
		case LT_EXTENSION:
			raise_error(&S, OT_INTERNAL);
			break;
		case LT_REV_CHAIN:
			return _lookup_unsupported(lookup);
		default:
			raise_error(&S, OT_VERSION);
		}
	}
	lookup->type = realtype;
	lookup->nsubt = subn;
	lookup->subt = subt;
}

void
read_gsub_lookups(FontFile *F, MemChunk *mem, OTFont *font, Tables *tables)
{
	FontFile ltab = *F;
	off_t table_pos = F->pos;
	u16 n, i;
	n = read_u16(F);
	tables->nlookups = n;
	tables->lookups = allocate(F, mem, n * sizeof(tables->lookups[0]));
	for (i = 0; i < n; i++) {
		seek_to(&ltab, table_pos + read_u16(F));
		read_gsub_lookup(&ltab, mem, &tables->lookups[i]);
	}
}

void
read_header_collection(FontFile *F, OTFont *font)
{
	FontFile tdir = *F;
	
	u32 signature;
	u16 vmajor, vminor;
	u32 nfonts, i;
	
	signature = read_u32(F);
	if (signature != SIGNATURE_TTC)
		raise_error(F, OT_SIGNATURE);
	
	vmajor = read_u16(F);
	vminor = read_u16(F);
	if (vminor != 0 || (vmajor != 1 && vmajor != 2))
		raise_error(F, OT_VERSION);
	
	nfonts = read_u32(F);
	if (nfonts > 1 && !font->full_name)
		raise_error(F, OT_NAME);
	for (i = 0; i < nfonts; i++) {
		seek_to(&tdir, read_u32(F));
		
		read_header_ttf(&tdir, font);
		if (font->loaded)
			break;
	}
}

void
read_header_ttf(FontFile *F, OTFont *font)
{
	CMap *map;
	FontInternal *f = font->internal;
	Tables tables = {};
	u32 signature;
	u16 ntabs;
	int i;
	
	signature  = read_u32(F);
	if (signature != SIGNATURE_TTF && signature != SIGNATURE_OTF)
		raise_error(F, OT_SIGNATURE);
	ntabs = read_u16(F);
	skip(F, 6);
	
	for (i = 0; i < ntabs; i++) {
		read_table(F, f->mem, font, &tables);
		if (tables.name_found && !tables.name_matched)
			break;
	}
	if (!tables.name_matched)
		return;
	
	map = allocate(F, f->mem, sizeof(*map));
	f->cmap = map;
	for (i = 0; i < SIZE(map->pages); i++)
		map->pages[i] = &null_page;
	
	if (tables.cmap_at == 0)
		return;
	seek_to(F, tables.cmap_at);
	read_table_cmap(F, f->mem, map);
	
	if (tables.gsub_at == 0) {
		font->loaded = true;
		return;
	}
	seek_to(F, tables.gsub_at);
	read_gsub_table(F, f->mem, font, &tables);
	
	font->loaded = true;
}

i16
read_i16(FontFile *F)
{
	return (i16) read_u16(F);
}

void
read_file(FontFile F, OTFont *font)
{
	u32 signature = read_u32(&F);
	seek_to(&F, 0);
	switch (signature) {
	case SIGNATURE_TTF:
	case SIGNATURE_OTF:
		return read_header_ttf(&F, font);
	case SIGNATURE_TTC:
		return read_header_collection(&F, font);
	default:
		raise_error(&F, OT_SIGNATURE);
	}
}

#define NAME_ID_FULL 4

int
read_name_record(FontFile *F, FontFile *strings, char *full_name, size_t buflen)
{
	u16 id;
	u16 nlen, off;
	
	skip(F, 6);
	id = read_u16(F);
	nlen = read_u16(F);
	off = read_u16(F);
	
	if (id != NAME_ID_FULL)
		return 0;
	read_string(strings, off, nlen, full_name, buflen);
	return 1;
}

void
read_string(FontFile *F, off_t off, size_t slen, char *s, size_t buflen)
{
	skip(F, off);
	check_len(F, slen);
	if (slen + 1 > buflen)
		raise_error(F, OT_LONG_STRING);
	memmove(s, F->data + F->pos, slen);
	s[slen] = 0;
}

void
read_subtable_multi(FontFile *F, MemChunk *mem, LookupMulti *lookup)
{
	FontFile subt = *F;
	off_t table_pos = F->pos;
	u16 fmt, cover_off, n, sn, i, j;
	
	fmt = read_u16(F);
	if (fmt != 1)
		raise_error(F, OT_VERSION);
	cover_off = read_u16(F);
	
	n = read_u16(F);
	lookup->count = n;
	lookup->subst = allocate(F, mem, n * sizeof(lookup->subst[0]));
	for (i = 0; i < n; i++) {
		GlyphSeq *gs = &lookup->subst[i];
		
		seek_to(&subt, table_pos + read_u16(F));
		sn = read_u16(&subt);
		gs->count = sn;
		
		if (sn <= SIZE(gs->glyphs)) {
			for (j = 0; j < sn; j++)
				gs->glyphs[j] = read_u16(&subt);
		} else {
			gs->more_glyphs = allocate(&subt, mem, sn * sizeof(gs->more_glyphs[0]));
			for (j = 0; j < sn; j++)
				gs->more_glyphs[j] = read_u16(&subt);
		}
	}
	
	seek_to(F, table_pos + cover_off);
	read_coverage(F, mem, &lookup->from);
}

void
read_subtable_single(FontFile *F, MemChunk *mem, LookupSingle *lookup)
{
	off_t table_pos = F->pos;
	u16 fmt, cover_off, n, i;
	
	fmt = read_u16(F);
	if (fmt != 1 && fmt != 2)
		raise_error(F, OT_VERSION);
	cover_off = read_u16(F);
	
	if (fmt == 1) {
		lookup->delta = read_i16(F);
		lookup->count = 0;
		lookup->subst = NULL;
	} else if (fmt == 2) {
		n = read_u16(F);
		lookup->delta = 0;
		lookup->count = n;
		lookup->subst = allocate(F, mem, n * sizeof(lookup->subst[0]));
		for (i = 0; i < n; i++)
			lookup->subst[i] = read_u16(F);
	}
	
	seek_to(F, table_pos + cover_off);
	read_coverage(F, mem, &lookup->from);
}

void
read_table(FontFile *F, MemChunk *mem, OTFont *font, Tables *tables)
{
	FontFile table = *F;
	u32 tag;
	u32 off;
	
	tag = read_u32(F);
	skip(F, 4);	/* checksum */
	off = read_u32(F);
	skip(F, 4);	/* length */
	
	seek_to(&table, off);
	switch (tag) {
	case TABLE_cmap:
		tables->cmap_at = off;
		break;
	case TABLE_GSUB:
		tables->gsub_at = off;
		break;
	case TABLE_name:
		read_table_name(&table, mem, font, tables);
		break;
	default:
		break;
	}
}

static int
_rate_encoding(u16 plat, u16 enc)
{
	switch (plat) {
	case 0:
		if (enc > 0 && enc <= 4)
			return 10 + enc;
		else
			return 0;
	case 2:
		if (enc == 0)
			return 1;	/* ISO / ASCII */
		return 0;
	case 3:
		if (enc == 1)
			return 2;	/* Windows / Unicode BMP */
		else if (enc == 10)
			return 3;	/* Windows / Unicode Full */
		else
			return 0;
	default:
		return 0;
	}
}

static void
_map_delta_off(FontFile *subt, MemChunk *mem, CMap *cmap, u32 s, u32 e, i32 delta)
{
	u16 r;
	int i;
	for (i = s; i <= e; i++) {
		if (i > 0x1FFFF)
			break;
		r = read_u16(subt);
		if (!r)
			continue;
		set_glyph_map(subt, mem, cmap, i, r + delta);
	}
}

static void
_read_cmap_group4(FontFile *subt, MemChunk *mem, CMap *cmap,
	off_t sp, off_t ep, off_t dp, off_t rp)
{
	int i;
	u16 s, e, r;
	i16 d;
	s = read_u16_at(subt, sp);
	e = read_u16_at(subt, ep);
	d = read_u16_at(subt, dp);
	r = read_u16_at(subt, rp);
	if (r) {
		seek_to(subt, rp + r);
		_map_delta_off(subt, mem, cmap, s, e, d);
		return;
	}
	for (i = s; i <= e; i++)
		set_glyph_map(subt, mem, cmap, i, (u16) (i + d));
}

static void
_read_cmap_group12(FontFile *subt, MemChunk *mem, CMap *cmap)
{
	int i;
	u32 s, e, first;
	s = read_u32(subt);
	e = read_u32(subt);
	first = read_u32(subt);
	for (i = s; i <= e; i++) {
		if (i > 0x1FFFF)
			break;
		set_glyph_map(subt, mem, cmap, i, i - s + first);
	}
}

void
read_table_cmap(FontFile *F, MemChunk *mem, CMap *cmap)
{
	FontFile subt = *F;
	off_t table_pos = subt.pos;
	off_t map_pos = 0;
	off_t startp, endp, deltap, rangep;
	int map_score = 0;
	u32 version, n, i, format;
	
	version = read_u16(F);
	if (version != 0)
		raise_error(F, OT_VERSION);
	n = read_u16(F);
	
	for (i = 0; i < n; i++) {
		int score;
		u16 platform, encoding;
		u32 off;
		
		platform = read_u16(F);
		encoding = read_u16(F);
		off = read_u32(F);
		
		score = _rate_encoding(platform, encoding);
		if (score > map_score) {
			map_pos = table_pos + off;
			map_score = score;
		}
	}
	
	if (map_score == 0)
		return;
	seek_to(&subt, map_pos);
	
	format = read_u16(&subt);
	switch (format) {
	case 4:
		skip(&subt, 4);
		n = read_u16(&subt);
		if (n % 2 != 0)
			raise_error(F, OT_INVALID);
		n /= 2;
		
		skip(&subt, 6);
		endp = subt.pos;
		skip(&subt, 2*n);
		skip(&subt, 2);
		
		startp = subt.pos;
		skip(&subt, 2*n);
		
		deltap = subt.pos;
		skip(&subt, 2*n);
		
		rangep = subt.pos;
		
		for (i = 0; i < n; i++)
			_read_cmap_group4(&subt, mem, cmap,
				startp + 2*i, endp + 2*i, deltap + 2*i, rangep + 2*i);
		break;
	case 12:
		if (read_u16(&subt) != 0)
			raise_error(&subt, OT_INVALID);
		skip(&subt, 8);
		n = read_u32(&subt);
		for (i = 0; i < n; i++)
			_read_cmap_group12(&subt, mem, cmap);
		break;
	default:
		raise_error(F, OT_VERSION);
		break;
	}
}

void
read_table_name(FontFile *F, MemChunk *mem, OTFont *font, Tables *tables)
{
	char full_name[256] = {};
	char *nameb;
	FontFile strings = *F;
	int i;
	size_t len;
	u16 version, count, offset;
	
	version = read_u16(F);
	count = read_u16(F);
	offset = read_u16(F);
	skip(&strings, offset);
	
	if (version != 0 && version != 1)
		raise_error(F, OT_VERSION);
	
	for (i = 0; i < count; i++)
		if (read_name_record(F, &strings, full_name, sizeof(full_name)))
			break;
	
	if (!full_name[0]) {
		if (font->full_name)
			raise_error(F, OT_UNNAMED);
		return;
	}
	
	tables->name_found = true;
	if (font->full_name) {
		if (strcmp(full_name, font->full_name) == 0)
			tables->name_matched = true;
		return;
	}
	
	len = strlen(full_name);
	nameb = allocate(F, mem, len + 1);
	memmove(nameb, full_name, len + 1);
	font->full_name = nameb;
}

void
read_tag(FontFile *F, char tag[5])
{
	check_len(F, 4);
	memmove(tag, F->data + F->pos, 4);
	tag[4] = 0;
	F->pos += 4;
}

u16
read_u16(FontFile *F)
{
	u16 d = read_u16_at(F, F->pos);
	F->pos += 2;
	return d;
}

u16
read_u16_at(FontFile *F, off_t p)
{
	const u8 *data;
	if (p >= F->len)
		raise_error(F, OT_OFFSET);
	data = F->data + p;
	return (data[0] << 8) | data[1];
}

u32
read_u32(FontFile *F)
{
	u32 d = read_u32_at(F, F->pos);
	F->pos += 4;
	return d;
}

u32
read_u32_at(FontFile *F, off_t p)
{
	const u8 *data;
	if (p >= F->len)
		raise_error(F, OT_OFFSET);
	data = F->data + p;
	return (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
}

void
seek_to(FontFile *F, off_t p)
{
	/* offsets always point to the beginning of some data structure,
	 * so seeking to EOF is an error. */
	if (p < 0 || p >= F->len)
		raise_error(F, OT_OFFSET);
	F->pos = p;
}

void
set_glyph_map(FontFile *subt, MemChunk *mem, CMap *cmap, u32 i, u32 g)
{
	GlyphPage *page;
	int pn = (i & 0x1FF00) >> 8;
	page = cmap->pages[pn];
	if (page == &null_page) {
		page = allocate(subt, mem, sizeof(*page));
		memset(page, 0, sizeof(*page));
		cmap->pages[pn] = page;
	}
	page->glyphs[i & 0xFF] = g;
}

void
skip(FontFile *F, off_t off)
{
	seek_to(F, F->pos + off);
}

void
unmap_file(FontFile f)
{
	munmap((void*) f.data, f.len);
}
