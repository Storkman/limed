/*	#include <stdint.h>
	#include "def.h"
*/

typedef struct Text Text;

/* Text is a gap buffer - a dynamic array with free space somewhere in the middle. */
struct Text {
	char *data;
	int64 gap;	/* where the gap starts */
	int64 after;	/* index of the first byte after the gap */
	int64 cap;	/* total lenght of the buffer in bytes */
};

char*	text_all(Text*);
void	text_append(Text*, const char*, int64);
Rune	text_at(Text*, int64);
void	text_change(Text*, int64 p0, int64 p1, const char *str, int64 len);
int64	text_findrune(Text*, int64, Rune);
int64	text_fixpos(Text*, int64);
Text*	text_free(Text*);
int64	text_len(Text*);
Text*	text_new(void);
int	text_put(Text*, const char*);
int	text_put_fd(Text*, int fd);
int64	text_read(Text*, int64 p, char *buf, int64 len);
char*	text_ref(Text*, int64 p0, int64 p1);
int64	text_rfindrune(Text*, int64, Rune);
int64	text_seek(Text*, int64 p, int64 d);
