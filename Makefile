PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

CCOPT = ${CFLAGS} --std=c99
PLAT = linux
LUA = lua5.4

OBJ = limed.o ansi.o text.o typo.o otf.o file.o layout.o lex.o lua.o parse.o panel.o proc.o term.o xim.o
.PHONY: all clean test install uninstall

all: limed ansi2loz

otf.o: otf.c otf.h
	${CC} ${CCOPT} $$(pkg-config --cflags fontconfig) -c $<

tags: *.c *.h
	ctags -n *.c *.h

test_otf: test_otf.o otf.o text.o
	${CC} -o $@ $^ $$(pkg-config --libs fontconfig) -lutf -L/usr/X11R6/lib -lX11 -lXft

test_otf.o: test_otf.c otf.h
	${CC} ${CCOPT} $$(pkg-config --cflags fontconfig) -c $<

limed: ${OBJ} limed.o
	${CC} -o $@ $^ -lm -ldl -L/usr/X11R6/lib -lX11 -lXext -lXft \
		$$(pkg-config --libs fontconfig) $$(pkg-config --libs ${LUA}) -lutf

limed.o: limed.c *.h
	${CC} ${CCOPT} -DVERSION=\""$$(git describe --always) - $$(date -R)"\" \
		$$(pkg-config --cflags fontconfig) -c $<

lua.o: lua.c *.h
	${CC} ${CCOPT} $$(pkg-config --cflags ${LUA}) -c $<

lua/src/liblua.a:
	make -C lua PLAT=${PLAT}

panel.o: panel.c *.h
	${CC} ${CCOPT} $$(pkg-config --cflags fontconfig) -c $<

typo.o: typo.c *.h
	${CC} ${CCOPT} $$(pkg-config --cflags fontconfig) -c $<

ansi2loz: ansi2loz.o ansi.o lex.o
	${CC} -o $@ $^

lime-tags: lime-tags.c
	${CC} -o $@ $^ -lreadtags

%.o: %.c *.h
	${CC} ${CCOPT} -c $<

clean:
	rm -f limed lime-tags ansi2loz test_otf *.o

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f limed ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/limed
	
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	cp -f limed.1 ${DESTDIR}${MANPREFIX}/man1/limed.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/limed.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/limed
	rm -f ${DESTDIR}${MANPREFIX}/man1/limed.1

test: lua20k test_otf

lua20k:
	cat lua/src/*.c | egrep '[^ ]+' | sed 20000q >lua20k
