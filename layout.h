struct Frag {
	struct DocNode	*node;
	
	int	x, y, w, h;
	int	p, len;
	
	int	attr, fg, bg;
	FontList	*font;
	
	bool	newline;
	int8	is_indent, indent_spaces;
	
	bool fake;
	char fake_text[16];
	
	struct Frag *prev, *next, *prevtab;
};

struct Layout {
	int	w, h;
	int64	start_at;
	struct DocOpts *parse_opts;
	struct FontList *fonts[2];
	
	Text	*txt;
	struct DocNode	*doc;
	
	struct Frag *frags, *firstvisible, *lastvisible, *lastfrag;
	int overflow;
};

struct Frag*	frag_atpos(struct Frag*, int64);
void	frag_freeall(struct Frag*);
struct Frag*	frag_line_end(struct Frag* from);
struct Frag*	frag_line_prev(struct Frag* from);
struct Frag*	frag_line_start(struct Frag* from);

void	layout_perform(struct Layout*);
