#define CMD_RUNE 0x25CA
#define DOC_TXT_MAX 2048

enum {
	LAYOUT_PLAIN, LAYOUT_COLS, LAYOUT_ROWS, LAYOUT_VERBATIM,
};

/* Attributes set by generic text flags like ◊+B◊ */
#define ATRS_MASK (ATR_BOLD_ITALIC|ATR_MONO|ATR_SWAP|ATR_UNDER|ATR_STRIKE|ATR_RDONLY)

enum {
	ATR_REG=0, ATR_BOLD=1, ATR_ITALIC=2, ATR_BOLD_ITALIC=3,
	ATR_MONO=1<<2, ATR_SWAP=1<<3, ATR_UNDER=1<<4, ATR_STRIKE=1<<5,
	ATR_RDONLY=1<<6,
	
	/* Attributes used to control layout. */
	ATR_FOLD=1<<16,	/* Heading of a folded section/branch. After newline, nodes will be hidden. */
	ATR_HIDE=1<<17,	/* Hide node. Only used internally. */
	ATR_OUTLINE=1<<18,	/* Draw tree outlines under  this branch. */
	
	/* Attributes used to control editor behavior. */
	ATR_SPACES=1<<24,
	ATR_PROMPT=1<<25, ATR_PROMPT2=1<<26,
	ATR_CONTROL=1<<27,
};

enum {
	DOC_NONE,
	DOC_ROOT,
	DOC_SECTION, DOC_NL,
	DOC_TXT, DOC_TAB,
	DOC_SPACES,	/* two or more spaces */
	DOC_ATTR, DOC_COL, DOC_PROMPT,
	DOC_LINK,
	DOC_CMDESC, DOC_ERROR,
	DOC_TREE_BRANCH, DOC_END, DOC_TREE_LEAF,
	DOC_TREE_UP, DOC_TREE_DOWN,
	DOC_EOF,  /* DOC_EOF must be the last element of this enum type */
};

struct DocNode {
	struct DocNode *prev, *next;
	struct DocNode *parent;
	
	uint8 type;
	uint8 visible;	/* true if a node of this type can display text */
	
	uint32 layout, attr, fg, bg;
	int depth;
	int64 p0, p1;	/* start and end of text content */
	int64 n0, n1;	/* start and end of node itself */
	uint8 lasttab;	/* true if this node is in the last column on this line */
	
	int8 tabw;	/* tab width for verbatim layout */
};

struct DocOpts {
	uint8 plain;
};

struct DocNode*	node_after(struct DocNode *root, int64 p);
void	node_discard(struct DocNode *root, int64 p);
void	node_free(struct DocNode*);
void	node_freelist(struct DocNode*);
int	node_hastext(struct DocNode*);
int	node_in_section(struct DocNode*);
int	node_lasttab(Text*, struct DocOpts*, struct DocNode*);
struct DocNode*	node_new_root(void);
int	node_plain_text(char *dst, int64 maxlen, Text*, struct DocNode*);
void	node_write(Text*, struct DocNode *prev, struct DocNode *n, Text *out);
struct DocNode*	parse_at(Text*, struct DocOpts*, struct DocNode *root, int64 p);
struct DocNode*	parse_next(Text*, struct DocOpts*, struct DocNode *cur);
