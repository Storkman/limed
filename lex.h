typedef struct Lex {
	const char *buf;
	int offset, len, pos;
} Lex;

void	lex_all(Lex*);
int	lex_eof(Lex*);
int	lex_getc(Lex*);
void	lex_init(Lex*, const char *buf, int len);
int	lex_next_any(Lex*, const char *cs);
int	lex_next_b(Lex*, char c);
int	lex_next_brange(Lex*, char a, char b);
int	lex_peekc(Lex*);
int	lex_str(Lex*, char *buf, int len);
void	lex_token(Lex*);

int	parse_int(Lex*, int*);
int	parse_uint(Lex*, int*);
