#define _DEFAULT_SOURCE
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <utf.h>
#include <X11/Xft/Xft.h>

#include "def.h"
#include "typo.h"
#include "typo-xft.h"
#include "otf.h"

enum { ATR_REG=0, ATR_BOLD=1, ATR_ITALIC=2, ATR_BOLD_ITALIC=3 };

typedef struct Color Color;

struct Color {
	XftColor fc;
};

struct FontData {
	bool	loaded;
	XftFont	*style[4];
	OTFont	otf[4];
};

static Color *colors;

static size_t	strtorunes(OTGlyph*, size_t, const char*, size_t);
/* typo_need_font forces loading a font on the list previously prepared with typo_load_fonts_deferred. */
static void	typo_need_font(FontList*, int idx);
static void	xft_alloc_color(XftColor*, unsigned int rgb);

size_t
strtorunes(OTGlyph *rs, size_t bn, const char *str, size_t len)
{
	size_t i, ri;
	int n;
	i = ri = 0;
	while (i < len && ri < bn) {
		n = charntorune(rs + ri, str + i, len - i);
		if (n == 0)
			break;
		i += n;
		ri++;
	}
	return ri;
}

int
typo_advance(FontList *font, int attr, const char *str, int len)
{
	FontExtents ext;
	typo_measure(font, attr, str, len, &ext);
	return ext.advance;
}

int
typo_advance_span(FontData *font, int attr, const char *str, int len)
{
	FontExtents ext;
	typo_measure_span(font, attr, str, len, &ext);
	return ext.advance;
}

static int
_ascii_font(FontList *fonts, const char *str, int len, FontData **font)
{
	const char *c = str + 1, *end = str + len;
	*font = &fonts->fonts[0];
	c = str;
	while (c < end && ((unsigned char) *c & (1<<7)) == 0)
		c++;
	return c - str;
}

static int
_first_font(FontList *fonts, int attr, Rune r)
{
	XftFont *xf;
	int i;
	attr &= ATR_BOLD_ITALIC;
	for (i = 0; i < fonts->count; i++) {
		typo_need_font(fonts, i);
		xf = fonts->fonts[i].style[attr];
		if (FcCharSetHasChar(xf->charset, r))
			return i;
	}
	return 0;
}

int
typo_choose_font(FontList *fonts, int attr, const char *str, int len, FontData **font)
{
	int i, n, fn = -1, nextfn;
	Rune r;
	i = 0;
	if (len <= 0)
		return 0;
	typo_need_font(fonts, 0);
	if ((unsigned char) str[0] < 0x80)
		return _ascii_font(fonts, str, len, font);
	while (i < len) {
		if ((unsigned char) str[i] < 0x80)
			return i;
		n = charntorune(&r, str + i, len - i);
		if (n == 0)
			break;
		
		nextfn = _first_font(fonts, attr, r);
		if (fn < 0) {
			fn = nextfn;
			*font = &fonts->fonts[fn];
		} else if (nextfn != fn)
			return i;
		i += n;
	}
	return i;
}

FontData*
typo_choose_font_rune(FontList *fonts, int attr, Rune r)
{
	int i;
	attr &= ATR_BOLD_ITALIC;
	for (i = 0; i < fonts->count; i++) {
		typo_need_font(fonts, i);
		if (FcCharSetHasChar(fonts->fonts[i].style[attr]->charset, r))
			return &fonts->fonts[i];
	}
	return &fonts->fonts[0];
}

void
typo_draw(Surface *dst, int x, int y, FontList *fonts, int attr, unsigned long color, const char *str, int len)
{
	int span;
	FontData *font;
	attr &= ATR_BOLD_ITALIC;
	while (len > 0) {
		span = typo_choose_font(fonts, attr, str, len, &font);
		if (span == 0)
			break;
		typo_draw_span(dst, x, y, font, attr, color, str, span);
		x += typo_advance_span(font, attr, str, span);
		str += span;
		len -= span;
	}
}

void
typo_draw_span(Surface *dst, int x, int y, FontData *font, int attr, unsigned long color, const char *str, int len)
{
	OTFont *otf ;
	OTGlyph buf[2560];
	
	XftFont *xfont= font->style[attr & ATR_BOLD_ITALIC];
	XftColor c;
	XftColor *xc;
	
	if (color & 0xff000000) {
		xft_alloc_color(&c, color);
		xc = &c;
	} else
		xc = &colors[color].fc;
	
	otf = &font->otf[attr & ATR_BOLD_ITALIC];
	if (!otf->loaded) {
		XftDrawStringUtf8(dst, xc, xfont, x, y, (FcChar8*) str, len);
		return;
	}
	len = strtorunes(buf, sizeof(buf), str, len);
	len = OT_map_glyphs(otf, buf, sizeof(buf), len);
	if (len == 0)
		fprintf(stderr, "typo_draw_span: input too long to perform glyph substitution\n");
	XftDrawGlyphs(dst, xc, xfont, x, y, buf, len);
}

void
typo_draw_rect(Surface *dst, int x, int y, int w, int h, unsigned long color)
{
	XftColor c;
	XftColor *xc;
	if (color & 0xff000000) {
		xft_alloc_color(&c, color);
		xc = &c;
	} else
		xc = &colors[color].fc;
	XftDrawRect(dst, xc, x, y, w, h);
}

void
typo_draw_rune(Surface *dst, int x, int y, FontList *fonts, int attr, unsigned long color, Rune r)
{
	XftFont *xfont;
	XftColor c;
	XftColor *xc;
	FontData *font = typo_choose_font_rune(fonts, attr, r);
	xfont = font->style[attr & ATR_BOLD_ITALIC];
	if (color & 0xff000000) {
		xft_alloc_color(&c, color);
		xc = &c;
	} else
		xc = &colors[color].fc;
	XftDrawString32(dst, xc, xfont, x, y, (FcChar32*) &r, 1);
}

static XftFont*
font_load_single(FcPattern *p)
{
	XftPattern *match;
	XftResult r;
	FcConfigSubstitute(NULL, p, FcMatchPattern);
	FcDefaultSubstitute(p);
	match = XftFontMatch(disp, XDefaultScreen(disp), p, &r);
	if (r != XftResultMatch)
		return NULL;
	return XftFontOpenPattern(disp, match);
}

static void
font_load(FontData *font, const char *name, double size)
{
	FcPattern *p, *pb;
	char *fmt;
	int i;
	p = FcNameParse((const FcChar8*) name);
	if (!p)
		die("invalid font name: %s\n", name);
	FcPatternAddDouble(p, FC_SIZE, size);
	for (i = 0; i <= ATR_BOLD_ITALIC; i++) {
		pb = FcPatternDuplicate(p);
		if (i & ATR_BOLD) {
			FcPatternDel(pb, FC_STYLE);
			FcPatternAddInteger(pb, FC_WEIGHT, FC_WEIGHT_BOLD);
		}
		if (i & ATR_ITALIC)
			FcPatternAddInteger(pb, FC_SLANT, FC_SLANT_ITALIC);
		font->style[i] = font_load_single(pb);
		if (!font->style[i])
			die("couldn't load font: %s\n", name);
		
		FcPatternGetString(font->style[i]->pattern, FC_FONTFORMAT, 0, (FcChar8**) &fmt);
		if (strcmp(fmt, "TrueType") == 0) {
			font->otf[i] = OT_load_FcPattern(font->style[i]->pattern);
			if (font->otf[i].error)
				fprintf(stderr, "couldn't load font %s: %s\n", name, OT_strerror(font->otf[i].error));
		}
		FcPatternDestroy(pb);
	}
	FcPatternDestroy(p);
	font->loaded = true;
}

void
typo_load_colors(int *rgbvals, int n)
{
	int i, v, rgb;
	XRenderColor xrc;
	Color *c;
	colors = calloc(n, sizeof(*c));
	if (!colors)
		die("out of memory\n");
	for (i = 0; i < n; i++) {
		rgb = rgbvals[i];
		c = &colors[i];
		
		v = (rgb & 0xFF0000) >> 16;
		v |= v << 8;
		xrc.red = v;
		
		v = (rgb & 0x00FF00) >> 8;
		v |= v << 8;
		xrc.green = v;
		
		v = rgb & 0x0000FF;
		v |= v << 8;
		xrc.blue = v;
		
		xrc.alpha = 0xFFFF;
		XftColorAllocValue(disp, DefaultVisual(disp, screen), DefaultColormap(disp, screen), &xrc, &c->fc);
	}
}

void
typo_load_fonts(FontList *list)
{
	int i;
	typo_load_fonts_deferred(list);
	for (i = 0; i < list->count; i++)
		typo_need_font(list, i);
}

void
typo_load_fonts_deferred(FontList *list)
{
	int n;
	for (n = 0; list->names[n]; n++) {}
	list->count = n;
	list->fonts = calloc(n, sizeof(list->fonts[0]));
	if (!list->fonts)
		die("out of memory\n");
	typo_need_font(list, 0);
	typo_measure_font(&list->fonts[0], 0, &list->extents);
}

void
typo_measure(FontList *fonts, int attr, const char *str, int len, FontExtents *extents)
{
	int span;
	FontData *font;
	FontExtents ext = {};
	*extents = ext;
	
	while (len > 0) {
		span = typo_choose_font(fonts, attr, str, len, &font);
		if (span == 0)
			break;
		typo_measure_span(font, attr, str, span, &ext);
		typo_measure_add(extents, extents, &ext);
		str += span;
		len -= span;
	}
}

void
typo_measure_add(FontExtents *res, FontExtents *a, FontExtents *b)
{
	int a_bottom, b_bottom;
	a_bottom = a->h - a->y;
	b_bottom = b->h - b->y;
	res->w = a->advance + b->x + b->w - a->x;
	res->h = MAX(a_bottom, b_bottom) + MAX(a->y, b->y);
	res->x = MAX(a->x, b->x);
	res->y = MAX(a->y, b->y);
	res->ascent = MAX(a->ascent, b->ascent);
	res->descent = MAX(a->descent, b->descent);
	res->advance = a->advance + b->advance;
}

static void
set_measure(XftFont *f, XGlyphInfo *xe, FontExtents *ext)
{
	ext->x = xe->x;
	ext->y = xe->y;
	ext->ascent = f->ascent;
	ext->descent = f->descent;
	ext->w = xe->width;
	ext->h = xe->height;
	ext->advance = xe->xOff;
}

void
typo_measure_font(FontData *f, int attr, FontExtents *e)
{
	XftFont *xf = f->style[attr & ATR_BOLD_ITALIC];
	e->ascent = xf->ascent;
	e->descent = xf->descent;
	e->h = xf->height;
	e->advance = xf->max_advance_width;
}

void
typo_measure_span(FontData *font, int attr, const char *str, int len, FontExtents *extents)
{
	OTFont *otf ;
	OTGlyph buf[2560];
	
	XGlyphInfo xe;
	XftFont *xfont = font->style[attr & ATR_BOLD_ITALIC];
	otf = &font->otf[attr & ATR_BOLD_ITALIC];
	if (!otf->loaded) {
		XftTextExtentsUtf8(disp, xfont, str, len, &xe);
		set_measure(xfont, &xe, extents);
		return;
	}
	len = strtorunes(buf, sizeof(buf), str, len);
	len = OT_map_glyphs(otf, buf, sizeof(buf), len);
	if (len == 0)
		fprintf(stderr, "typo_measure_span: input too long to perform glyph substitution\n");
	XftGlyphExtents(disp, xfont, buf, len, &xe);
	set_measure(xfont, &xe, extents);
}

void
typo_measure_rune(FontList *fonts, int attr, Rune r, FontExtents *ex)
{
	FontData *f = typo_choose_font_rune(fonts, attr, r);
	XftFont *xfont = f->style[attr & ATR_BOLD_ITALIC];
	FcChar32 c = r;
	XGlyphInfo xe;
	XftTextExtents32(disp, xfont, &c, 1, &xe);
	set_measure(xfont, &xe, ex);
}

void
typo_need_font(FontList *fonts, int idx)
{
	assert(idx < SIZE(fonts->names));
	if (fonts->fonts[idx].loaded)
		return;
	font_load(&fonts->fonts[idx], fonts->names[idx], fonts->size);
}

void
typo_unload_fonts(FontList *fonts)
{
	int i, j;
	for (i = 0; i < fonts->count; i++)
		for (j = 0; j < SIZE(fonts->fonts[i].style); j++)
			if (fonts->fonts[i].style[j])
				XftFontClose(disp, fonts->fonts[i].style[j]);
	free(fonts->fonts);
	fonts->fonts = NULL;
	fonts->count = 0;
}

void
xft_alloc_color(XftColor *c, unsigned int rgb)
{
	XRenderColor xrc;
	bool ok;
	xrc.red = (rgb & 0xFF0000) >> 16;
	xrc.green = (rgb & 0x00FF00) >> 8;
	xrc.blue = (rgb & 0x0000FF);
	xrc.red |= xrc.red << 8;
	xrc.green |= xrc.green << 8;
	xrc.blue |= xrc.blue << 8;
	ok = XftColorAllocValue(
		disp, DefaultVisual(disp, screen), DefaultColormap(disp, screen),
		&xrc, c);
	if (!ok) die("couldn't allocate color\n");
}
