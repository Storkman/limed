typedef struct Term Term;
struct Term {
	struct FileRef	file;
	struct FileRef	out_file;
	uint8	interactive;
	
	Text	*command;
	uint8	reading_command;
	
	char wdir[PATHLEN];
	int pid, sanitizer_pid;
	int infd, outfd;
	
	Text	*input;
	bool	eof;	/* if set, close infd after input runs out. */
	
	int	error;
};

void	term_deinit(Term*);
void	term_init(Term*);
int	term_is_attached(Term*);
void	term_handle_fd(Term*, struct pollfd fd[2]);
void	term_hang_up(Term*);
void	term_poll_fd(Term*, struct pollfd fds[2]);
void	term_send(Term*, const char*, int64);
void	term_set_interactive(Term*, int);
