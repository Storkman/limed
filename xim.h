typedef struct Xim Xim;
struct Xim {
	XIC	ic;
	
	Window	win;
	XPoint	spot;
	XVaNestedList	icattr;
	
	XICCallback cb[4];
};

void	xim_deinit(Xim*);
void	xim_focus_in(Xim*);
void	xim_focus_out(Xim*);
int	xim_init(Xim*, Window);
int	xim_key(Xim*, XKeyEvent*, char*, size_t, KeySym*);
bool	xim_need_cursor(Xim*);
void	xim_open(Display*, char *name, char *class);
void	xim_set_cursor(Xim*, int, int);
