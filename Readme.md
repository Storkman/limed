# limed

	limed [file [address]]

`limed` is a programming-oriented text editor mostly inspired by [Acme](http://acme.cat-v.org/) from Plan 9.

A proportional font with [elastic tabstops](http://nickgravgaard.com/elastic-tabstops/) is used by default.

Text attributes and formatting can be controlled with a custom markup syntax.
Open `doc_format.loz` in `limed` for details.

See the man page for documentation.

![Screenshot](screenshot.png)

## Dependencies

- Xlib
- [libutf](https://github.com/cls/libutf)
- [plumber](http://man.cat-v.org/plan_9/4/plumber) from [Plan 9 Port](https://9fans.github.io/plan9port/)
  for opening files/URLs - not necessary for editing, might be replaced or made optional in the future
- A [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts) patched font to display some UI elements.
   Default config uses the Hack font.
