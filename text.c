#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <utf.h>

#include "def.h"
#include "text.h"

struct Text*
text_new(void)
{
	struct Text *t = malloc(sizeof(*t));
	if (!t)
		die("out of memory\n");
	t->gap = 0;
	t->after = t->cap = BLOCKSIZE;
	t->data = malloc(t->cap);
	if (!t->data)
		die("out of memory\n");
	return t;
}

static void
expand(Text *t, int64 n)
{
	int64 nc;
	int64 len = t->gap + t->cap - t->after;
	if (len + n < t->cap)
		return;
	for (nc = t->cap; len + n + 1 > nc; nc += BLOCKSIZE) {}
	t->data = realloc(t->data, nc);
	if (!t->data)
		die("out of memory");
	memmove(t->data + nc - (t->cap - t->after), t->data + t->after, t->cap - t->after);
	t->after = nc - (t->cap - t->after);
	t->cap = nc;
}

static void
split(Text *t, int64 p)
{
	if (p < t->gap) {
		int64 len = t->gap - p;
		memmove(t->data + t->after - len, t->data + p, len);
		t->gap -= len;
		t->after -= len;
		return;
	}
	if (p > t->gap) {
		int64 len = p - t->gap;
		memmove(t->data + t->gap, t->data + t->after, len);
		t->gap += len;
		t->after += len;
	}
}

char*
text_all(Text *t)
{
	return text_ref(t, 0, text_len(t));
}

void
text_append(Text *t, const char *str, int64 n)
{
	t->data = realloc(t->data, t->cap+n);
	if (!t->data)
		die("out of memory\n");
	memmove(t->data+t->cap, str, n);
	t->cap += n;
}

Rune
text_at(Text *t, int64 p)
{
	Rune r;
	if (p == text_len(t))
		return 0;
	if (t->gap <= p)
		text_all(t);
	chartorune(&r, t->data + p);
	return r;
}

void
text_change(Text *t, int64 p0, int64 p1, const char *str, int64 len)
{
	assert(p1 <= text_len(t));
	split(t, p1);
	t->gap -= p1 - p0;
	expand(t, len);
	memmove(t->data + t->gap, str, len);
	t->gap += len;
}

int64
text_findrune(Text *t, int64 p, Rune tgt)
{
	return utfrune(text_ref(t, p, text_len(t)), tgt) - t->data;
}

int64
text_read(Text *t, int64 p, char *buf, int64 len)
{
	int64 n = text_len(t) - p;
	if (len < n)
		n = len;
	if (p + n > t->gap)
		split(t, p + n);
	memmove(buf, t->data + p, n);
	return n;
}

int64
text_rfindrune(Text *t, int64 p, Rune tgt)
{
	char *s = utfrrune(text_ref(t, 0, p), tgt);
	return s ? s - t->data : -1;
}

int64
text_fixpos(Text *t, int64 p)
{
	int64 len = text_len(t);
	if (p < 0)
		return 0;
	if (p > len)
		return len;
	while (text_at(t, p) == Runeerror)
		p++;
	return p;
}

Text*
text_free(Text *t)
{
	if (!t)
		return NULL;
	free(t->data);
	free(t);
	return NULL;
}

int64
text_len(Text *t)
{
	return t->gap + t->cap - t->after;
}

char*
text_ref(Text *t, int64 p0, int64 p1)
{
	split(t, p1);
	t->data[p1] = 0;
	return t->data + p0;
}

int
text_put(Text *t, const char *name)
{
	int fd, err = 0;
	fd = open(name, O_WRONLY | O_TRUNC | O_CREAT, 0622);
	if (fd < 0)
		return errno;
	err = text_put_fd(t, fd);
	if (close(fd))
		return err ? err : -1;
	return err;
}

int
text_put_fd(Text *t, int fd)
{
	int n, off = 0;
	text_all(t);
	do {
		n = write(fd, t->data + off, t->gap - off);
		if (n < 0)
			return errno ? errno : -1;
		off += n;
	} while (off < t->gap);
	return 0;
}

/* text_seek returns the byte-index D runes after byte-index P. D may be negative. */
int64
text_seek(Text *t, int64 p, int64 d)
{
	Rune r;
	int64 len = text_len(t);
	if (d > 0) {
		if (t->gap < p + d * UTFmax)
			text_all(t);
		while (d > 0) {
			assert(p < len);
			p += chartorune(&r, t->data + p);
			d--;
		}
	}
	if (d < 0) {
		if (t->gap < p)
			split(t, p);
		while (d < 0 && p > 0) {
			do {
				assert(p > 0);
				p--;
				chartorune(&r, t->data + p);
			} while (r == Runeerror);
			d++;
		}
	}
	return p;
}
