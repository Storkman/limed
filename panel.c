#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <poll.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <regex.h>
#include <utf.h>

#include <X11/XKBlib.h>
#include <X11/Xatom.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/Xdbe.h>
#include <X11/keysym.h>

#include "def.h"
#include "text.h"
#include "parse.h"
#include "typo.h"
#include "layout.h"
#include "file.h"
#include "xim.h"
#include "panel.h"

typedef void ButtonAction(struct Panel*, int mod_state, Time time, int arg);
typedef void FragAction(struct Panel*, struct Frag*, struct DocNode*);
typedef void KeyAction(struct Panel*, int mod_state, Time time, int arg);

static int	getcurx(struct Text*, struct Frag*, int64 p0);
static bool	is_read_only(struct Panel*, int64 p0, int64 p1);
static bool	next_is(Text*, int64, CharClass*, int dir);
static void	order_sel(int64*, int64*, int64, int64);
static void	overlay_branch(struct Panel*, struct Frag*, int color, Surface*);
static void	overlay_tree(struct Panel*, Surface*);
static void	panel_changed_apply(struct Panel*, int64 p0, int64 p1, int64 len);
static struct Frag*	panel_frag_at(struct Panel*, int x, int y);
static void	panel_layout_from_top(struct Panel*);
static void	panel_rewrite_node(struct Panel*, struct DocNode*);
static void	panel_select_time(struct Panel*, int64, int64, Time);
static int	run_frag_action(struct Panel*, struct Frag*, int look);
static int64	seek_past(Text*, int64, CharClass*, int dir);
static void	unfold_node(struct Panel*, struct DocNode*);
static void	update_cursor(struct Panel*);
static void	user_ins(struct Panel *p, const char *str, int64 len);
static void	user_replace(struct Panel *p, int64 p0, int64 p1, const char *str, int64 len);

static void	_bt_sel(struct Panel*, int, Time, int);
static void	_bt_cut(struct Panel*, int, Time, int);
static void	_bt_paste(struct Panel*, int, Time, int);
static void	_bt_act(struct Panel*, int, Time, int);

static void	frag_act_branch_look(struct Panel*, struct Frag*, struct DocNode*);
static void	frag_act_link_look(struct Panel*, struct Frag*, struct DocNode*);

static void	_key_bs(struct Panel*, int, Time, int);
static void	_key_del(struct Panel*, int, Time, int);
static void	_key_insert(struct Panel*, int, Time, int);
static void	_key_paste(struct Panel*, int, Time, int);
static void	_key_tab(struct Panel*, int, Time, int);
static void	_key_ret(struct Panel*, int, Time, int);

static void	_nav_del_word(struct Panel*, int, Time, int);
static void	_nav_end(struct Panel*, int, Time, int);
static void	_nav_h(struct Panel*, int, Time, int);
static void	_nav_home(struct Panel*, int, Time, int);
static void	_nav_line_bol(struct Panel*, int, Time, int);
static void	_nav_line_eol(struct Panel*, int, Time, int);
static void	_nav_line_delf(struct Panel*, int, Time, int);
static void	_nav_line_delb(struct Panel*, int, Time, int);
static void	_nav_pd(struct Panel*, int, Time, int);
static void	_nav_pu(struct Panel*, int, Time, int);
static void	_nav_v(struct Panel*, int, Time, int);
static void	_nav_word(struct Panel*, int, Time, int);

extern Display *disp;
extern XIM xim;

#define MOD_ALT Mod1Mask
#define ButtonChord	-1
#define ButtonCancelled	-2

struct ButtonStateEvent {
	int state, button, event, mod;
	ButtonAction *fn;
	int arg;
	int next;
} button_states[] = {
	/* state,	received event,	key mod,	function + argument,	next state */
	{0,	Button1, ButtonPress,	0,	_bt_sel, 1,	Button1},
	{0,	Button1, ButtonPress,	ShiftMask,	_bt_sel, 1,	Button1},
	{0,	Button2, ButtonPress,	0,	NULL, 0,	Button2},
	{0,	Button3, ButtonPress,	0,	NULL, 0,	Button3},
					
	{Button1,	Button2, ButtonPress,	0,	_bt_cut, 0,	ButtonChord},
	{Button1,	Button3, ButtonPress,	MOD_ALT,	_bt_paste, 0,	ButtonChord},
	{Button1,	Button3, ButtonPress,	0,	_bt_paste, 1,	ButtonChord},
					
	{Button2,	Button1, ButtonPress,	0,	NULL, 0,	ButtonCancelled},
	{Button2,	Button3, ButtonPress,	0,	NULL, 0,	0},
					
	{Button3,	Button1, ButtonPress,	0,	NULL, 0,	ButtonCancelled},
	{Button3,	Button2, ButtonPress,	0,	NULL, 0,	0},
					
	{ButtonChord,	Button2, ButtonPress,	0,	_bt_cut, 0,	ButtonChord},
	{ButtonChord,	Button3, ButtonPress,	MOD_ALT,	_bt_paste, 0,	ButtonChord},
	{ButtonChord,	Button3, ButtonPress,	0,	_bt_paste, 1,	ButtonChord},
					
	{ButtonChord,	Button1, ButtonRelease,	0,	NULL, 0,	0},
	{Button1,	Button1, ButtonRelease,	0,	_bt_sel, 0,	0},
	{Button2,	Button2, ButtonRelease,	0,	_bt_act, 0,	0},
	{Button3,	Button3, ButtonRelease,	0,	_bt_act, 1,	0},
					
	{ButtonCancelled,	Button1, ButtonRelease,	0,	NULL, 0,	0},
};

struct KeyCommandConf {
	KeySym	ksym;
	unsigned int	mod;
	KeyAction	*fn;
	bool	shift_nav;
	int	arg;
} key_commands[] = {
	/* ksym,	mod mask,	fn,	shift-nav,	fn arg	*/
	{ XK_Left,	ControlMask,	_nav_word,	1,	-1 },
	{ XK_Right,	ControlMask,	_nav_word,	1,	1},
	{ XK_a,	ControlMask,	_nav_line_bol,	0 },
	{ XK_A,	ControlMask,	_nav_line_bol,	1 },
	{ XK_e,	ControlMask,	_nav_line_eol,	0 },
	{ XK_E,	ControlMask,	_nav_line_eol,	1 },
	{ XK_k,	ControlMask,	_nav_line_delf,	0 },
	{ XK_u,	ControlMask,	_nav_line_delb,	0 },
	{ XK_w,	ControlMask,	_nav_del_word,	0 },
				
	{ XK_Delete,	Mod1Mask,	_key_paste,	0 },
				
	{ XK_BackSpace,	0,	_key_bs,	0 },
	{ XK_Delete,	0,	_key_del,	0 },
	{ XK_Left,	0,	_nav_h,	1,	-1 },
	{ XK_Right,	0,	_nav_h,	1,	1},
	{ XK_Up,	0,	_nav_v,	1,	-1},
	{ XK_Down,	0,	_nav_v,	1,	1 },
	{ XK_Page_Down,	0,	_nav_pd,	0 },
	{ XK_Page_Up,	0,	_nav_pu,	0 },
	{ XK_Home,	0,	_nav_home,	0 },
	{ XK_End,	0,	_nav_end,	0 },
	{ XK_Insert,	0,	_key_insert,	0 },
	{ XK_Tab,	0,	_key_tab,	0 },
	{ XK_Return,	0,	_key_ret,	0 },
};

struct NodeActionConf {
	FragAction *look, *exec;
} node_actions[] = {
	[DOC_LINK]	=	{ frag_act_link_look,	NULL },
	[DOC_TREE_BRANCH]	=	{ frag_act_branch_look,	NULL },
};

struct Panel *primary_selection_panel;

void
frag_act_branch_look(struct Panel *p, struct Frag *f, struct DocNode *n)
{
	n->attr ^= ATR_FOLD;
	panel_rewrite_node(p, n);
	p->layout_done = false;
}

void
frag_act_link_look(struct Panel *p, struct Frag *f, struct DocNode *n)
{
	p->onlook(p->handler_data, p, n->p0, n->p1, 0);
}

int
getcurx(struct Text *t, struct Frag *f, int64 p0)
{
	if (f->len <= 1)
		return f->x;
	return f->x + typo_advance(f->font, f->attr, text_ref(t, f->p, p0), p0 - f->p);
}

bool
is_read_only(struct Panel *panel, int64 p0, int64 p1)
{
	struct DocNode *n = parse_at(panel->txt, &panel->parse_opts, panel->doc, p0);
	do {
		if (n->attr & ATR_RDONLY)
			return true;
		n = parse_next(panel->txt, &panel->parse_opts, n);
	} while (n->type != DOC_EOF && n->n1 <= p1);
	return false;
}

bool
name_rune(Rune r)
{
	return !isspacerune(r) && r != CMD_RUNE;
}

bool
not_name_rune(Rune r)
{
	return isspacerune(r) || r == CMD_RUNE;
}

bool
space_rune(Rune r)
{
	return isspacerune(r);
}

bool
word_rune(Rune r)
{
	return isalnumrune(r) || r == '_';
}

bool
not_word_rune(Rune r)
{
	return !isalnumrune(r) && r != '_';
}

bool
next_is(Text *tx, int64 p, CharClass *class, int dir)
{
	int64 at = (dir >= 0) ? p : text_seek(tx, p, -1);
	return class(text_at(tx, at));
}

void
order_sel(int64 *p0, int64 *p1, int64 c0, int64 c1)
{
	if (c0 < 0)
		return;
	if (c1 > c0) {
		*p0 = c0;
		*p1 = c1;
	} else {
		*p0 = c1;
		*p1 = c0;
	}
}

void
overlay_tree(struct Panel *panel, Surface *drw)
{
	struct Frag *f, *lastf = panel->lastvisible->next;
	for (f = panel->firstvisible; f != lastf; f = f->next) {
		struct DocNode *n = f->node;
		if (n->type != DOC_TREE_BRANCH || n->attr & ATR_FOLD)
			continue;
		if (n->attr & ATR_OUTLINE)
			overlay_branch(panel, f, COL_TREE, drw);
	}
}

void
overlay_branch(struct Panel *panel, struct Frag *root, int color, Surface *drw)
{
	struct Frag *f, *lastf = panel->lastvisible->next;
	struct Frag *last_branch_frag = NULL;
	int root_depth = root->node->depth;
	int ly;
	
	int ox = panel->x + PANEL_MARGIN;
	int oy = panel->y + 2;
	
	int rx = root->x + root->w / 2;
	int ry = root->y + root->h + root->h / 8;
	
	for (f = root->next; f != lastf; f = f->next) {
		int type = f->node->type;
		if (type == DOC_END)
			break;
		if (type == DOC_EOF)
			return;
	}
	
	for (; f != lastf; f = f->next) {
		struct DocNode *n = f->node;
		if (n->depth < root_depth)
			break;
		if (n->type == DOC_TREE_BRANCH) {
			if (n->depth == root_depth)
				break;
			if (n->depth != root_depth + 1)
				continue;
		} else if (n->type == DOC_TREE_LEAF) {
			if (n->depth != root_depth + 1)
				continue;
		} else
			continue;
		last_branch_frag = f;
		typo_draw_rect(drw, ox + rx, oy + f->y + f->h / 2, f->x - rx - root->w/3,  1, color);
	}
	
	if (!last_branch_frag)
		return;
	ly = last_branch_frag->y + last_branch_frag->h / 2;
	typo_draw_rect(drw, ox + rx, oy + ry, 1, ly - ry, color);
}

int
run_frag_action(struct Panel *p, struct Frag *f, int look)
{
	struct DocNode *n = f->node;
	FragAction *act;
	while (n->attr & ATR_CONTROL)
		n = n->parent;
	if (n->type >= SIZE(node_actions))
		return 0;
	if (look)
		act = node_actions[n->type].look;
	else
		act = node_actions[n->type].exec;
	
	if (!act)
		return 0;
	act(p, f, n);
	return 1;
}

int64
panel_atpos(struct Panel *p, int x, int y, struct Frag **frag)
{
	FontExtents ext;
	char *str;
	Rune r;
	struct Frag *f;
	int i, xoff, rlen;
	x = MIN(x, p->w);
	y = MIN(y, p->h);
	f = panel_frag_at(p, x, y);
	if (frag)
		*frag = f;
	if (f->fake) {
		while (f->fake && f->prev)
			f = f->prev;
		return f->p + f->len;
	}
	if (f->len <= 1)
		return f->p;
	
	xoff = f->x;
	str = text_ref(p->txt, f->p, f->p + f->len);
	i = 0;
	while (i < f->len) {
		rlen = charntorune(&r, str + i, f->len - i);
		typo_measure_rune(f->font, f->attr, r, &ext);
		xoff += ext.advance;
		if (xoff >= x)
			return f->p + i;
		i += rlen;
	}
	return f->p + i;
}

int64
panel_atwinpos(struct Panel *p, int x, int y, struct Frag **frag)
{
	x -= p->x + PANEL_MARGIN;
	y -= p->y;
	return panel_atpos(p, x, y, frag);
}

int64
panel_bol(struct Panel *panel, int64 cpos)
{
	int64 p = text_rfindrune(panel->txt, text_fixpos(panel->txt, cpos), '\n');
	if (p < 0)
		return 0;
	else
		return p + 1;
}

int64
panel_eol(struct Panel *panel, int64 cpos)
{
	int64 p = text_findrune(panel->txt, text_fixpos(panel->txt, cpos), '\n');
	if (p < 0)
		return text_len(panel->txt);
	else
		return p;
}

void
panel_button(struct Panel *p, XButtonEvent *ev)
{
	struct Frag *frag;
	int i;
	int click;
	if (ev->button == Button4 || ev->button == Button5) {
		if (ev->type != ButtonPress)
			return;
		if (p->disable_scrolling)
			return;
		panel_lnscroll(p, ev->button == Button4 ? -1 : 1);
		update_cursor(p);
		return;
	}
	
	click = panel_atwinpos(p, ev->x, ev->y, &frag);
	if (p->buttons_state == 0) {
		if (!(ev->state & ShiftMask) || p->click0 < 0)
			p->click0 = click;
		p->click_frag = frag;
	}
	p->click1 = click;
	
	for (i = 0; i < SIZE(button_states); i++) {
		struct ButtonStateEvent *st = &button_states[i];
		if (st->state != p->buttons_state || ev->button != st->button || ev->type != st->event)
			continue;
		if ((ev->state & st->mod) != st->mod)
			continue;
		p->buttons_state = st->next;
		if (st->fn)
			st->fn(p, ev->state, ev->time, st->arg);
		update_cursor(p);
		return;
	}
}

void
_bt_sel(struct Panel *p, int mod_state, Time time, int arg)
{
	if (!arg) {
		if (p->p0 == p->p1)
			return;
		panel_select_time(p, p->p0, p->p1, time);
		return;
	}
	
	p->cursorx = -1;
	if (time - p->lastclick < DCLICK_DELAY
	&& p->click1 == p->p0 && p->p0 == p->p1)
		panel_expandsel(p, p->click1, &p->p0, &p->p1);
	else
		order_sel(&p->p0, &p->p1, p->click0, p->click1);
	p->lastclick = time;
}

void
_bt_cut(struct Panel *p, int mod_state, Time time, int arg)
{
	panel_cut(p);
}

void
_bt_paste(struct Panel *p, int mod_state, Time time, int arg)
{
	panel_paste(p, arg ? xatom("CLIPBOARD") : XA_PRIMARY);
}

void
_bt_act(struct Panel *p, int mod_state, Time time, int arg)
{
	PanelAction *action = arg ? p->onlook : p->onexec;
	int64 c0 = 0, c1 = 0;
	order_sel(&c0, &c1, p->click0, p->click1);
	if (c0 == c1 && c0 >= p->p0 && c1 <= p->p1) {
		c0 = p->p0;
		c1 = p->p1;
	}
	if (p->click_frag) {
		if (run_frag_action(p, p->click_frag, arg)) {
			p->click0 = p->click1 = -1;
			return;
		}
	}
	action(p->handler_data, p, c0, c1, mod_state & MOD_ALT);
	p->click0 = p->click1 = -1;
}

void
panel_changed(struct Panel *p, struct Change *ch)
{
	panel_changed_apply(p, ch->p0, ch->p1, ch->len);
}

void
panel_changed_apply(struct Panel *p, int64 p0, int64 p1, int64 len)
{
	int64 delta = len - (p1 - p0);
	p->cursorx = -1;
	p->layout_done = 0;
	p->last_seek_node = NULL;
	node_discard(p->doc, p0);
	if (p->orig > p0) {
		if (p->orig >= p1)
			p->orig += delta;
		else
			p->orig = p0;
	}
	
	if (p->p0 >= p0) {
		if (p->p0 >= p1)
			p->p0 += delta;
		else
			p->p0 = p0;
	}
	
	if (p->p1 >= p0) {
		if (p->p1 >= p1)
			p->p1 += delta;
		else
			p->p1 = p0 + len;
	}
	update_cursor(p);
}

void
panel_cut(struct Panel *p)
{
	int64 len = p->p1 - p->p0;
	if (len == 0)
		return;
	if (cutlen < len || cutlen > 2*len) {
		if (cutbuf)
			free(cutbuf);
		cutbuf = malloc(len);
		if (!cutbuf)
			die("out of memory\n");
	}
	cutlen = len;
	text_read(p->txt, p->p0, cutbuf, len);
	if (!is_read_only(p, p->p0, p->p1)) {
		panel_replace(p, p->p0, p->p1, "", 0);
		p->p1 = p->p0;
	}
	p->click0 = -1;
	p->cursorx = -1;
	XSetSelectionOwner(disp, xatom("CLIPBOARD"), cutwin, CurrentTime);
}

static void
_drawcursor(XftDraw *drw, int fg, int bg, int x, int y, int h)
{
	x--;
	typo_draw_rect(drw, x, y, 3, h, bg);
	typo_draw_rect(drw, x, y, 3, 3, fg);
	typo_draw_rect(drw, x + 1, y, 1, h - 3, fg);
	typo_draw_rect(drw, x, y + h - 3, 3, 3, fg);
}

static void
_drawselrange(XftDraw *drw, int ox, int oy, struct Frag *fe, char *t, int p0, int p1, int color)
{
	FontExtents curext0, curext1;
	int fonth = fe->font->extents.ascent + fe->font->extents.descent;
	int sel0, sel1;
	sel0 = -1;
	if (p1 <= fe->p)
		return;
	if (p0 <= fe->p && p1 >= fe->p + fe->len) {
		typo_draw_rect(drw, ox + fe->x, oy + fe->y, fe->w, fe->h, color);
		return;
	}
	if (p0 <= fe->p)
		sel0 = fe->p;
	else if (p0 < fe->p + fe->len)
		sel0 = p0;
	
	sel1 = p1;
	if (sel1 > fe->p + fe->len)
		sel1 = fe->p + fe->len;
	
	/* If this fragment intersects the selected range, draw selection background. */
	if (sel0 >= 0 && sel1 != sel0) {
		typo_measure(fe->font, fe->attr, t, sel0 - fe->p, &curext0);
		typo_measure(fe->font, fe->attr, t + (sel0 - fe->p), sel1 - sel0, &curext1);
		typo_draw_rect(drw, ox + fe->x + curext0.advance, oy + fe->y, curext1.advance, fonth, color);
	}
}

static bool
_cursor_in_frag(
	struct Panel *panel, XftDraw *drw,
	struct Frag *frag, char *fragtext, int64 p,
	int fg, int bg)
{
	FontExtents ext;
	int ox = panel->x + PANEL_MARGIN;
	int oy = panel->y + 2;
	int h = frag->font->extents.ascent + frag->font->extents.descent;
	if (p == frag->p) {
		_drawcursor(drw, fg, bg, ox + frag->x, oy + frag->y, h);
		return true;
	}
	if (p == frag->p + frag->len) {
		if (frag->newline)
			return 0;
		_drawcursor(drw, fg, bg, ox + frag->x + frag->w, oy + frag->y, h);
		return true;
	}
	if (p < frag->p || p - frag->p > frag->len)
		return false;
	typo_measure(frag->font, frag->attr, fragtext, p - frag->p, &ext);
	_drawcursor(drw, fg, bg, ox + frag->x + ext.advance, oy + frag->y, h);
	return true;
}

static void
_frag_draw(Surface *drw, int x, int y, struct Frag *frag, const char *buf, int len)
{
	typo_draw(drw, x + frag->x, y + frag->y + frag->font->extents.ascent,
		frag->font, frag->attr, frag->fg, buf, len);
}

void
panel_draw(struct Panel *panel, Surface *drw)
{
	char buf[DOC_TXT_MAX];
	int len;
	struct Frag *frag;
	bool cursor_done = false, eof_done = false, mark_done = false;
	
	int64 p0, p1;
	int mark0 = -1, mark1 = -1, markcol = -1;
	
	int ox = panel->x + PANEL_MARGIN;
	int oy = panel->y + 2;
	
	p0 = panel->p0;
	p1 = panel->p1;
	if (panel->replace_mode && p0 == p1) {
		int64 txtlen = text_len(panel->txt);
		p1++;
		p1 = MIN(p1, txtlen);
	}
	
	if (panel->buttons_state > 1) {
		mark0 = MIN(panel->click0, panel->click1);
		mark1 = MAX(panel->click0, panel->click1);
	}
	
	switch (panel->buttons_state) {
	case Button2:
		markcol = COL_SEL2;
		break;
	case Button3:
		markcol = COL_SEL3;
		break;
	}
	
	typo_draw_rect(drw, panel->x, panel->y, panel->w, panel->h, panel->bg);
	
	for (frag = panel->firstvisible; frag != panel->lastvisible->next; frag = frag->next) {
		FontExtents ext;
		int font_ascent;
		typo_measure_font(frag->font->fonts, frag->attr, &ext);
		font_ascent = ext.ascent;
		
		/* if this is the end of text, draw a different background color after it. */
		if (!eof_done && frag->p + frag->len == text_len(panel->txt)) {
			int right;
			if (frag->next)
				right = ox + frag->x + frag->w;
			else
				right = ox + frag->x + 1;
			
			typo_draw_rect(drw, right, oy + frag->y,
				panel->x + panel->w - right, frag->h - oy, panel->ebg);
			typo_draw_rect(drw, ox, oy + frag->y + frag->h,
				panel->w, panel->h - frag->y , panel->ebg);
			eof_done = true;
		}
		
		len = text_read(panel->txt, frag->p, buf, MIN(frag->len, sizeof(buf)-1));
		buf[len] = 0;
		
		if (frag->bg != panel->bg)
			typo_draw_rect(drw, ox + frag->x, oy + frag->y, frag->w, frag->h, frag->bg);
		if (!frag->fake) {
			_drawselrange(drw, ox, oy, frag, buf, p0, p1, COL_SEL);
			_drawselrange(drw, ox, oy, frag, buf, mark0, mark1, markcol);
		}
		
		if (frag->len > 0 && (frag->len > 1 || !isspace(buf[0])))
			_frag_draw(drw, ox, oy, frag, buf, frag->len);
		else if (frag->fake && frag->fake_text[0])
			_frag_draw(drw, ox, oy, frag,
				frag->fake_text, strnlen(frag->fake_text, sizeof(frag->fake_text)));
		
		if (frag->attr & ATR_UNDER)
			typo_draw_rect(drw, ox + frag->x, oy + frag->y + font_ascent + 1,
				frag->w, 1, frag->fg);
		if (frag->attr & ATR_STRIKE)
			typo_draw_rect(drw, ox + frag->x, oy + frag->y + (font_ascent * 2) / 3,
				frag->w, 1, frag->fg);
		
		if (!frag->fake) {
			if (!cursor_done && p1 == p0)
				cursor_done = _cursor_in_frag(panel, drw, frag, buf, p0, frag->fg, frag->bg);
			if (!mark_done && mark0 >= 0 && mark0 == mark1)
				mark_done = _cursor_in_frag(panel, drw, frag, buf, mark0, markcol, frag->bg);
		}
	}
	
	overlay_tree(panel, drw);
}

void
panel_expandsel(struct Panel *panel, int click, int64 *p0, int64 *p1)
{
	int64 b, e;
	if (panel_expandselrange(panel, click, p0, p1))
		return;
	
	b = panel_bol(panel, click);
	e = text_findrune(panel->txt, b, '\n');
	if (e < 0)
		e = text_len(panel->txt);
	if (click == b || click == e) {
		*p0 = b;
		*p1 = e;
		if (e < text_len(panel->txt))
			(*p1)++;
		return;
	}
	panel_expandselword(panel, click, p0, p1, not_word_rune);
}

static int
_expanddel(Text *txt, int64 pos, int dir, Rune sdel, Rune del, Rune esc, int escrep)
{
	Rune r;
	int64 nest = 1;
	int64 len = text_len(txt);
	if (dir < 0)
		pos = text_seek(txt, pos, -1);
	for (; pos > 0 && pos < len; pos = text_seek(txt, pos, dir)) {
		r = text_at(txt, pos);
		if (sdel != del && r == sdel) {
			nest++;
			continue;
		}
		if (r != del)
			continue;
		
		if (esc && pos > 0 && text_at(txt,  text_seek(txt, pos, -1)) == esc)
			continue;
		
		if (escrep) {
			int64 nxt;
			if (pos + dir < 0)
				return pos;
			nxt = text_seek(txt, pos, dir);
			if (nxt == len)
				return pos;
			if (text_at(txt, nxt) == del) {
				pos = nxt;
				continue;
			}
		}
		
		nest--;
		if (nest <= 0)
			return pos;
	}
	return -1;
}

int
panel_expandselrange(struct Panel *panel, int click, int64 *p0, int64 *p1)
{
	static struct {
		Rune r0, r1, esc;
		int escrep;
	} delim[] = {
		/* left,	right,	escape,	escape by repeating delimiter? */
		{'(',	')',	0,	0},
		{'[',	']',	0,	0},
		{'{',	'}',	0,	0},
		{'<',	'>',	0,	0},
		{'"',	'"',	'\\',	0},
		{'\'',	'\'',	'\\',	1},
		{0x00ab,	0x00bb,	0,	0},	/* « » */
		{0x2039,	0x203a,	0,	0},	/* ‹ › */
		{0x3008,	0x3009,	0,	0},	/* 〈 〉 */
		{0x300a,	0x300b,	0,	0},	/* 《 》 */
		{0x300c,	0x300d,	0,	0},	/* 「 」 */
		{0x3010,	0x3011,	0,	0},	/* 【 】 */
		{0x301a,	0x301b,	0,	0},	/* 〚 〛 */
	};
	Rune l, r;
	int64 b, e, i;
	
	b = panel_bol(panel, click);
	e = text_findrune(panel->txt, b, '\n');
	if (e < 0)
		e = text_len(panel->txt);
	if (e == b)
		return 0;
	
	if (click != b)
		l = text_at(panel->txt, click-1);
	else
		l = 0;
	
	if (click != e)
		r = text_at(panel->txt, click);
	else
		r = 0;
	for (i = 0; i < SIZE(delim); i++) {
		if (l == delim[i].r0) {
			*p0 = click;
			*p1 = _expanddel(panel->txt, click, 1,
				delim[i].r0, delim[i].r1, delim[i].esc, delim[i].escrep);
			if (*p1 < 0)
				*p1 = click;
			return 1;
		}
		if (r == delim[i].r1) {
			*p0 = _expanddel(panel->txt, click, -1,
				delim[i].r1, delim[i].r0, delim[i].esc, delim[i].escrep);
			*p1 = click;
			if (*p0 < 0)
				*p0 = click;
			else
				(*p0)++;
			return 1;
		}
	}
	return 0;
}

void
panel_expandselword(struct Panel *panel, int64 click, int64 *p0, int64 *p1, CharClass *boundary)
{
	int64 b, e, i;
	int64 sel0, sel1;
	b = panel_bol(panel, click);
	e = text_findrune(panel->txt, click, '\n');
	if (e < 0)
		e = text_len(panel->txt);
	
	sel0 = click;
	while (sel0 > b) {
		i = text_seek(panel->txt, sel0, -1);
		if (boundary(text_at(panel->txt, i)))
			break;
		sel0 = i;
	}
	
	sel1 = click;
	while (sel1 < e && !boundary(text_at(panel->txt, sel1)))
		sel1 = text_seek(panel->txt, sel1, 1);
	
	*p0 = sel0;
	*p1 = sel1;
}

void
panel_focus(struct Panel *p, int64 p0)
{
	struct Frag *f;
	panel_layout(p);
	f = frag_atpos(p->frags, p0);
	if (f && f->y >= 0 && f->y + 2 + f->h <= p->h)
		return;
	p->orig = panel_bol(p, p0);
	panel_layout(p);
	f = frag_atpos(p->frags, p0);
	while (f->prev && -f->y < p->h / 3)
		f = f->prev;
	p->orig = panel_bol(p, f->p);
}

void
panel_focus_in(struct Panel *panel)
{
	update_cursor(panel);
}

struct Frag*
panel_frag_at(struct Panel *p, int x, int y)
{
	struct Frag *f;
	for (f = p->firstvisible; f->next; f = f->next)
		if (f->y + f->h >= y && f->x + f->w >= x)
			break;
	return f;
}

void
panel_free(struct Panel *p)
{
	if (primary_selection_panel == p)
		primary_selection_panel = NULL;
	frag_freeall(p->frags);
	free(p);
}

int
panel_indent_spaces(struct Panel *p, int64 p0)
{
	struct DocNode *n;
	if (p->indent_spaces)
		return p->indent_spaces;
	n = parse_at(p->txt, &p->parse_opts, p->doc, p0);
	if ((n->attr & ATR_SPACES) && n->tabw > 0)
		return n->tabw;
	return 0;
}

void
_key_bs(struct Panel *panel, int mod_state, Time time, int arg)
{
	int64 p;
	if (panel->replace_mode || is_read_only(panel, panel->p0, panel->p1)) {
		panel_navh(panel, -1);
		return;
	}
	panel_replace(panel, panel->p0, panel->p1, "", 0);
	if (panel->p0 > 0) {
		p = panel->p0;
		panel_navh(panel, -1);
		panel_focus(panel, panel->p0);
		if (is_read_only(panel, panel->p0, p))
			return;
		user_replace(panel, panel->p0, p, "", 0);
	}
	panel->p1 = panel->p0;
}

void
_key_del(struct Panel *panel, int mod_state, Time time, int arg)
{
	if (panel->p0 != panel->p1) {
		panel_cut(panel);
		panel->last_edit0 = panel->last_edit1 = -1;
		return;
	}
	if (panel->last_edit0 < 0)
		return;
	panel->p0 = panel->last_edit0;
	panel->p1 = panel->last_edit1;
}

void
_key_insert(struct Panel *panel, int mod_state, Time time, int arg)
{
	panel->replace_mode = !panel->replace_mode;
}

void
_key_paste(struct Panel *panel, int mod_state, Time time, int arg)
{
	panel_paste(panel, xatom("CLIPBOARD"));
}

void
_key_ret(struct Panel *panel, int mod_state, Time time, int arg)
{
	if (is_read_only(panel, panel->p0, panel->p1)) {
		panel_navv(panel, 1);
		return;
	}
	panel_newline(panel);
}

void
_key_tab(struct Panel *panel, int mod_state, Time time, int arg)
{
	int i;
	if (is_read_only(panel, panel->p0, panel->p1))
		return;
	i = panel_indent_spaces(panel, panel->p0);
	if (i > 0) {
		char b[128];
		i = MIN(i, sizeof(b));
		memset(b, ' ', i);
		user_ins(panel, b, i);
		return;
	}
	user_ins(panel, "\t", 1);
}

void
_nav_del_word(struct Panel *panel, int mod_state, Time time, int arg)
{
	int64 p;
	if (panel->p0 != panel->p1)
		return;
	p = panel->p0;
	panel_focus(panel, panel->p0);
	_nav_word(panel, 0, time, -1);
	if (is_read_only(panel, panel->p0, p))
		return;
	user_replace(panel, panel->p0, p, "", 0);
}

void
_nav_end(struct Panel *panel, int mod_state, Time time, int arg)
{
	if (panel->disable_scrolling)
		return;
	if (text_len(panel->txt) == 0)
		return;
	panel_focus(panel, panel_bol(panel, text_len(panel->txt) - 1));
}

void
_nav_h(struct Panel *panel, int mod_state, Time time, int arg)
{
	panel_navh(panel, arg);
	panel_focus(panel, panel->p0);
}

void
_nav_home(struct Panel *panel, int mod_state, Time time, int arg)
{
	if (panel->disable_scrolling)
		return;
	panel->orig = 0;
}

void
_nav_line_bol(struct Panel *panel, int mod_state, Time time, int arg)
{
	struct Frag *f;
	panel_focus(panel, panel->p0);
	f = frag_line_start(frag_atpos(panel->frags, panel->p0));
	panel->p0 = panel->p1 = f->p;
}

void
_nav_line_delb(struct Panel *panel, int mod_state, Time time, int arg)
{
	int64 p;
	panel_focus(panel, panel->p0);
	p = panel_bol(panel, panel->p0);
	if (panel->p0 == panel->p1 && panel->p0 == p)
		return;
	if (is_read_only(panel, p, panel->p1))
		return;
	panel->p0 = p;
	user_replace(panel, panel->p0, panel->p1, "", 0);
	panel->p1 = panel->p0;
}

void
_nav_line_delf(struct Panel *panel, int mod_state, Time time, int arg)
{
	struct Frag *f;
	int64 p;
	panel_focus(panel, panel->p1);
	f = frag_line_end(frag_atpos(panel->frags, panel->p1));
	p = f->p + f->len;
	if (panel->p0 == panel->p1 && panel->p0 == p)
		return;
	if (is_read_only(panel, panel->p0, p))
		return;
	panel->p1 = p;
	user_replace(panel, panel->p0, p, "", 0);
	panel->p1 = panel->p0;
}

void
_nav_line_eol(struct Panel *panel, int mod_state, Time time, int arg)
{
	struct Frag *f;
	panel_focus(panel, panel->p1);
	f = frag_line_end(frag_atpos(panel->frags, panel->p1));
	panel->p0 = panel->p1 = f->p + f->len;
}

void
_nav_pd(struct Panel *panel, int mod_state, Time time, int arg)
{
	struct Frag *of;
	if (panel->disable_scrolling)
		return;
	panel_layout(panel);
	of = panel->lastvisible;
	of = of->next ? of->next : of;
	panel_seek(panel, of->p);
}

void
_nav_pu(struct Panel *panel, int mod_state, Time time, int arg)
{
	struct Frag *f;
	if (panel->disable_scrolling)
		return;
	panel_layout_from_top(panel);
	for (f = panel->firstvisible; f->prev; f = f->prev)
		if (f->y < -panel->h)
			break;
	panel_seek(panel, f->next->p);
}

void
_nav_v(struct Panel *panel, int mod_state, Time time, int arg)
{
	panel_navv(panel, arg);
}

static bool
_not_word_or_nl(Rune r)
{
	return !isalnumrune(r) && r != '_' && r != '\n';
}

void
_nav_word(struct Panel *panel, int mod_state, Time time, int dir)
{
	Text *tx = panel->txt;
	int64 p0 = panel->p0;
	assert(dir != 0);
	if (next_is(tx, p0, _not_word_or_nl, dir))
		p0 = seek_past(tx, p0, _not_word_or_nl, dir);
	p0 = seek_past(tx, p0, word_rune, dir);
	panel->p0 = panel->p1 = p0;
}

void
_shift_nav_before(struct Panel *p)
{
	if (p->click0 >= 0 && p->click1 >= 0) {
		p->p0 = p->p1 = p->click1;
		return;
	}
	p->click0 = p->p0;
	p->click1 = p->p1;
	p->p0 = p->p1;
}

void
_shift_nav_after(struct Panel *p)
{
	p->click1 = p->p0;
	order_sel(&p->p0, &p->p1, p->click0, p->click1);
}

void
_shift_nav_exit(struct Panel *p)
{
	p->click0 = p->click1 = -1;
}

static bool
_keycmd(struct Panel *panel, XKeyEvent *e, KeySym ksym)
{
	int i;
	for (i = 0; i < SIZE(key_commands); i++) {
		struct KeyCommandConf *cmd = &key_commands[i];
		bool shift_nav = false;
		if (ksym != cmd->ksym || (e->state & cmd->mod) != cmd->mod)
			continue;		
		if (!cmd->fn)
			return true;
		
		shift_nav = cmd->shift_nav && (e->state & ShiftMask);
		if (shift_nav)
			_shift_nav_before(panel);
		else
			_shift_nav_exit(panel);
		cmd->fn(panel, e->state, e->time, cmd->arg);
		if (shift_nav)
			_shift_nav_after(panel);
		update_cursor(panel);
		return true;
	}
	
	switch (ksym) {
	case XK_Control_L:	case XK_Control_R:	case XK_Alt_L:	case XK_Alt_R:
	case XK_Shift_L:	case XK_Shift_R:	case XK_Meta_L:	case XK_Meta_R:
	case XK_Super_L:	case XK_Super_R:	case XK_Hyper_L:	case XK_Hyper_R:
	case XK_Escape:
		return true;
	default:
		break;
	}
	return false;
}

void
panel_key(struct Panel *panel, XKeyEvent *e)
{
	char buf[256];
	KeySym ksym = NoSymbol;
	int len;
	if (e->type == KeyRelease)
		return;
	len = xim_key(panel->xim, e, buf, sizeof(buf), &ksym);
	if (ksym != NoSymbol)
		if (_keycmd(panel, e, ksym))
			return;
	
	if (len == 0)
		return;
	if (is_read_only(panel, panel->p0, panel->p1))
		update_cursor(panel);
	else
		user_ins(panel, buf, len);
}

void
panel_layout(struct Panel *p)
{
	struct Layout l = {};
	if (p->layout_done && p->layout_orig == p->orig)
		return;
	l.w = p->w - PANEL_MARGIN;
	l.h = p->h - 2;
	l.start_at = p->orig;
	l.parse_opts = &p->parse_opts;
	l.fonts[0] = p->fonts[0];
	l.fonts[1] = p->fonts[1];
	
	l.txt = p->txt;
	l.doc = p->doc;
	
	l.frags = p->frags;
	l.lastfrag = l.frags;
	l.overflow = 0;
	
	layout_perform(&l);
	
	p->frags = l.frags;
	p->firstvisible = l.firstvisible;
	p->lastvisible = l.lastvisible;
	p->lastfrag = l.lastfrag;
	p->overflow = l.overflow;
	
	p->layout_orig = p->orig;
	p->layout_done = 1;
}

void
panel_layout_from_top(struct Panel *p)
{
	panel_layout(p);
}

/* panel_lnscroll scrolls the panel one line up or down. */
void
panel_lnscroll(struct Panel *p, int n)
{
	struct Frag *f;
	if (n < 0) {
		if (p->orig == 0)
			return;
		p->orig = frag_line_prev(p->firstvisible)->p;
		return;
	}
	
	for (f = p->firstvisible; f->next && f->next->next; f = f->next) {
		if (f->y > p->firstvisible->y)
			break;
	}
	if (f->p >= text_len(p->txt)) {
		p->orig = text_len(p->txt);
		return;
	}
	p->orig = f->p;
}

int
panel_motion(struct Panel *p, XMotionEvent *ev)
{
	int64 c;
	if (p->buttons_state == 0 || p->buttons_state == ButtonChord)
		return 0;
	c = panel_atwinpos(p, ev->x, ev->y, NULL);
	if (c == p->click1)
		return 0;
	p->click1 = c;
	p->click_frag = NULL;
	if (p->buttons_state != Button1)
		return 1;
	order_sel(&p->p0, &p->p1, p->click0, p->click1);
	return 1;
}

static int64
_seek_frag(struct Panel *panel, int64 pos, int d)
{
	struct Frag *f;
	Text *txt = panel->txt;
	int64 np;
	int indent;
	
	np = text_seek(txt, pos, d);
	f = frag_atpos(panel->frags, pos);
	if (f->p <= np && np <= f->p + f->len) {
		/* destination is in the same fragment */
	} else if (d < 0) {
		do {
			f = f->prev;
		} while (f->prev && f->fake);
		if (f->len > 0)
			np = text_seek(txt, f->p + f->len, -1);
		else
			np = f->p;
	} else {
		do {
			f = f->next;
		} while (f->next && f->fake);
		if (f->len > 0)
			np =  text_seek(txt, f->p, 1);
		else
			np =  f->p;
	}
	
	indent = panel->indent_spaces ? panel->indent_spaces : f->indent_spaces;
	if (indent && f->is_indent) {
		np -= (np - f->p) % indent;
		if (d > 0)
			np += indent;
		return np;
	}
	
	if (f->newline && np == f->p + f->len) {
		do {
			f = f->next;
		} while (f->next && f->fake);
		return f->p;
	}
	return np;
}

void
panel_navh(struct Panel *panel, int d)
{
	int p;
	assert(d == -1 || d == 1);
	if (d < 0 && panel->p0 == 0) {
		panel->p1 = 0;
		return;
	}
	if (d > 0 && panel->p1 == text_len(panel->txt)) {
		panel->p0 = panel->p1;
		return;
	}
	panel->cursorx = -1;
	
	p = (d < 0) ? panel->p0 : panel->p1;
	panel_focus(panel, p);
	panel_layout(panel);
	panel->p0 = panel->p1 = _seek_frag(panel, p, d);
}

void
panel_navv(struct Panel *p, int d)
{
	struct Frag *f;
	int64 p0;
	int scroll;
	assert(d != 0);
	
	f = frag_atpos(p->firstvisible, p->p0);
	if (!f) {
		panel_focus(p, p->p0);
		panel_layout(p);
		f = frag_atpos(p->firstvisible, p->p0);
	}
	
	if (d > 0 && f->y + f->h > p->lastvisible->y)
		scroll = 1;
	else if (d < 0 && f->y <= p->firstvisible->y)
		scroll = -1;
	else
		scroll = 0;
	
	if (scroll) {
		if (p->disable_scrolling)
			return;
		panel_lnscroll(p, scroll);
		panel_layout(p);
		f = frag_atpos(p->firstvisible, p->p0);
	}
	
	if (p->cursorx < 0)
		p->cursorx = getcurx(p->txt, f, p->p0);
	
	if (d < 0)
		p0 = panel_atpos(p, p->cursorx + 1, f->y - 1, &f);
	else
		p0 = panel_atpos(p, p->cursorx + 1, f->y + f->h + 1, &f);
	
	if (f->fake)
		while (f->fake && f->prev)
			f = f->prev;
	if (f->indent_spaces && f->is_indent)
		p0 -= (p0 - f->p) % f->indent_spaces;
	
	if (p0 >= 0)
		p->p0 = p->p1 = p0;
	else
		fprintf(stderr, "BUG: vertical navigation: don't know where to go.\n");
	panel_focus(p, p->p0);
}

struct Panel*
panel_new(Text *t, FontList *font_default, FontList *font_mono, int fg, int bg)
{
	struct Panel *p = malloc(sizeof(*p));
	if (!p)
		die("out of memory\n");
	memset(p, 0, sizeof(*p));
	p->w = 0;
	p->h = 0;
	p->fonts[0] = font_default;
	p->fonts[1] = font_mono;
	p->fg = fg;
	p->ebg = p->bg = bg;
	p->frags = malloc(sizeof(*p->frags));
	memset(p->frags, 0, sizeof(*p->frags));
	p->txt = t;
	if (!t) {
		p->txt = text_new();
		p->doc = calloc(1, sizeof(*p->doc));
		p->doc->p0 = p->doc->p1 = 0;
		p->doc->type = DOC_ROOT;
		p->doc->fg = p->fg;
		p->doc->bg = p->bg;
	}
	p->last_edit0 = p->last_edit1 = -1;
	
	return p;
}

static int
_indent(const char *b, int len)
{
	int n, i = 0;
	const char *c = b, *end = b + len;
	Rune r;
	while (c < end) {
		n = charntorune(&r, c, len - i);
		if (!isspacerune(r) || r == '\n')
			return c - b;
		c += n;
	}
	return len;
}

void
panel_newline(struct Panel *p)
{
	int64 pb, len, off;
	char *line;
	char buf[2048];
	pb = panel_bol(p, p->p0);
	off = p->p0 - pb;
	
	line = text_ref(p->txt, pb, p->p0);
	len = _indent(line, off);
	if (len > sizeof(buf)) {
		user_ins(p, "\n", 1);
		return;
	}
	memmove(buf, line, len);
	
	if (p->indent_trim && len == off)
		p->p0 = pb;
	user_ins(p, "\n", 1);
	user_ins(p, buf, len);
}

struct DocNode*
panel_parse_at(struct Panel *p, int64 i)
{
	struct DocNode *ls = p->last_seek_node;
	struct DocNode *n;
	if (!ls)
		return parse_at(p->txt, &p->parse_opts, p->doc, i);
	if (ls->n1 < i)
		return parse_at(p->txt, &p->parse_opts, ls, i);
	for (n = ls; n->n0 > i; n = n->prev) {}
	return n;
}

struct DocNode*
panel_parse_next(struct Panel *p, struct DocNode *n)
{
	struct DocNode *next = parse_next(p->txt, &p->parse_opts, n);
	assert(next != NULL);
	return next;
}

void
panel_paste(struct Panel *p, Atom clipboard)
{
	if (is_read_only(p, p->p0, p->p1))
		return;
	if (clipboard != XA_PRIMARY && cutlen != 0) {
		int64 pos = p->p0;
		panel_replace(p, p->p0, p->p1, cutbuf, cutlen);
		p->p0 = pos;
		return;
	}
	p->paste->from = clipboard;
	p->paste->panel = p;
	p->paste->at = p->paste->p0 = p->p0;
	panel_replace(p, p->p0, p->p1, "", 0);
	XDeleteProperty(disp, p->paste->win, clipboard);
	XConvertSelection(disp, clipboard, xatom("UTF8_STRING"), clipboard, p->paste->win, CurrentTime);
}

static void
_replace(struct Panel *p, int64 p0, int64 p1, const char *str, int64 len)
{
	struct Change *c;
	char *from;
	
	if (!p->onchange) {
		text_change(p->txt, p0, p1, str, len);
		panel_changed_apply(p, p0, p1, len);
		return;
	}
	
	c = malloc(sizeof(*c));
	from = strdup(text_ref(p->txt, p0, p1));
	if (!from || !c)
		die("out of memory\n");
	memset(c, 0, sizeof(*c));
	
	c->source = SOURCE_TYPED;
	c->from = from;
	c->to = strndup(str, len);
	c->p0 = p0;
	c->p1 = p1;
	c->len = len;
	
	p->onchange(p->handler_data, p, c);
	panel_changed(p, c);
}

void
panel_replace(struct Panel *p, int64 p0, int64 p1, const char *str, int64 len)
{
	if (p1 < p->last_edit0) {
		int64 delta = len - (p1 - p0);
		p->last_edit0 += delta;
		p->last_edit1 += delta;
	} else if (p0 < p->last_edit1)
		p->last_edit0 = p->last_edit1 = -1;
	_replace(p, p0, p1, str, len);
}

void
panel_resize(struct Panel *p, int w, int h)
{
	p->w = w;
	p->h = h;
	p->layout_done = 0;
}

void
panel_rewrite_node(struct Panel *p, struct DocNode *n)
{
	Text *t = text_new();
	node_write(p->txt, n->prev, n, t);
	panel_replace(p, n->n0, n->n1, text_all(t), text_len(t));
	text_free(t);
}

int
panel_search(struct Panel *p, int64 p0, int64 p1, char *s, int64 len)
{
	char *all, *f, *tg;
	tg = strndup(s, len);
	all = text_all(p->txt);
	f = utfutf(all + p1, tg);
	if (!f)
		f = utfutf(all, tg);
	if (f) {
		struct DocNode *n;
		p->p0 = f - all;
		p->p1 = p->p0 + len;
		p->cursorx = -1;
		
		n = panel_parse_at(p, p->p0);
		unfold_node(p, n);
	}
	free(tg);
	return !!f;
}

int
panel_search_rev(struct Panel *p, int64 p0, int64 p1, char *s, int64 len)
{
	struct DocNode *n;
	char *all, *tg, *f = NULL, *found = NULL;
	if (len == 0) {
		p->p1 = p->p0 = p0;
		p->cursorx = -1;
		return 1;
	}
	tg = strndup(s, len);
	all = text_all(p->txt);
	
	f = utfutf(all, tg);
	if (!f) {
		free(tg);
		return 0;
	}
	while (f - all + len < p0) {
		found = f;
		f = utfutf(f + len, tg);
	}
	if (!found)
		do {
			found = f;
			f = utfutf(f + len, tg);
		} while (f);
	p->p0 = found - all;
	p->p1 = p->p0 + len;
	p->cursorx = -1;
	
	n = panel_parse_at(p, p->p0);
	unfold_node(p, n);
	
	free(tg);
	return 1;
}

static int
regexec_last(regex_t *re, const char *data, size_t n, regmatch_t *match, int flags)
{
	regmatch_t found;
	int err;
	const char *s;
	regoff_t len;
	assert(n > 0);
	assert(match != NULL);
	
	s = data;
	len = match[0].rm_eo;
	err = regexec(re, s, n, match, flags);
	if (err)
		return err;
	
	/* POSIX doesn't say if regexec is allowed to clobber submatches when no match is found.
	 * E-mail me if you find an implementation that does.
	 */
	do {
		found = match[0];
		found.rm_so += s - data;
		found.rm_eo += s - data;
		s = data + found.rm_eo;
		if (flags & REG_STARTEND) {
			match[0].rm_so = 0;
			match[0].rm_eo = len - (s - data);
		}
	} while (!regexec(re, s, n, match, flags));
	match[0] = found;
	return 0;
}

static int
_search_regex(struct Panel *p, const char *str, int64 len)
{
	char re_buf[256];
	regex_t re;
	regmatch_t lim;
	int err = 0;
	char *all, *addr;
	bool rev = false;
	
	if (len >= sizeof(re_buf)) {
		fprintf(stderr, "regex too long\n");
		return 0;
	}
	
	all = text_all(p->txt);
	strncpy(re_buf, str, len);
	re_buf[len] = 0;
	
	switch (re_buf[0]) {
	case '/':
		rev = false;
		break;
	case '?':
		rev = true;
		break;
	default:
		fprintf(stderr, "invalid regex\n");
		return 0;
	}
	
	if (re_buf[len-1] == re_buf[0] && re_buf[len-2] != '\\')
		len--;
	addr = re_buf + 1;
	
	lim.rm_so = 0;
	lim.rm_eo = text_len(p->txt);
	
	err = regcomp(&re, addr, REG_EXTENDED | REG_NEWLINE);
	if (err) {
		char errb[256];
		regerror(err, &re, errb, sizeof(errb));
		fprintf(stderr, "invalid regex: %s\n", errb);
		return 0;
	}
	
	if (rev)
		err = regexec_last(&re, all, 1, &lim, REG_STARTEND);
	else
		err = regexec(&re, all, 1, &lim, REG_STARTEND);
	if (err)
		return 0;
	p->p0 = lim.rm_so;
	p->p1 = lim.rm_eo;
	return 1;
}

static int
_search_line(struct Panel *p, char *str, int64 len)
{
	int64 n;
	{
		char buf[24];
		char *e;
		if (len > sizeof(buf)-1)
			return 0;
		memmove(buf, str, len);
		buf[len] = 0;
		n = strtol(str, &e, 10);
		if (*e)
			return 0;
	}
	{
		int64 i = -1, last = 0;
		while (n > 0) {
			last = i + 1;
			i = text_findrune(p->txt, last, '\n');
			if (i < 0)
				return 0;
			n--;
		}
		p->p0 = last;
		p->p1 = i + 1;
	}
	return 1;
}

int
panel_searchaddr(struct Panel *p, int64 p0, int64 p1, char *str, int64 len)
{
	if (len < 1)
		return 0;
	if (isdigit(str[0]))
		return _search_line(p, str, len);
	return _search_regex(p, str, len);
}

/* panel_seek sets the panel's origin to a valid position near the given index. */
void
panel_seek(struct Panel *p, int64 pos)
{
	struct DocNode *n = panel_parse_at(p, pos);
	while (!node_hastext(n) && n->type != DOC_EOF)
		n = panel_parse_next(p, n);
	if (n->type == DOC_EOF)
		p->orig = panel_bol(p, n->n0);
	else
		p->orig = panel_bol(p, n->p0);
	p->last_seek_node = n;
}

void
panel_select(struct Panel *p, int64 p0, int64 p1)
{
	panel_select_time(p, p0, p1, CurrentTime);
}

void
panel_select_time(struct Panel *p, int64 p0, int64 p1, Time time)
{
	p->p0 = p0;
	p->p1 = p1;
	if (p0 == p1) {
		if (primary_selection_panel == p)
			XSetSelectionOwner(disp, XA_PRIMARY, None, time);
		return;
	}
	primary_selection_panel = p;
	XSetSelectionOwner(disp, XA_PRIMARY, cutwin, time);
}

void
panel_showplain(struct Panel *p, bool plain)
{
	if (!!p->parse_opts.plain == !!plain)
		return;
	p->parse_opts.plain = !!plain;
	node_freelist(p->doc->next);
	p->doc->next = NULL;
	p->layout_done = 0;
}

void
panel_shrinkwrap(struct Panel *p, int maxh)
{
	struct Frag *f;
	p->h = maxh;
	panel_layout(p);
	for (f = p->frags; f->next; f = f->next) {}
	p->h = f->y + f->h + 3;
	p->layout_done = 0;
}

int64
seek_past(Text *tx, int64 p, CharClass *class, int dir)
{
	int64 len = text_len(tx);
	while ((dir > 0 || p > 0) && (dir < 0 || p < len)) {
		p = text_seek(tx, p, dir);
		if (!next_is(tx, p, class, dir))
			return p;
	}
	return p;
}

void
unfold_node(struct Panel *p, struct DocNode *node)
{
	struct DocNode *n, *parent;
	for (n = node; n->type != DOC_ROOT; n = parent) {
		parent = n->parent;
		assert(parent->type > DOC_NONE && parent->type <= DOC_EOF);
		if (n->type != DOC_TREE_BRANCH)
			continue;
		if (n->attr & ATR_FOLD) {
			n->attr &= ~ATR_FOLD;
			panel_rewrite_node(p, n);
		}
	}
}

void
update_cursor(struct Panel *p)
{
	struct Frag *f;
	if (!xim_need_cursor(p->xim))
		return;
	if (p->p0 < p->firstvisible->p || p->p0 >= p->lastvisible->p + p->lastvisible->len)
		return;
	if (!(f = frag_atpos(p->firstvisible, p->p0)))
		return;
	xim_set_cursor(p->xim, p->x + getcurx(p->txt, f, p->p0), p->y + f->y);
}

void
user_ins(struct Panel *p, const char *str, int64 len)
{
	int64 p0 = p->p0, p1 = p->p1;
	if (p->replace_mode && p0 == p1)
		if (p0 < text_len(p->txt))
			p1 = p0 + 1;
	user_replace(p, p0, p1, str, len);
	p->p0 = p->p1;
}

void
user_replace(struct Panel *p, int64 p0, int64 p1, const char *str, int64 len)
{
	assert(p0 <= p1);
	if (p->last_edit0 < 0) {
		p->last_edit0 = p0;
		p->last_edit1 = p0 + len;
	} else {
		p->last_edit0 = MIN(p->last_edit0, p0);
		p->last_edit1 = MAX(p->last_edit1, p1) + len - (p1 - p0);
	}
	_replace(p, p0, p1, str, len);
}
