#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utf.h>

#include "def.h"
#include "lex.h"
#include "text.h"
#include "parse.h"

static int	node_level(int type);

static struct colname {const char *s; int c;} colnames[] = {
	{"1",	COL_1},	{"2",	COL_2},	{"3",	COL_3},
	{"R",	COL_R},	{"R+",	COL_LR},
	{"Y",	COL_Y},	{"Y+",	COL_LY},
	{"G",	COL_G},	{"G+",	COL_LG},
	{"C",	COL_C},	{"C+",	COL_LC},
	{"B",	COL_B},	{"B+",	COL_LB},
	{"M",	COL_M},	{"M+",	COL_LM},
	
	{"O",	COL_O},	{"O+",	COL_LO},
};

static char*
_nextbreak(char *str, char *end)
{
	int len;
	Rune r;
	while (str != end) {
		len = charntorune(&r, str, end - str);
		if (len == 0 || r == CMD_RUNE || r == '\t' || r == '\n')
			return str;
		if (r == ' ') {
			int sl = charntorune(&r, str + len, end - (str + len));
			if (sl && r == ' ')
				return str;
		}
		str += len;
	}
	return end;
}

struct DocNode*
node_after(struct DocNode *root, int64 p)
{
	struct DocNode *n = root;
	while (n && n->type != DOC_EOF && n->n0 <= p)
		n = n->next;
	return n;
}

void
node_discard(struct DocNode *root, int64 p)
{
	struct DocNode *n;
	for (n = root; n->next; n = n->next) {
		if (n->next->type == DOC_EOF) {
			node_free(n->next);
			n->next = NULL;
			return;
		}
		if (n->next->p1 >= p || n->next->n1 > p) {
			node_freelist(n->next);
			n->next = NULL;
			return;
		}
	}
}

void
node_free(struct DocNode *n)
{
	free(n);
}

void
node_freelist(struct DocNode *n)
{
	struct DocNode *nx;
	while (n) {
		nx = n->next;
		free(n);
		n = nx;
	}
}

int
node_hastext(struct DocNode *n)
{
	return n->p0 != n->p1;
}

int
node_in_section(struct DocNode *n)
{
	return n
		&& node_level(n->type) < node_level(DOC_SECTION)
		&& n->type != DOC_TREE_BRANCH
		&& n->type != DOC_TREE_UP && n->type != DOC_TREE_DOWN
		&& n->type != DOC_EOF
		;
}

int
node_lasttab(Text *txt, struct DocOpts *opts, struct DocNode *node)
{
	struct DocNode *n;
	int islast;
	if (node->layout == LAYOUT_VERBATIM)
		return 1;
	if (node->lasttab != 0)
		return node->lasttab > 0;
	for (n = node; n->type == DOC_TXT; n = parse_next(txt, opts, n)) {}
	if (n->type == DOC_TAB || n->type == DOC_SPACES)
		islast = -1;
	else
		islast = 1;
	for (n = node; n->type == DOC_TXT; n = n->next)
		n->lasttab = islast;
	return islast > 0;
}

int
node_level(int type)
{
	switch (type) {
	case DOC_ROOT:
		return 1000;
	case DOC_TREE_BRANCH:
		return 300;
	case DOC_TREE_UP:
	case DOC_TREE_DOWN:
	case DOC_END:
		return 250;
	case DOC_TREE_LEAF:
	case DOC_SECTION:
		return 200;
	case DOC_TXT:
	case DOC_SPACES:
	case DOC_TAB:
	case DOC_NL:
	case DOC_ATTR:
	case DOC_COL:
	case DOC_PROMPT:
	case DOC_CMDESC:
		return 100;
	case DOC_LINK:
		return 90;
	case DOC_ERROR:
	case DOC_EOF:
		return 1;
	default:
		die("node_level: invalid document node type\n");
		return -1;
	}
}

int
cmp_level(struct DocNode *a, struct DocNode *b)
{
	int la, lb;
	if (a->depth != b->depth)
		return a->depth > b->depth ? -1 : 1;
	la = node_level(a->type);
	lb = node_level(b->type);
	if (la == lb)
		return 0;
	return la > lb ? 1 : -1;
}

struct DocNode*
node_new_root(void)
{
	struct DocNode *n = calloc(1, sizeof(*n));
	if (!n)
		die("out of memory\n");
	n->type = DOC_ROOT;
	n->attr = ATR_OUTLINE;
	n->fg = COL_FG;
	n->bg = COL_BG;
	return n;
}

int
node_plain_text(char *dst, int64 maxlen, Text *txt, struct DocNode *n)
{
	int64 len;
	if (maxlen <= 0)
		return 0;
	if (!n->visible)
		return 0;
	if (n->type == DOC_CMDESC)
		len = runelen(CMD_RUNE);
	else
		len = n->p1 - n->p0;
	len = text_read(txt, n->p0, dst, MIN(len, maxlen-1));
	dst[len] = 0;
	return len;
}

static int
_encode_flags(char *buf, int flags)
{
	int i, len;
	static const struct {int f; char c;} flag_vals[] = {
		{ATR_BOLD,	'B'},
		{ATR_FOLD,	'F'},
		{ATR_ITALIC,	'I'},
		{ATR_MONO,	'M'},
		{ATR_OUTLINE,	'o'},
		{ATR_RDONLY,	'R'},
		{ATR_SWAP,	'S'},
		{ATR_UNDER,	'U'},
		{ATR_STRIKE,	'X'},
	};
	len = 0;
	for (i = 0; i < SIZE(flag_vals); i++)
		if (flags & flag_vals[i].f)
			buf[len++] = flag_vals[i].c;
	return len;
}

static int
_encode_tag(char *buf, const char *tag, int set, int clear)
{
	int tl = 0, len = 0;
	int r = CMD_RUNE;
	if (!tag && !set && !clear)
		return 0;
	if (tag)
		tl = strlen(tag);
	len += runetochar(buf + len, &r);
	memmove(buf + len, tag, tl);
	len += tl;
	if (set) {
		buf[len++] = '+';
		len += _encode_flags(buf + len, set);
	}
	if (clear) {
		buf[len++] = '-';
		len += _encode_flags(buf + len, clear);
	}
	return len;
}

static int
_encode_color(char *buf, int c)
{
	const char *name = NULL;
	int len = 1, sl, i;
	buf[0] = ' ';
	for (i = 0; i < SIZE(colnames); i++)
		if (colnames[i].c == c)
			name = colnames[i].s;
	if (!name)
		die("node_write: invalid color value %s\n", c);
	sl = strlen(name);
	memmove(buf + len, name, sl);
	len += sl;
	return len;
}

static int
_encode_color_tag(char *buf, int set, int clear, int color)
{
	int cmd = CMD_RUNE, space = ' ';
	int len = 0;
	len += _encode_tag(buf + len, "C", set, clear);
	len += runetochar(buf + len, &space);
	len += _encode_color(buf + len, color);
	len += runetochar(buf + len, &cmd);
	return len;
}

void
node_write(Text *text, struct DocNode *context, struct DocNode *n, Text *output)
{
	char buf[DOC_TXT_MAX];
	struct DocNode no_node = { .attr = 0, .fg = COL_FG, .bg = COL_BG };
	int flags_set, flags_cleared;
	int len, r;
	if (context)
		while (context && cmp_level(context, n) < 0)
			context = context->parent;
	if (!context)
		context = &no_node;
	
	flags_set = n->attr & ~context->attr;
	flags_cleared = (~n->attr) & context->attr;
	if (n->fg != context->fg) {
		len = _encode_color_tag(buf,
			flags_set & ATRS_MASK, flags_cleared & ATRS_MASK, n->fg);
		text_append(output, buf, len);
	}
	
	switch (n->type) {
	case DOC_TXT:
	case DOC_TAB:
	case DOC_SPACES:
		len = 0;
		len += _encode_tag(buf + len, NULL, flags_set, flags_cleared);
		text_append(output, buf, len);
		
		len = 0;
		len += text_read(text, n->n0, buf + len, n->n1 - n->n0);
		text_append(output, buf, len);
		return;
	
	case DOC_NL:
	case DOC_END:
		text_append(output, "\n", 1);
		return;
	
	case DOC_ATTR:
		len = 0;
		len += _encode_tag(buf + len, NULL, flags_set, flags_cleared);
		text_append(output, buf, len);
		return;
	
	case DOC_COL:
		return;
	
	case DOC_TREE_BRANCH:
		len = 0;
		if (n->attr & ATR_FOLD)
			flags_set |= ATR_FOLD;
		flags_cleared &= ~ATR_FOLD;
		len += _encode_tag(buf + len, "TB", flags_set, flags_cleared);
		r = CMD_RUNE;
		len += runetochar(buf + len, &r);
		text_append(output, buf, len);
		return;
	
	default:
		die("node_write: unknown node type %d\n", n->type);
	}
}

struct DocNode*
parse_at(Text *txt, struct DocOpts *opts, struct DocNode *root, int64 p)
{
	struct DocNode *n;
	for (n = root; n->next; n = n->next) {
		if (n->next->n1 > p)
			return n->next;
	}
	if (n->type == DOC_EOF)
		return n;
	do {
		n = parse_next(txt, opts, n);
	} while (n->type != DOC_EOF && p >= n->n1);
	return n;
}

static void
set_parent(struct DocNode *node, struct DocNode *last)
{
	struct DocNode *n;
	int level;
	assert(!node->parent);
	assert(node->type != DOC_NONE);
	assert(last->type != DOC_NONE);
	
	node->depth = last->depth;
	switch (node->type) {
	case DOC_TREE_DOWN:
		node->depth++;
		break;
	case DOC_TREE_UP:
		if (node->depth > 0)
			node->depth--;
		break;
	}
	
	level = cmp_level(last, node);
	if (level > 0) {
		node->parent = last;
		if (last->type == DOC_TREE_BRANCH)
			node->depth++;
		return;
	}
	if (level == 0) {
		node->parent = last->parent;
		return;
	}
	
	n = last;
	do {
		n = n->parent;
	} while (cmp_level(n, node) <= 0);
	node->parent = n;
}

static void
inherit_from(struct DocNode *node, struct DocNode *from)
{
	struct DocNode *prev = node->prev;
	struct DocNode *base = prev;
	
	if (from)
		base = from;
	else
		while (cmp_level(base, node) < 0)
			base = base->parent;
	
	node->layout = base->layout;
	node->tabw = base->tabw;
	node->attr = base->attr;
	node->fg = base->fg;
	node->bg = base->bg;
	
	if (prev->type == DOC_TREE_BRANCH)
		node->attr |= ATR_CONTROL;
	if (node->type == DOC_END)
		node->attr &= ~ATR_CONTROL;
	
	if (base == node->parent) {
		if (base->attr & ATR_FOLD && node->type != DOC_END) {
			node->attr &= ~ATR_FOLD;
			node->attr |= ATR_HIDE;
		}
	}
}

static struct DocNode*
error_node(struct DocNode *node, struct DocNode *last)
{
	node->type = DOC_ERROR;
	node->prev = last;
	if (!node->parent)
		set_parent(node, last);
	inherit_from(node, NULL);
	node->visible = 1;
	node->p1 = node->p0 = node->n0;
	return node;
}

static struct DocNode*
visible_node(struct DocNode *node, int type, int64 len, struct DocNode *last)
{
	node->type = type;
	node->visible = 1;
	node->n1 = node->n0 + len;
	node->p0 = node->n0;
	node->p1 = node->n1;
	set_parent(node, last);
	inherit_from(node, NULL);
	return node;
}

static void
parse_flags(Lex *lex, struct DocNode *node, int *flags, int allow)
{
	static int attrname[] = {
		['B'] = ATR_BOLD,
		['F'] = ATR_FOLD,
		['I'] = ATR_ITALIC,
		['M'] = ATR_MONO,
		['o'] = ATR_OUTLINE,
		['R'] = ATR_RDONLY,
		['S'] = ATR_SWAP,
		['U'] = ATR_UNDER,
		['X'] = ATR_STRIKE,
	};
	int c, flag;
	for (;;) {
		c = lex_peekc(lex);
		if (c < 0 || c >= SIZE(attrname) || !attrname[c])
			return;
		flag = attrname[c];
		if (!(flag & ATRS_MASK) && !(flag & allow))
			return;
		*flags |= flag;
		lex_getc(lex);
	}
}

static int
_parse_color(Lex *lex, unsigned long *c)
{
	char color[32];
	int i;
	if (lex_eof(lex)) {
		*c = COL_FG;
		return 0;
	}
	lex_all(lex);
	if (lex_str(lex, color, sizeof(color)))
		return -1;
	
	if (color[0] == '#') {
		char *end;
		unsigned long cv;
		if (strlen(color) != 7)
			return -1;
		cv = strtol(color + 1, &end, 16);
		if (*end != '\0')
			return -1;
		*c = cv | 0xff000000;
		return 0;
	}
	
	for (i = 0; i < SIZE(colnames); i++)
		if (strcmp(colnames[i].s, color) == 0) {
			*c = colnames[i].c;
			return 0;
		}
	return -1;
}

static struct DocNode*
must_start_line(struct DocNode *node, struct DocNode *last)
{
	struct DocNode *n;
	for (n = node->prev; n->type != DOC_ROOT; n = n->prev) {
		if (!n->visible)
			continue;
		switch (n->type) {
		case DOC_NL:
		case DOC_END:
			return NULL;
		default:
			return error_node(node, last);
		}
	}
	return NULL;
}

static struct DocNode*
cmd_branch(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	inherit_from(node, node->parent);
	node->attr &= ~ATR_FOLD;
	return must_start_line(node, last);
}

static struct DocNode*
cmd_branch_down(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	return must_start_line(node, last);
}

static struct DocNode*
cmd_branch_leaf(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	if (must_start_line(node, last))
		return node;
	inherit_from(node, node->parent);
	return NULL;
}

static struct DocNode*
cmd_branch_up(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	if (last->depth == 0)
		return error_node(node, last);
	return must_start_line(node, last);
}

static struct DocNode*
cmd_color(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	unsigned long fg = COL_FG;
	if (lex_eof(lex)) {
		node->fg = COL_FG;
		return NULL;
	}
	if (_parse_color(lex, &fg))
		return error_node(node, last);
	node->fg = fg;
	return NULL;
}

static struct DocNode*
cmd_link(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	node->attr |= ATR_UNDER;
	return NULL;
}

static struct DocNode*
cmd_prompt(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	node->attr |= ATR_PROMPT;
	return NULL;
}

static struct DocNode*
cmd_prompt_cont(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	node->attr |= ATR_PROMPT2;
	return NULL;
}

static struct DocNode*
cmd_prompt_end(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	node->attr &= ~(ATR_PROMPT | ATR_PROMPT2);
	return NULL;
}

static struct DocNode*
cmd_reset(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	inherit_from(node, node->parent);
	node->attr |= last->attr & ~ATRS_MASK;
	return NULL;
}

static struct DocNode*
cmd_sec_cols(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	inherit_from(node, node->parent);
	node->layout = LAYOUT_COLS;
	return NULL;
}

static struct DocNode*
cmd_sec_rows(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	inherit_from(node, node->parent);
	node->layout = LAYOUT_ROWS;
	return NULL;
}

static struct DocNode*
cmd_sec_verbatim(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	int tabw = 4;
	inherit_from(node, node->parent);
	node->layout = LAYOUT_VERBATIM;
	if (!lex_eof(lex) && parse_uint(lex, &tabw))
		return error_node(node, last);
	if (tabw > 100)
		return error_node(node, last);
	if (!lex_eof(lex)) {
		if (!lex_next_b(lex, 's'))
			return error_node(node, last);
		node->attr |= ATR_SPACES;
	}
	node->attr |= ATR_MONO;
	node->tabw = tabw;
	return NULL;
}

static struct DocNode*
cmd_sec_end(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	inherit_from(node, node->parent);
	node->layout = LAYOUT_PLAIN;
	return NULL;
}

static struct DocNode*
cmd_noop(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	return NULL;
}

static struct DocNode*
cmd_error(Lex *lex, struct DocNode *node, struct DocNode *last)
{
	return error_node(node, last);
}

typedef struct DocNode* cmdFunc(Lex*, struct DocNode*, struct DocNode*);

struct cmdspec {
	const char *name;
	cmdFunc *fn;
	int ntype;
	char has_arg, display_arg;
	int allow_flags;
} cmdnames[] = {
/*	name,	func,	node type,	has arg,	display arg,	extra flags */
						
	{"",	cmd_noop,	DOC_ATTR,	0,	0,	},
	{"0",	cmd_reset,	DOC_ATTR,	0,	0,	},
	{"C",	cmd_color,	DOC_COL,	1,	0,	},
	{"L",	cmd_link,	DOC_LINK,	1,	1,	},
	{"P0",	cmd_prompt_end,	DOC_PROMPT,	0,	0,	},
	{"P1",	cmd_prompt,	DOC_PROMPT,	0,	0,	},
	{"P2",	cmd_prompt_cont,	DOC_PROMPT,	0,	0,	},
	{"S0",	cmd_sec_end,	DOC_SECTION,	0,	0,	},
	{"SC",	cmd_sec_cols,	DOC_SECTION,	0,	0,	},
	{"SR",	cmd_sec_rows,	DOC_SECTION,	0,	0,	},
	{"SV",	cmd_sec_verbatim,	DOC_SECTION,	1,	0,	},
	{"TB",	cmd_branch,	DOC_TREE_BRANCH,	0,	0,	ATR_FOLD|ATR_OUTLINE},
	{"TD",	cmd_branch_down,	DOC_TREE_DOWN,	0,	0,	},
	{"TL",	cmd_branch_leaf,	DOC_TREE_LEAF,	0,	0,	},
	{"TU",	cmd_branch_up,	DOC_TREE_UP,	0,	0,	},
	{"?",	cmd_error,	DOC_ERROR,	1,	0,	},
};

static struct cmdspec*
parse_cmd_name(Lex *lex)
{
	int i;
	char buf[8];
	lex_token(lex);
	while (lex_next_brange(lex, 'A', 'Z') || lex_next_brange(lex, '0', '9')
	|| lex_next_any(lex, "?=")) {}
	if (lex_str(lex, buf, sizeof(buf)))
		return NULL;
	for (i = 0; i < SIZE(cmdnames); i++) {
		struct cmdspec *cmd = &cmdnames[i];
		if (strcmp(buf, cmd->name) == 0)
			return cmd;
	}
	return NULL;
}

static struct DocNode*
parse_cmd(char *buf, int len, struct DocNode *node, struct DocNode *last)
{
	Lex lex;
	struct cmdspec *cmd;
	int setflags = 0, unsetflags = 0;
	int cmdrune = runelen(CMD_RUNE);
	node->n1 = node->n0 + cmdrune + len + cmdrune;
	node->p1 = node->p0 = node->n0;
	if (len == 0) {
		node->type = DOC_CMDESC;
		node->visible = 1;
		set_parent(node, last);
		inherit_from(node, NULL);
		return node;
	}
	
	lex_init(&lex, buf, len);
	cmd = parse_cmd_name(&lex);
	if (!cmd)
		return error_node(node, last);
	node->type = cmd->ntype;
	node->visible = cmd->display_arg;
	set_parent(node, last);
	inherit_from(node, NULL);
	
	lex_token(&lex);
	if (lex_next_b(&lex, '+'))
		parse_flags(&lex, node, &setflags, cmd->allow_flags);
	if (lex_next_b(&lex, '-'))
		parse_flags(&lex, node, &unsetflags, cmd->allow_flags);
	
	lex_token(&lex);
	if (!lex_eof(&lex)) {
		if (!cmd->has_arg || !lex_next_b(&lex, ' '))
			return error_node(node, last);
		lex_token(&lex);
		if (cmd->display_arg) {
			node->p0 = node->n0 + cmdrune + lex.offset;
			node->p1 = node->n0 + cmdrune + len;
		}
	}
	
	if (cmd->fn(&lex, node, last))
		return node;
	node->attr |= setflags;
	node->attr &= ~unsetflags;
	return node;
}

struct DocNode*
parse_next(Text *txt, struct DocOpts *opts, struct DocNode *last)
{
	static struct DocOpts default_opts = {.plain = 0};
	char buf[DOC_TXT_MAX];
	char *end;
	Rune r;
	int len, rlen;
	struct DocNode *node;
	if (last->next)
		return last->next;
	if (last->type == DOC_EOF)
		return last;
	if (!opts)
		opts = &default_opts;

	node = calloc(1, sizeof(*node));
	last->next = node;
	node->prev = last;
	node->n0 = last->n1;
	
	len = text_read(txt, node->n0, buf, sizeof(buf)-1);
	if (len == 0) {
		node->type = DOC_EOF;
		node->p0 = node->p1 = node->n1 = node->n0;
		set_parent(node, last);
		inherit_from(node, NULL);
		return node;
	}
	buf[len] = 0;
	
	rlen = charntorune(&r, buf, len);
	
	if (r == CMD_RUNE && !opts->plain) {
		char *start = buf + rlen;
		end = utfrune(start, CMD_RUNE);
		if (!end || memchr(start, '\n', end - start)) {
			node->n1 = node->n0 + runelen(CMD_RUNE);
			return error_node(node, last);
		}
		return parse_cmd(start, end - start, node, last);
	}
	
	switch (r) {
	case '\t':
		return visible_node(node, DOC_TAB, 1, last);
	case '\n':
		if (last->attr & ATR_CONTROL)
			return visible_node(node, DOC_END, 1, last);
		else
			return visible_node(node, DOC_NL, 1, last);
	case ' ':
		for (end = buf + 1; *end == ' '; end++) {}
		return visible_node(node, DOC_SPACES, end - buf, last);
	default:
		end = _nextbreak(buf + rlen, buf + len);
		return visible_node(node, DOC_TXT, end - buf, last);
	}
}
