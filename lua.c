#define _DEFAULT_SOURCE
#include <assert.h>
#include <stdbool.h>
#include <time.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <utf.h>

#include "def.h"
#include "text.h"
#include "file.h"
#include "window.h"
#include "lua.h"

typedef struct WinData WinData;
struct WinData {
	struct Win	*win;
	struct FileRef	sel;
};

static void	lua_file_push(lua_State*, struct File*, int64, int64);
static int	lua_win_exec(lua_State*);
static int	lua_win_follow(lua_State*);
static int	lua_win_mark_clean(lua_State*);
static bool	lua_win_push(lua_State*, struct Win*);
static int	lua_win_select(lua_State*);
static int	lua_win_write(lua_State*);

const char *file_type = "lime-file";
static struct luaL_Reg file_lib[] = {
	{NULL,	NULL}
};

const char *win_type = "lime-window";
static struct luaL_Reg win_lib[] = {
	{"exec",	lua_win_exec},
	{"follow",	lua_win_follow},
	{"markClean",	lua_win_mark_clean},
	{"select",	lua_win_select},
	{"write",	lua_win_write},
	{NULL,	NULL}
};

static lua_State *lua_ctx;

void
lua_file_push(lua_State *L, struct File *f, int64 sel0, int64 sel1)
{
	struct FileRef *ref;
	
	ref = lua_newuserdata(L, sizeof(*ref));
	file_ref(ref, f, sel0, sel1);
	luaL_setmetatable(L, file_type);
}

void
lua_init(void)
{
	int err;
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	luaL_newmetatable(L, file_type);
	luaL_newlib(L, file_lib);
	lua_setfield(L, -2, "__index");
	lua_pop(L, 1);
	
	luaL_newmetatable(L, win_type);
	luaL_newlib(L, win_lib);
	lua_setfield(L, -2, "__index");
	lua_pop(L, 1);
	
	err = luaL_dostring(L, "function keepStack(m) return debug.traceback(m,2) end");
	assert(!err);
	
	lua_ctx = L;
}

int
lua_run(struct Win *w, const char *src)
{
	lua_State *L = lua_ctx;
	int err;
	if (!src)
		return 0;
	lua_win_push(L, w);
	lua_setglobal(L, "window");
	
	lua_getglobal(L, "keepStack");
	luaL_loadstring(L, src);
	err = lua_pcall(L, 0, 0, -2);
	if (!err)
		return 0;
	switch (err) {
	case LUA_ERRRUN:
		fprintf(stderr, "%s\n", lua_tostring(L, -1));
		break;
	case LUA_ERRMEM:
		fprintf(stderr, "Lua memory allocation error\n");
		break;
	case LUA_ERRERR:
		fprintf(stderr, "internal Lua error: error while running Lua error handler\n");
		break;
#ifdef LUA_ERRGCMM
	case LUA_ERRGCMM:
		fprintf(stderr, "internal Lua error: error while running a __gc metamethod\n");
		break;
#endif
	default:
		fprintf(stderr, "unknown Lua error\n");
		break;
	}
	return err;
}

int
lua_win_exec(lua_State *L)
{
	struct WinData *d = luaL_checkudata(L, 1, win_type);
	const char *c = luaL_checkstring(L, 2);
	win_exec(d->win, c);
	lua_pushvalue(L, 1);
	return 1;
}

int
lua_win_follow(lua_State *L)
{
	struct WinData *d = luaL_checkudata(L, 1, win_type);
	if (lua_isnoneornil(L, 2)) {
		lua_pushboolean(L, d->win->follow);
		return 1;
	}
	luaL_checktype(L, 2, LUA_TBOOLEAN);
	d->win->follow = lua_toboolean(L, 2);
	lua_pushvalue(L, 1);
	return 1;
}

int
lua_win_mark_clean(lua_State *L)
{
	struct WinData *d = luaL_checkudata(L, 1, win_type);
	d->win->file->save_chg = d->win->file->history.last;
	lua_pushvalue(L, 1);
	return 1;
}

bool
lua_win_push(lua_State *L, struct Win *w)
{
	struct WinData *d;
	lua_pushlightuserdata(L, w);
	if (lua_gettable(L, LUA_REGISTRYINDEX) == LUA_TUSERDATA)
		return false;
	d = lua_newuserdata(L, sizeof(*d));
	d->win = w;
	luaL_setmetatable(L, win_type);
	return true;
}

int
lua_win_select(lua_State *L)
{
	struct WinData *d;
	int64 p0, p1, len;
	d = luaL_checkudata(L, 1, win_type);
	p0 = luaL_checkinteger(L, 2);
	if (lua_isnoneornil(L, 3))
		p1 = p0;
	else
		p1 = luaL_checkinteger(L, 3);
	len = text_len(d->win->file->txt);
	if (p0 < 0 || p0 > len || p1 < 0 || p1 > len || p0 > p1)
		return luaL_error(L, "address out of range");
	lua_pop(L, 2);
	file_ref(&d->sel, d->win->file, p0, p1);
	lua_pushvalue(L, 1);
	return 1;
}

int
lua_win_write(lua_State *L)
{
	struct WinData *d;
	const char *s;
	size_t len;
	int64 p0, p1;
	int i, n;
	d = luaL_checkudata(L, 1, win_type);
	file_ref_get(&d->sel, &p0, &p1);
	if (p0 < 0) {
		lua_pushnil(L);
		lua_pushliteral(L, "selection is no longer valid");
		return 2;
	}
	
	n  = lua_gettop(L);
	for (i = 2; i <= n; i++) {
		s = lua_tolstring(L, i, &len);
		if (!s)
			return luaL_error(L, "argument %d is not a string", i - 1);
		file_change(d->win->file, p0, p1, s, len, SOURCE_CMD);
		p0 = p1 = p0 + len;
	}
	lua_pop(L, n - 1);
	return 1;
}

void
lua_win_trigger(lua_State *L, struct Win *w, int ev)
{
	
}
