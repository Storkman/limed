struct KeyCommand winkeys[] = {
	{ ControlMask,	XK_apostrophe,	kcmd_mark_go,	{} },
	{ ControlMask,	XK_bracketleft,	kcmd_pipe,	{.s = "sed \"s/^$indent//\" " } },
	{ ControlMask,	XK_bracketright,	kcmd_pipe,	{.s = "sed \"s/^/$indent/\" " } },
	{ ControlMask,	XK_b,	kcmd_makecmd,	{} },
	{ ControlMask,	XK_m,	kcmd_mark_set,	{} },
	{ ControlMask,	XK_o,	kcmd_ins_line,	{} },
	{ ControlMask,	XK_p,	kcmd_mono,	{} },
	{ ControlMask,	XK_s,	kcmd_exec,	{.s = "Put"} },
	{ ControlMask,	XK_t,	kcmd_plain,	{} },
	{ ControlMask,	XK_y,	kcmd_paste,	{} },
	{ ControlMask,	XK_equal,	kcmd_font_size,	{.i=1} },
	{ ControlMask,	XK_minus,	kcmd_font_size,	{.i=-1} },
	{ ControlMask,	XK_Home,	kcmd_sel_all,	{} },
	{ AltMask,	XK_m,	kcmd_mark_push,	{} },
	{ AltMask,	XK_Tab,	kcmd_warp,	{} },
	{ 0,	XK_F1,	kcmd_proc,	{.s = "lime-tags"},	APPEND_SEL },
	{ 0,	XK_F2,	kcmd_proc,	{.s = "lime-g"},	APPEND_SEL },
};

int popup_width = 800, popup_height = 600;
int popup_x = 10, popup_y = 10;

/* Default colors based on Gruvbox (https://github.com/morhetz/gruvbox-contrib). */
int colors[] = {
	[COL_FG] 	= 	0xebdbb2,	/* fg */
	[COL_BG] 	= 	0x1d2021,	/* Gruvbox bg0_h */
	
	/* Normal, Exec, and Look selections. */
	[COL_SEL]	=	0x504945,	/* bg2 */
	[COL_SEL2]	=	0x9d0006,	/* light mode dark red */
	[COL_SEL3]	=	0x427b58,	/* light mode dark aqua */
	
	/* Malformed document commands. */
	[COL_ERR]	=	0xff0000,
	
	/* Scroll area and the scroll bar itself */
	[COL_SCR]	=	0x427b58,
	[COL_SCR_BAR]	=	0x1d2021,
	
	/* Tag area background. */
	[COL_TAG] 	= 	0x3c3836,	/* bg1 */
	
	/* Tag underline. */
	[COL_TAG_CUR]	=	0x1d2021,
	[COL_TAG_DIRTY]	=	0x689d6a,	/* aqua */
	
	/* Margin line set by the Margin command. */
	[COL_MARGIN]	=	0x7c6f64,	/* bg4 */
	
	[COL_TREE]	=	0x7c6f64,	/* bg3 */
	
	/* Alternative backgroud displayed after the end of file. */
	[COL_BG0]	=	0x282828,	/* bg */
	
	[COL_1]	=	0x7c6f64,	[COL_2]	=	0x928374,	[COL_3]	=	0xbdae93,
	[COL_R]	=	0xcc241d,	[COL_LR]	=	0xfb4934,
	[COL_Y]	=	0xd79921,	[COL_LY]	=	0xfabd2f,
	[COL_G]	=	0x98971a,	[COL_LG]	=	0xb8bb26,
	[COL_B]	=	0x458588,	[COL_LB]	=	0x83a598,
	[COL_C]	=	0x689d6a,	[COL_LC]	=	0x8ec07c,	/* aqua */
	[COL_M]	=	0xb16286,	[COL_LM]	=	0xd3869b,	/* purple */
	
	[COL_O]	=	0xd65d0e,	[COL_LO]	=	0xfe8019,
};

struct FontList fonts[] = {
	[FNT_MAIN] = {
		.names = {"Go", "Hack Nerd Font", "Unifont"},
		.size=12 },
	[FNT_MONO] = {
		.names = {"Iosevka", "Unifont"},
		.size=13 },
	[FNT_UI] = {
		.names = {"Go", "Unifont"},
		.size=12 },
};

const char *win_new_tag_text = " Clone ";
/* Remove indentation on empty lines by default? Can be set at runtime with Trim on/off. */
bool indent_trim_default = false;
