#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "def.h"
#include "lex.h"

void
lex_all(Lex *lex)
{
	lex->pos = lex->len;
}

int
lex_eof(Lex *lex)
{
	return lex->pos == lex->len;
}

int
lex_getc(Lex *lex)
{
	char c;
	if (lex->pos == lex->len)
		return -1;
	c = lex->buf[lex->pos];
	lex->pos++;
	return c;
}

void
lex_init(Lex *lex, const char *buf, int len)
{
	lex->offset = 0;
	lex->buf = buf;
	lex->len = len;
	lex->pos = 0;
}

int
lex_next_any(Lex *lex, const char *cs)
{
	const char *c;
	char n;
	if (lex->pos == lex->len)
		return 0;
	n = lex->buf[lex->pos];
	for (c = cs; *c; c++) {
		if (n != *c)
			continue;
		lex->pos++;
		return n;
	}
	return 0;
}

int
lex_next_b(Lex *lex, char c)
{
	if (lex->pos == lex->len || lex->buf[lex->pos] != c)
		return 0;
	lex->pos++;
	return c;
}

int
lex_next_brange(Lex *lex, char a, char b)
{
	char c;
	if (lex->pos == lex->len)
		return 0;
	c = lex->buf[lex->pos];
	if (a <= c && c <= b) {
		lex->pos++;
		return c;
	}
	return 0;
}

int
lex_peekc(Lex *lex)
{
	if (lex->pos == lex->len)
		return -1;
	return lex->buf[lex->pos];
}

int
lex_str(Lex *lex, char *buf, int len)
{
	if (len < lex->pos + 1)
		return 1;
	memmove(buf, lex->buf, lex->pos);
	buf[lex->pos] = 0;
	return 0;
}

void
lex_token(Lex *lex)
{
	lex->offset += lex->pos;
	lex->buf += lex->pos;
	lex->len -= lex->pos;
	lex->pos = 0;
}

int
parse_int(Lex *lex, int *ptr)
{
	bool neg = false;
	if (lex_next_b(lex, '-'))
		neg = true;
	if (parse_uint(lex, ptr))
		return -1;
	if (neg)
		*ptr = -*ptr;
	return 0;
}

int
parse_uint(Lex *lex, int *ptr)
{
	uint64 n = 0;
	char c;
	n = lex_next_brange(lex, '0', '9');
	if (!n)
		return -1;
	n -= '0';
	while ((c = lex_next_brange(lex, '0', '9'))) {
		n *= 10;
		n += c - '0';
		if (n > INT32_MAX)
			return -1;
	}
	*ptr = n;
	return 0;
}
