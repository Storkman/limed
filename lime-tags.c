#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <readtags.h>

int	find_tag(tagFile *tags, const char *name);
void	spawn_editor(const char *file, unsigned long line);

int
find_tag(tagFile *tags, const char *name)
{
	tagEntry tag;
	if (tagsFind(tags, &tag, name, 0) == TagFailure)
		return 0;
	do {
		spawn_editor(tag.file, tag.address.lineNumber);
		return 1;
	} while (tagsFindNext(tags, &tag) == TagSuccess);
	return 0;
}

void
spawn_editor(const char *file, unsigned long line)
{
	int pid;
	char *fp = strdup(file);
	char addr[16];
	char *const args[] = {"limed", fp, addr, NULL};
	snprintf(addr, sizeof(addr), "%ld", line);
	pid = fork();
	switch (pid) {
	case -1:
		perror("failed to fork");
		break;
	case 0:
		execvp(args[0], args);
		perror("failed to exec");
		exit(1);
		break;
	default:
		break;
	}
	free(fp);
}

int
main(int argc, char *argv[])
{
	tagFile *tags;
	tagFileInfo tinfo;
	int found;
	if (argc < 2)
		return 0;
	tags = tagsOpen("./tags", &tinfo);
	if (!tags)
		return 1;
	found = find_tag(tags, argv[1]);
	tagsClose(tags);
	return !found;
}
