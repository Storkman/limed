# The LimEd Coding Style

These are some of the conventions followed, more or less consistently,
by the `limed` text editor source code,
in rough order of decreasing importance.

## Formatting
Code is indented with Tabs.
Alignment (in struct definitions, data tables...) is also done with tabs, using [elastic tabstops](http://nickgravgaard.com/elastic-tabstops/),
i.e. columns are separated with a single Tab.

Empty lines are indented the same way as if they contained code.

	-→if (!c)
	-→-→return;
	-→
	-→change_apply(w, c, c);

Git will helpfully highlight the indentation of the empty line in red,
to make it clear that you haven't accidentally removed it.

Braces are in K&R style.
In function definitions, the return type is placed on a separate line.

Source files are structured in the following order:
* constants
* data structures
* function declarations
* data tables
* global variables
* function definitions

Declarations of each kind are in alphabetical order,
unless they're only used by a single function (see following sections).

## Programming Conventions

### Header files
Header files must never be included by other header files.
Just include the needed headers in the right order in the source file itself.

Obviously, this doesn't apply to pre-existing system header files and such.

### Functions

To return a "success/failure" value from a function, use 0 or *true* for success; and non-zero or *false* for failure.

	int  file_put(File*, const char*);        /* returns non-zero error code on failure */
	bool _keycmd(Panel*, XKeyEvent*, KeySym); /* returns true if the event was handled  */

