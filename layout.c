#define _DEFAULT_SOURCE
#include <assert.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <utf.h>

#include "def.h"
#include "text.h"
#include "parse.h"
#include "typo.h"
#include "layout.h"

struct TableNodes {
	struct DocNode **nodes;
	struct DocNode *end;
	int len, cap;
};

struct TableFrags {
	struct Frag *last[32], *this[32];
};

struct LayoutState {
	struct Layout *layout;
	struct DocNode *node;
	int depth;
	int attr;
	
	int x, y;
	char retry;
	
	struct Frag *linefirst, *frag;
	struct Frag *firstvisible, *lastvisible;
	
	/* elastic tabstops and multi-column */
	int ntabs, maxntabs;
};

static struct Frag*	add_frag(struct LayoutState*);
static struct Frag*	add_frag_text(struct LayoutState*, int xoff);
static void	add_frag_text_fake(struct LayoutState*, const char*);
static struct Frag*	add_frag_text_part(struct LayoutState*, int64 p, int64 len, int xoff);
static void	add_nl(struct LayoutState*, struct TableFrags*);
static void	add_nl_fake(struct LayoutState*, struct TableFrags*);
static struct Frag*	add_tab(struct LayoutState*, struct TableFrags*);

static int	layout_advance_of(struct LayoutState*, const char *str, int64 len);
static int	layout_check_tabs(struct LayoutState*, struct Frag *table);
static void	layout_extents(struct LayoutState*, const char *str, int64 len, FontExtents*);
static struct DocNode*	layout_next(struct LayoutState*);
static void	layout_state_init(struct LayoutState*, struct Layout*);

static struct LayoutState	layout_document(struct LayoutState);
static struct LayoutState	layout_plain(struct LayoutState, struct TableFrags*);
static struct LayoutState	layout_plain_text(struct LayoutState, struct TableFrags*);
static struct LayoutState	layout_section(struct LayoutState);
static struct LayoutState	layout_table(struct LayoutState, struct TableFrags*);
static struct LayoutState	layout_verbatim(struct LayoutState, struct TableFrags*);

static FontList*	node_font(struct LayoutState*, struct DocNode*);
static int	node_fonth(struct LayoutState*, struct DocNode*);
static int	node_frag_len(struct DocNode*);
static void	node_to_frag(struct LayoutState*, struct DocNode*, struct Frag*);

void	read_table(struct TableNodes*, struct LayoutState);
struct LayoutState	try_section(struct LayoutState, struct TableFrags*, struct TableNodes*);

static const char *etabs_min_cellw	= "0000";
static const char *etabs_min_padding	= "...";
static const int table_margin_px = 25;

static const char *fold_mark_folded	= "洛";
static const char *fold_mark_unfolded	= "";

static void
set_indent_offset(struct LayoutState *ctx)
{
	FontExtents ext;
	if (ctx->x != 0 || ctx->depth == 0)
		return;
	layout_extents(ctx, etabs_min_cellw, strlen(etabs_min_cellw), &ext);
	ctx->x = ctx->depth * ext.advance;
}

struct Frag*
add_frag(struct LayoutState *ctx)
{
	struct Frag *f = ctx->frag;
	struct Frag *next;
	if (!f->next) {
		f->next = malloc(sizeof(*f));
		if (!f->next)
			die("out of memory\n");
		f->next->next = NULL;
	}
	f = f->next;
	next = f->next;
	
	memset(f, 0, sizeof(*f));
	f->x = ctx->x;
	f->y = ctx->y;
	f->prev = ctx->frag;
	f->next = next;
	node_to_frag(ctx, ctx->node, f);
	ctx->frag = f;
	return f;
}

struct Frag*
add_frag_text(struct LayoutState *ctx, int xoff)
{
	set_indent_offset(ctx);
	return add_frag_text_part(ctx, 0, ctx->node->p1 - ctx->node->p0, xoff);
}

static void
set_layout_limits(struct LayoutState *ctx, struct Frag *f)
{
	if (!ctx->firstvisible && f->p >= ctx->layout->start_at)
		ctx->firstvisible = f;
	if (!ctx->lastvisible && ctx->firstvisible
	&& f->y - ctx->firstvisible->y + f->h >= ctx->layout->h)
		ctx->lastvisible = f->prev ? f->prev : f;
}

void
add_frag_text_fake(struct LayoutState *ctx, const char *str)
{
	struct FontExtents ext;
	struct Frag *f;
	int len;
	set_indent_offset(ctx);
	f = add_frag(ctx);
	len = strlen(str);
	assert(len < sizeof(f->fake_text));
	f->p = ctx->node->n0;
	f->len = 0;
	f->fake = true;
	memmove(f->fake_text, str, len+1);
	layout_extents(ctx, str, len, &ext);
	f->w = ext.advance;
	ctx->x += f->w;
	set_layout_limits(ctx, f);
}

struct Frag*
add_frag_text_part(struct LayoutState *ctx, int64 p, int64 len, int xoff)
{
	struct Frag *f;
	set_indent_offset(ctx);
	f = add_frag(ctx);
	f->p += p;
	f->len = len;
	f->w = xoff;
	
	ctx->x += xoff;
	set_layout_limits(ctx, f);
	return f;
}

static void
end_of_line(struct LayoutState *ctx, struct TableFrags *tabs)
{
	ctx->x = 0;
	ctx->y += node_fonth(ctx, ctx->node);
	assert(ctx->ntabs < SIZE(tabs->this));
	memmove(tabs->last, tabs->this, sizeof(tabs->this));
	memset(tabs->this, 0, sizeof(tabs->this));
	ctx->ntabs = 0;
}

void
add_nl(struct LayoutState *ctx, struct TableFrags *tabs)
{
	struct Frag *f = add_frag_text(ctx, ctx->layout->w - ctx->x);
	f->newline = true;
	end_of_line(ctx, tabs);
	set_layout_limits(ctx, f);
}

void
add_nl_fake(struct LayoutState *ctx, struct TableFrags *tabs)
{
	struct Frag *f = add_frag(ctx);
	f->newline = true;
	f->fake = true;
	f->p = -1;
	f->len = 0;
	end_of_line(ctx, tabs);
	set_layout_limits(ctx, f);
}

static int
_resizecol(struct Frag *frag, int tab)
{
	struct Frag *tabf, *f;
	int shift;
	if (!frag)
		return tab;
	if (frag->x + frag->w >= tab)
		return frag->x + frag->w;
	for (tabf = frag; tabf; tabf = tabf->prevtab) {
		shift = tab - (tabf->x + tabf->w);
		tabf->w += shift;
		for (f = tabf->next; f && f->y+f->h == tabf->y+tabf->h; f = f->next)
			f->x += shift;
	}
	return tab;
}

struct Frag*
add_tab(struct LayoutState *ctx, struct TableFrags *tabs)
{
	FontExtents ext;
	struct Frag *tababove, *prevtab, *fe;
	int minw, mingap, curcell, cellstart;
	
	set_indent_offset(ctx);
	layout_extents(ctx, etabs_min_cellw, strlen(etabs_min_cellw), &ext);
	minw = ext.advance;
	layout_extents(ctx, etabs_min_padding, strlen(etabs_min_padding), &ext);
	mingap = ext.advance;
	
	if (ctx->ntabs > 0) {
		prevtab = tabs->this[ctx->ntabs-1];
		cellstart = prevtab->x + prevtab->w;
	} else
		cellstart = ctx->depth * minw;
	curcell = MAX(ctx->x + mingap, cellstart + minw);
	
	tababove = tabs->last[ctx->ntabs];
	
	if (ctx->ntabs >= SIZE(tabs->this) - 1) {
		fe = add_frag_text(ctx, curcell - ctx->x);
		fe->prevtab = tababove;
		return fe;
	}
	
	if (tababove)
		curcell = _resizecol(tababove, curcell);
	fe = add_frag_text(ctx, curcell - ctx->x);
	fe->prevtab = tababove;
	tabs->this[ctx->ntabs] = fe;
	ctx->ntabs++;
	return fe;
}

/* frag_atpos returns the fragment containing the index p. */
struct Frag*
frag_atpos(struct Frag *e, int64 p)
{
	struct Frag *f;
	assert(e != NULL);
	for (f = e; f->next; f = f->next) {
		if (f->p + f->len <= p)
			continue;
		if (f->newline && f->fake)
			continue;
		return f;
	}
	if (p <= f->p + f->len)
		return f;
	return NULL;
}

void
frag_freeall(struct Frag *exts)
{
	struct Frag *next, *e = exts;
	while (e) {
		next = e->next;
		free(e);
		e = next;
	}
}

struct Frag*
frag_line_end(struct Frag *from)
{
	struct Frag *f = from;
	int64 y;
	if (f->newline)
		return f;
	y = f->y;
	while (f->next && !f->next->newline && f->next->y == y)
		f = f->next;
	return f;
}

struct Frag*
frag_line_prev(struct Frag *from)
{
	struct Frag *f = from;
	int y = from->y;
	while (f->prev && f->y >= y)
		f = f->prev;
	return frag_line_start(f);
}

struct Frag*
frag_line_start(struct Frag *from)
{
	struct Frag *f = from;
	int y = f->y;
	while (f->prev && f->prev->y == y)
		f = f->prev;
	return f;
}

void
node_to_frag(struct LayoutState *ctx, struct DocNode *n, struct Frag *f)
{
	f->node = n;
	f->p = n->p0;
	f->len = node_frag_len(n);
	f->attr = ctx->attr;
	if (ctx->attr & ATR_SWAP) {
		f->fg = n->bg;
		f->bg = n->fg;
	} else {
		f->fg = n->fg;
		f->bg = n->bg;
	}
	if (ctx->attr & ATR_SPACES)
		f->indent_spaces = n->tabw;
	f->font = node_font(ctx, n);
	f->h = node_fonth(ctx, ctx->node);
}

int
layout_advance_of(struct LayoutState *ctx, const char *str, int64 len)
{
	FontExtents ext;
	layout_extents(ctx, str, len, &ext);
	return ext.advance;
}

int
layout_check_tabs(struct LayoutState *ctx, struct Frag *table)
{
	struct Frag *f;
	int maxw = ctx->layout->w - table_margin_px;
	if (ctx->maxntabs == 1)
		return 0;
	for (f = table; f != ctx->frag; f = f->next)
		if (f->x + f->w > maxw && !f->newline && !f->fake)
			return 1;
	return 0;
}

void
layout_extents(struct LayoutState *ctx, const char *str, int64 len, FontExtents *ext)
{
	typo_measure(node_font(ctx, ctx->node), ctx->attr & ATR_BOLD_ITALIC,
		str, MIN(len, INT_MAX), ext);
}

struct DocNode*
layout_next(struct LayoutState *ctx)
{
	ctx->node = parse_next(ctx->layout->txt, ctx->layout->parse_opts, ctx->node);
	ctx->attr = ctx->node->attr;
	ctx->depth = ctx->node->depth;
	return ctx->node;
}

void
layout_perform(struct Layout *layout)
{
	struct Frag *f;
	struct LayoutState ctx;
	int yoff;
	layout_state_init(&ctx, layout);
	layout->overflow = 0;
	
	ctx = layout_document(ctx);
	
	layout->lastfrag = ctx.frag;
	layout->firstvisible = ctx.firstvisible;
	layout->lastvisible = ctx.lastvisible;
	
	yoff = ctx.firstvisible->y;
	for (f = layout->frags; f; f = f->next)
		f->y -= yoff;
	if (ctx.lastvisible)
		layout->overflow = 1;
	else
		layout->lastvisible = ctx.frag;
}

struct LayoutState
layout_document(struct LayoutState ctx)
{
	struct Frag *endfrag;
	(void) layout_next(&ctx);
	while (!ctx.lastvisible && ctx.node->type != DOC_EOF)
		ctx = layout_section(ctx);
	
	endfrag = add_frag_text(&ctx, ctx.layout->w - ctx.x);
	frag_freeall(endfrag->next);
	endfrag->next = NULL;
	if (!ctx.firstvisible)
		ctx.firstvisible = endfrag;
	return ctx;
}

struct LayoutState
layout_plain(struct LayoutState ctx, struct TableFrags *tabs)
{
	struct DocNode *node = ctx.node;
	struct Frag *last = ctx.frag;
	switch (node->type) {
	case DOC_NL:
	case DOC_END:
		add_nl(&ctx, tabs);
		break;
	case DOC_TAB:
		add_tab(&ctx, tabs)->is_indent = (last->is_indent || last->newline);
		break;
	case DOC_ERROR:
		add_frag_text_fake(&ctx, "◊");
		ctx.frag->fg = COL_FG;
		ctx.frag->bg = COL_ERR;
		break;
	case DOC_TREE_LEAF:
		set_indent_offset(&ctx);
		add_frag(&ctx);
		break;
	default:
		return layout_plain_text(ctx, tabs);
	}
	layout_next(&ctx);
	return ctx;
}

struct LayoutState
layout_plain_text(struct LayoutState ctx, struct TableFrags *tabs)
{
	char buf[DOC_TXT_MAX];
	FontExtents ext;
	int64 len = 0, wraplen = 0;
	char *s, *line;
	struct DocNode *node = ctx.node;
	
	len = text_read(ctx.layout->txt, node->p0, buf, node_frag_len(node));
	layout_extents(&ctx, buf, len, &ext);
	
	if (node->type == DOC_SPACES && (ctx.frag->is_indent || ctx.frag->newline)) {
		add_frag_text(&ctx, ext.advance)->is_indent = 1;
		layout_next(&ctx);
		return ctx;
	}
	if (!tabs || !node_lasttab(ctx.layout->txt, ctx.layout->parse_opts, node)) {
		add_frag_text(&ctx, ext.advance);
		layout_next(&ctx);
		return ctx;
	}
	line = s = buf;
	while (len > 0) {
		layout_extents(&ctx, s, len, &ext);
		wraplen = len;
		while (PANEL_MARGIN + ctx.x + ext.advance > ctx.layout->w) {
			wraplen /= 2;
			if (wraplen == 0) {
				if (s == line) {
					layout_next(&ctx);
					return ctx;
				}
				break;
			}
			layout_extents(&ctx, s, wraplen, &ext);
		}
		
		if (wraplen == 0) {
			layout_extents(&ctx, etabs_min_padding, strlen(etabs_min_padding), &ext);
			ctx.y += node_fonth(&ctx, node);
			ctx.x = ext.advance;
			if (ctx.ntabs > 0) {
				struct Frag *f = tabs->this[ctx.ntabs-1];
				ctx.x += f->x + f->w;
			}
			line = s;
			continue;
		}
		add_frag_text_part(&ctx, s - buf, wraplen, ext.advance);
		s += wraplen;
		len -= wraplen;
	}
	layout_next(&ctx);
	return ctx;
}
	

void
layout_state_init(struct LayoutState *ctx, struct Layout *l)
{
	memset(ctx, 0, sizeof(*ctx));
	ctx->layout = l;
	ctx->node = l->doc;
	ctx->frag = l->frags;
}

struct LayoutState
layout_section(struct LayoutState ctx)
{
	struct TableNodes table = {};
	struct TableFrags tabs = {};
	struct LayoutState start;
	struct DocNode *n = ctx.node;
	
	if (n->attr & ATR_HIDE) {
		layout_next(&ctx);
		return ctx;
	}
	
	if (n->type == DOC_TREE_BRANCH) {
		const char *fmark = (n->attr & ATR_FOLD) ? fold_mark_folded : fold_mark_unfolded;
		ctx.attr &= ATR_MONO;
		add_frag_text_fake(&ctx, fmark);
		ctx.x += layout_advance_of(&ctx, " ", 1);
		layout_next(&ctx);
		
		/* add the heading text */
		while (ctx.node->type != DOC_END && ctx.node->type != DOC_EOF)
			ctx = layout_plain(ctx, &tabs);
		if (ctx.node->type == DOC_EOF)
			return ctx;
		
		/* add the newline */
		ctx = layout_plain(ctx, &tabs);
		return ctx;
	} else if (n->type == DOC_SECTION) {
		n = layout_next(&ctx);
	} else if (n->type == DOC_TREE_LEAF) {
		return layout_plain(ctx, &tabs);
	}
	
	if (!n->visible) {
		layout_next(&ctx);
		return ctx;
	}
	
	if (n->layout == LAYOUT_ROWS || n->layout == LAYOUT_COLS)
		read_table(&table, ctx);
	
	start = ctx;
	start.maxntabs = 0;
	start.ntabs = 0;
	
	do {
		start.maxntabs = ctx.maxntabs;
		ctx = start;
		memset(&tabs, 0, sizeof(tabs));
		ctx = try_section(ctx, &tabs, &table);
		if (!ctx.retry && ctx.maxntabs > 0 && layout_check_tabs(&ctx, start.frag)) {
			ctx.retry = 1;
			ctx.maxntabs--;
		}
	} while (ctx.retry);
	free(table.nodes);
	return ctx;
}

struct LayoutState
layout_table(struct LayoutState ctx, struct TableFrags *tabs)
{
	char buf[DOC_TXT_MAX+1];
	FontExtents ext;
	int64 len;
	struct DocNode *node = ctx.node;
	switch (ctx.node->type) {
	case DOC_NL:
		if (ctx.maxntabs < 1 || ctx.ntabs < ctx.maxntabs) {
			add_tab(&ctx, tabs);
			break;
		}
		add_nl(&ctx, tabs);
		break;
	case DOC_TAB:
		layout_extents(&ctx, "0", 1, &ext);
		add_frag_text(&ctx, ext.advance);
		break;
	default:
		assert(node->p0 >= 0 && node->p1 >= 0);
		assert(node->p1 - node->p0 < sizeof(buf));
		len = text_read(ctx.layout->txt, node->p0, buf, node->p1 - node->p0);
		buf[len] = 0;
		layout_extents(&ctx, buf, len, &ext);
		if (PANEL_MARGIN + ctx.x + ext.advance < ctx.layout->w - table_margin_px) {
			add_frag_text(&ctx, ext.advance);
			break;
		}
		if (ctx.maxntabs == 0 && ctx.ntabs > 0) {
			ctx.maxntabs = ctx.ntabs;
			ctx.retry = 1;
			return ctx;
		}
		if (ctx.maxntabs > 1) {
			ctx.maxntabs--;
			ctx.retry = 1;
			return ctx;
		}
		add_nl_fake(&ctx, tabs);
		add_frag_text(&ctx, ext.advance);
		break;
	}
	layout_next(&ctx);
	return ctx;
}

struct LayoutState
layout_verbatim(struct LayoutState ctx, struct TableFrags *tabs)
{
	struct Frag *last = ctx.frag;
	FontExtents ext;
	int curtab, tabw;
	if (ctx.node->type != DOC_TAB)
		return layout_plain(ctx, tabs);
	
	layout_extents(&ctx, "0", 1, &ext);
	tabw = ctx.node->tabw * ext.advance;
	curtab = ctx.x - (ctx.x % tabw) + tabw;
	add_frag_text(&ctx, curtab - ctx.x)->is_indent = (last->is_indent || last->newline);
	
	(void) layout_next(&ctx);
	return ctx;
}

FontList*
node_font(struct LayoutState *ctx, struct DocNode *n)
{
	int ft = (ctx->attr & ATR_MONO) ? FNT_MONO : FNT_MAIN;
	return ctx->layout->fonts[ft];
}

int
node_fonth(struct LayoutState *ctx, struct DocNode *n)
{
	FontExtents ext;
	typo_measure_font(node_font(ctx, n)->fonts, ctx->attr, &ext);
	return ext.ascent + ext.descent;
}

int
node_frag_len(struct DocNode *n)
{
	switch (n->type) {
	case DOC_EOF:
		return 0;
		break;
	case DOC_ERROR:
	case DOC_CMDESC:
		return runelen(CMD_RUNE);
		break;
	default:
		return n->p1 - n->p0;
	}
}

void
read_table(struct TableNodes *tn, struct LayoutState ctx)
{
	struct DocNode *n;
	int i;
	tn->len = 0;
	tn->cap = 128;
	tn->nodes = calloc(tn->cap, sizeof(*tn->nodes));
	
	i = 0;
	for (n = ctx.node; node_in_section(n); n = layout_next(&ctx)) {
		if (!n->visible)
			continue;
		if (!tn->nodes[i]) {
			tn->nodes[i] = n;
			tn->len++;
		}
		if (n->type != DOC_NL)
			continue;
		i++;
		if (i >= tn->cap) {
			struct DocNode **nn = NULL;
			tn->cap += 128;
			nn = realloc(tn->nodes, tn->cap * sizeof(*tn->nodes));
			if (!nn)
				die("out of memory\n");
			tn->nodes = nn;
		}
	}
	tn->end = ctx.node;
}

struct LayoutState
try_section(struct LayoutState ctx, struct TableFrags *tabs, struct TableNodes *table)
{
	int col_major = 0;
	int i, idx, ncols, nrows, ntotal;
	
	switch (ctx.node->layout) {
	case LAYOUT_COLS:
		col_major = 1;
		break;
	case LAYOUT_ROWS:
		break;
	case LAYOUT_PLAIN:
	case LAYOUT_VERBATIM:
		while (!ctx.retry && node_in_section(ctx.node)) {
			if (ctx.node->layout == LAYOUT_VERBATIM)
				ctx = layout_verbatim(ctx, tabs);
			else
				ctx = layout_plain(ctx, tabs);
			if (ctx.lastvisible)
				break;
		}
		return ctx;
	default:
		assert(0);
	}
	
	ntotal = table->len;
	if (col_major) {
		if (ctx.maxntabs > 0)
			ncols = MIN(ctx.maxntabs+1, table->len);
		else
			ncols = table->len;
		
		nrows = table->len / ncols;
		if (table->len % ncols > 0)
			nrows++;
		
		ncols = table->len / nrows;
		if (table->len % nrows > 0)
			ncols++;
		
		ntotal = nrows * ncols;
		ctx.maxntabs = ncols - 1;
	}
	
	i = 0;
	while (!ctx.retry && i < ntotal) {
		if (col_major)
			idx = (i % ncols) * nrows + i / ncols;
		else
			idx = i;
		
		if (idx >= table->len) {
			(void) add_nl_fake(&ctx, tabs);
			i++;
			continue;
		}
		ctx.node = table->nodes[idx];
		
		while (!ctx.retry && node_in_section(ctx.node)) {
			struct DocNode *n = ctx.node;
			ctx.attr = n->attr;
			ctx = layout_table(ctx, tabs);
			if (n->type == DOC_NL)
				break;
		}
		i++;
	}
	ctx.node = table->end;
	return ctx;
}

