struct Panel;
typedef bool CharClass(Rune);
typedef void PanelAction(void*, struct Panel*, int64 p0, int64 p1, int altmod);

struct Panel {
	int	x, y, w, h;
	struct FontList	*fonts[2];
	int	bg, fg, ebg;
	
	Text	*txt;
	struct DocNode	*doc;
	int64	orig, p0, p1;
	int64	last_edit0, last_edit1;
	int64	click0, click1;
	struct Frag	*click_frag;
	int	buttons_state;
	int	cursorx;
	Time	lastclick;
	
	bool	replace_mode;
	
	struct PasteTarget	*paste;
	Xim	*xim;
	
	struct DocOpts	parse_opts;
	int	indent_spaces;
	bool	indent_trim;
	
	struct Frag	*frags, *lastfrag;
	struct Frag	*firstvisible, *lastvisible;
	bool	overflow;
	
	struct DocNode	*last_seek_node;
	bool	layout_done;
	int64	layout_orig;
	
	bool	disable_scrolling;
	
	void	(*onchange)(void*, struct Panel*, struct Change*);
	PanelAction 	*onlook, *onexec;
	void	*handler_data;
};

struct PasteTarget {
	Window	win;
	Atom	from;		/* XA_PRIMARY or xatom("CLIPBOARD") */
	int64	p0;		/* original paste position */
	int64	at;		/* where to put the next pasted piece */
	Atom	prop;		/* source property */
	struct Panel	*panel;
};

bool	not_name_rune(Rune);
bool	not_word_rune(Rune);

void	panel_button(struct Panel*, XButtonEvent*);
void	panel_draw(struct Panel*, Surface*);
void	panel_free(struct Panel*);
void	panel_key(struct Panel*, XKeyEvent*);
int	panel_motion(struct Panel*, XMotionEvent*);
struct Panel*	panel_new(struct Text *t, FontList *fdefault, FontList *ffixed, int fg, int bg);
void	panel_resize(struct Panel*, int, int);

int64	panel_atpos(struct Panel*, int x, int y, struct Frag**);
int64	panel_atwinpos(struct Panel*, int x, int y, struct Frag**);
int64	panel_bol(struct Panel*, int64 pos);
void	panel_changed(struct Panel*, struct Change*);
void	panel_cut(struct Panel*);
int64	panel_eol(struct Panel*, int64 pos);
void	panel_expandsel(struct Panel*, int click, int64 *p0, int64 *p1);
int	panel_expandselrange(struct Panel*, int click, int64 *p0, int64 *p1);
void	panel_expandselword(struct Panel*, int64 click, int64 *p0, int64 *p1, CharClass *boundary);
void	panel_focus(struct Panel*, int64 p0);
void	panel_focus_in(struct Panel*);
void	panel_focus_out(struct Panel*);
int	panel_indent_spaces(struct Panel*, int64 at_pos);
void	panel_layout(struct Panel*);
void	panel_lnscroll(struct Panel*, int);
void	panel_navh(struct Panel*, int);
void	panel_navv(struct Panel*, int);
void	panel_newline(struct Panel*);
struct DocNode*	panel_parse_at(struct Panel*, int64 p);
struct DocNode*	panel_parse_next(struct Panel*, struct DocNode*);
void	panel_paste(struct Panel*, Atom);
void	panel_replace(struct Panel*, int64 p0, int64 p1, const char *str, int64 len);
int	panel_search(struct Panel*, int64 p0, int64 p1, char *s, int64 len);
int	panel_search_rev(struct Panel*, int64 p0, int64 p1, char *s, int64 len);
int	panel_searchaddr(struct Panel*, int64 p0, int64 p1, char *s, int64 len);
void	panel_seek(struct Panel*, int64 cpos);
void	panel_select(struct Panel*, int64, int64);
void	panel_showplain(struct Panel*, bool);
void	panel_shrinkwrap(struct Panel*, int maxh);

Atom	xatom(const char*);

extern struct Panel *primary_selection_panel;

extern Window cutwin;
extern char *cutbuf;
extern int cutlen;
