typedef struct FontData	FontData;
typedef struct FontExtents	FontExtents;
typedef struct FontList	FontList;
typedef void	Surface;

/* Struct FontExtents stores glyph, span, or font measurements.
 * For fonts (typo_measure_font), only ascent, descent, h, and advance are set. */
struct FontExtents {
	int x, y;		/* (x,y) - glyph or span origin, relative to its upper left corner. */
	int ascent, descent;
	int w, h;
	int advance;
};

struct FontList {
	/* passed to typo_load_fonts */
	const char	*names[16];
	double	size;
	
	/* set by typo_load_fonts */
	int	count;
	FontData	*fonts;
	FontExtents	extents;
};

/* Convenience functions that return the advance measurement for given text. */
int	typo_advance(FontList*, int attr, const char *str, int len);
int	typo_advance_span(FontData*, int attr, const char *str, int len);

/* typo_choose_font sets its last argument to the font that should be used for the beginning of the given string,
 * and returns the length that can be drawn with it.
 *
 * Specifically, it chooses the first font on the list that can at least render the first rune in the buffer,
 * and returns the total number of bytes at the beginning of the buffer which can be rendered by this font,
 * and which can't be rendered by previous fonts on the list.
 */
int	typo_choose_font(FontList*, int attr, const char *str, int len, FontData**);
FontData*	typo_choose_font_rune(FontList*, Rune, int attr);

void	typo_draw(Surface*, int x, int y, FontList*, int attr, unsigned long color, const char *str, int len);
void	typo_draw_rect(Surface*, int x, int y, int w, int h, unsigned long color);
void	typo_draw_span(Surface*, int x, int y, FontData*, int attr, unsigned long color, const char *str, int len);
void	typo_draw_rune(Surface*, int x, int y, FontList*, int attr, unsigned long color, Rune);

void	typo_load_fonts(FontList*);

/* typo_load_fonts_deferred prepares a FontList for use, without loading all the fonts.
 * Remaining fonts will be automatically loaded as necessary.
 */
void	typo_load_fonts_deferred(FontList*);

void	typo_measure(FontList*, int attr, const char *str, int len, FontExtents*);
void	typo_measure_add(FontExtents*, FontExtents*, FontExtents*);
void	typo_measure_font(FontData*, int attr, FontExtents*);
void	typo_measure_rune(FontList*, int attr, Rune, FontExtents*);
void	typo_measure_span(FontData*, int attr, const char *str, int len, FontExtents*);

void	typo_unload_fonts(FontList*);
