#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <utf.h>

#include "def.h"
#include "lex.h"
#include "text.h"
#include "ansi.h"

#define ESC 0x1b

#define STX "\x02"
#define DLE "\x10"

static int
cs_final(char c)
{
	/* A-Z\]^_`a-z{|}~ */
	return '@' <= c && c <= '~' && c != '[';
}

static void
writeout(int fd, const char *b, int len)
{
	int nwr = write(fd, b, len);
	if (nwr < 0) {
		fprintf(stderr, "write error: %s\n", strerror(errno));
		exit(1);
	}
}

static void
writestr(int fd, const char *s)
{
	return writeout(fd, s, strlen(s));
}

static void
werr(int outfd)
{
	writestr(outfd, "◊?◊");
}

static const char *cmd_sgr[] = {
	[0] = "◊0◊",
	[1] = "◊+B◊",
	[2] = "",	/* faint */
	[3] = "◊+I◊",
	[4] = "◊+U◊",
	[5] = "",	/* slow blink */
	[6] = "",	/* rapid blink */
	[7] = "◊+S◊",
	[8] = "",	/* hide */
	[9] = "◊+X◊",
	[10] = "",	/* default font */
	
	[20] = "",	/* blackletter font */
	[21] = "◊-B◊",
	[22] = "◊-B◊",	/* not bold and not faint */
	[23] = "◊-I◊",	/* not italic and not blackletter */
	[24] = "◊-U◊",
	[25] = "",	/* not blinking */
	[26] = "",	/* proportional spacing */
	[27] = "◊-S◊",
	[28] = "",	/* reveal */
	[29] = "◊-X◊",
	
	[30] = "",
	[31] = "◊C R◊",
	[32] = "◊C G◊",
	[33] = "◊C Y◊",
	[34] = "◊C B◊",
	[35] = "◊C M◊",
	[36] = "◊C C◊",
	[37] = "◊C◊",
	
	[39] = "◊C◊",
	
	[40] = "◊C-S◊",
	[41] = "◊C+S R◊",
	[42] = "◊C+S G◊",
	[43] = "◊C+S Y◊",
	[44] = "◊C+S B◊",
	[45] = "◊C+S M◊",
	[46] = "◊C+S C◊",
	[47] = "◊C+S◊",
	
	[49] = "◊C-S◊",
	
	[50] = "",	/* disable proportional spacing */
	[51] = "",	/* framed */
	[52] = "",	/* encircled */
	[53] = "",	/* overlined */
	[54] = "",	/* disable framed and encircled */
	[55] = "",	/* disable overlined */
	
	[91] = "◊C R+◊",
	[92] = "◊C G+◊",
	[93] = "◊C Y+◊",
	[94] = "◊C B+◊",
	[95] = "◊C M+◊",
	[96] = "◊C C+◊",
	[97] = "◊C◊",
	
	[101] = "◊C R+◊",
	[102] = "◊C G+◊",
	[103] = "◊C Y+◊",
	[104] = "◊C B+◊",
	[105] = "◊C M+◊",
	[106] = "◊C C+◊",
};

static int
translate_sgr_fg(int outfd, int *attr, int nattr)
{
	char buf[32];
	static const unsigned char lut[6] = {0x00, 0x33, 0x66, 0x99, 0xcc, 0xff};
	int cn, len, skip = 0;
	unsigned char r, g, b;
	r = b = 0xff;
	g = 0;
	if (attr[0] != 38)
		return 0;
	if (attr[1] == 5) {
		if (nattr < 3)
			return 0;
		cn = attr[2];
		if (cn < 16) {
			writestr(outfd, cmd_sgr[30 + cn]);
			return 2;
		}
		if (cn > 255)
			return 0;
		if (cn >= 232) {
			r = g = b = (cn - 232) * 11;
		} else {
			cn -= 16;
			b = lut[cn % 6];
			cn /= 6;
			g = lut[cn % 6];
			r = lut[cn / 6];
		}
		skip = 2;
	} else if (attr[1] == 2) {
		if (nattr < 5)
			return 0;
		r = attr[2];
		g = attr[3];
		b = attr[4];
		skip = 4;
	} else
		return 0;
	len = snprintf(buf, SIZE(buf), "◊C #%02x%02x%02x◊", r, g, b);
	writeout(outfd, buf, len);
	return skip;
}

static void
translate_sgr(int outfd, int *attr, int nattr)
{
	int i, skip;
	if (nattr == 0)
		writestr(outfd, cmd_sgr[0]);
	for (i = 0; i < nattr; i++) {
		int n = attr[i];
		if (n == 38) {
			skip = translate_sgr_fg(outfd, attr + i, nattr - i);
			if (skip == 0)
				return werr(outfd);
			i += skip;
			continue;
		}
		if (n > SIZE(cmd_sgr) || !cmd_sgr[n]) {
			fprintf(stderr, "Unknown SGR %d\n", n);
			return werr(outfd);
		}
		writestr(outfd, cmd_sgr[n]);
	}
}

static int
parse_num(Lex *lex, int *num)
{
	char buf[8];
	int c;
	lex_token(lex);
	c = lex_peekc(lex);
	if (c < '0' || '9' < c)
		return 0;
	while ('0' <= lex_peekc(lex) && lex_peekc(lex) <= '9')
		lex_getc(lex);
	if (lex_str(lex, buf, sizeof(buf)))
		return 0;
	*num = strtol(buf, NULL, 10);
	return 1;
}

static void
translate(int outfd, char *seq, int len)
{
	Lex lex = {.buf = seq, .len = len};
	int attr[16] = {0};
	int attrn = 0;
	if (!lex_next_b(&lex, ESC))
		return werr(outfd);
	if (!lex_next_b(&lex, '['))
		return werr(outfd);
	if (parse_num(&lex, &attr[0])) {
		attrn = 1;
		while (lex_next_b(&lex, ';')) {
			if (attrn >= SIZE(attr))
				return werr(outfd);
			if (!parse_num(&lex, &attr[attrn]))
				return werr(outfd);
			attrn++;
		}
	}
	
	int final = lex_getc(&lex);
	if (!cs_final(final))
		return werr(outfd);
	
	switch (final) {
	case 'H':
		return;	/* cursor position */
	case 'J':
		if (attrn > 1)
			return werr(outfd);
		if (attrn == 0 || attr[0] == 0)
			return;	/* clear from cursor to end of screen */
		if (attr[0] == 2)
			return writestr(outfd, DLE "C" STX);
		return;
	case 'K':
		return;	/* erase in line */
	case 'm':
		return translate_sgr(outfd, attr, attrn);
	default:
		return werr(outfd);
	}
}

static void* text(ANSIState*, char*, int);
static void* command(ANSIState*, char*, int);

void*
text(ANSIState *st, char *buf, int len)
{
	char *seq, *wrpos;
	
	seq = memchr(buf + st->pos, ESC, len - st->pos);
	
	wrpos = buf + st->pos;
	if (seq)
		writeout(st->outfd, wrpos, seq - wrpos);
	else
		writeout(st->outfd, wrpos, len - st->pos);
	
	if (!seq) {
		st->pos = len;
		return text;
	}
	st->pos = seq - buf;
	st->seqpos = 0;
	return command;
}

void*
command(ANSIState *st, char *buf, int len)
{
	int i;
	for (i = st->pos; i < len; i++) {
		if (st->seqpos >= sizeof(st->seqbuf)) {
			fprintf(stderr, "buffer overflow\n");
			return NULL;
		}
		st->seqbuf[st->seqpos] = buf[i];
		st->seqpos++;
		if (cs_final(buf[i])) {
			translate(st->outfd, st->seqbuf, st->seqpos);
			st->pos = i + 1;
			return text;
		}
	}
	st->pos = len;
	return command;
}

void
ansi_init(ANSIState *state)
{
	state->fn = text;
	state->seqpos = 0;
}

int
ansi_run(int out, int in)
{
	char buf[BLOCKSIZE];
	int len;
	ANSIState state;
	ansi_init(&state);
	for (;;) {
		len = read(in, buf, sizeof(buf));
		if (len < 0) {
			perror("read error");
			return 1;
		}
		if (len == 0)
			return 0;
		if (ansi_translate(&state, out, buf, len)) {
			fprintf(stderr, "buffer overflow\n");
			return 1;
		}
	}
}

int
ansi_translate(ANSIState *state, int fd, char *input, int len)
{
	state->pos = 0;
	state->outfd = fd;
	while (state->pos < len) {
		state->fn = state->fn(state, input, len);
		if (!state->fn)
			return 1;
	}
	return 0;
}
