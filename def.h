#define MAX(X,Y)	((X) >= (Y) ? (X) : (Y))
#define MIN(X,Y)	((X) <= (Y) ? (X) : (Y))
#define SIZE(X)	(sizeof(X)/sizeof((X)[0]))

#define BLOCKSIZE (16*1024)
#define PATHLEN 4096

typedef int8_t	int8;
typedef uint8_t	uint8;
typedef uint32_t	uint32;
typedef int64_t	int64;
typedef uint64_t	uint64;

void	die(const char *fmt, ...);

#define PANEL_MARGIN	5
#define SCROLLW	12
#define DCLICK_DELAY	500	/* miliseconds */
#define AUTOSAVE_INTERVAL	300	/* seconds */

enum { FNT_MAIN, FNT_MONO, FNT_UI };

enum {
	COL_DEFAULT = 0,
	COL_BG, COL_FG,
	COL_SEL, COL_SEL2, COL_SEL3,
	COL_ERR,
	COL_SCR, COL_SCR_BAR, COL_TAG,
	COL_TAG_CUR, COL_TAG_DIRTY,
	COL_MARGIN, COL_TREE,
	COL_BG0,
	
	COL_1,	COL_2,	COL_3,
	COL_R,	COL_Y,	COL_G,	COL_B,	COL_C,	COL_M,
	COL_LR,	COL_LY,	COL_LG,	COL_LB,	COL_LC,	COL_LM,
	COL_O,
	COL_LO,
};

void	errorwin_append(const char *wdir, const char *buf, int64 len);
char*	str_append(char *dst, const char *src, size_t capacity);
char*	str_copy(char *dst, const char *src, size_t capacity);
