#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <poll.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <utf.h>

#include "def.h"
#include "ansi.h"
#include "text.h"
#include "parse.h"
#include "file.h"
#include "term.h"
#include "proc.h"

struct Proc *procs;

void
proc_add_arg(Proc *p, const char *s)
{
	if (p->argc >= SIZE(p->argv)) {
		fprintf(stderr, "Too many arguments to %s.\n", p->argv[0]);
		p->error = ENOMEM;
		return;
	}
	p->argv[p->argc] = strdup(s);
	p->argc++;
}

void
proc_attach_term(Proc *p, struct File *f)
{
	proc_set_file(p, f);
	term_set_interactive(&p->term, 1);
}

void
proc_detach_file(Proc *p, struct File *f)
{
	if (p->term.file.file == f)
		file_ref_detach(&p->term.file);
	if (p->term.out_file.file == f)
		file_ref_detach(&p->term.out_file);
}

void
proc_free(Proc *p)
{
	char **s;
	struct File *ref_file = p->term.file.file;
	term_deinit(&p->term);
	if (ref_file && ref_file->client == p)
		ref_file->client = NULL;
	for (s = p->env; *s; s++)
		free(*s);
	for (s = p->argv; *s; s++)
		free(*s);
	free(p);
}

void
proc_handleall(struct pollfd *fds, int n)
{
	struct Proc *p;
	for (p = procs; p; p = p->next) {
		if (n < 2)
			return;
		term_handle_fd(&p->term, fds);
		n -= 2;
		fds += 2;
	}
}

void
proc_insert_at(Proc *p, struct File *f, int64 p0, int64 p1)
{
	file_ref(&p->term.out_file, f, p0, p1);
}

struct Proc*
proc_new(const char *s)
{
	struct Proc *p = calloc(1, sizeof(*p));
	if (!p)
		die("out of memory\n");
	term_init(&p->term);
	p->argv[0] = strdup(s);
	p->argc = 1;
	return p;
}

int
proc_pollfds(struct pollfd *fds, int nfds)
{
	int n = 0;
	struct Proc *p, *dead;
	struct Proc **live = &procs;
	p = procs;
	while (p) {
		if (term_is_attached(&p->term)) {
			*live = p;
			live = &p->next;
			p = p->next;
			continue;
		}
		dead = p;
		p = p->next;
		proc_free(dead);
	}
	*live = NULL;
	
	for (p = procs; p; p = p->next) {
		if (n + 2 > nfds)
			return n;
		term_poll_fd(&p->term, fds);
		n += 2;
		fds += 2;
	}
	return n;
}

void
proc_send(struct Proc *p, const char *b, int64 n)
{
	return term_send(&p->term, b, n);
}

void
proc_setenv(struct Proc *p, const char *name, const char *val)
{
	int len = strlen(name) + 1 + strlen(val);
	char *env = malloc(len+1);
	int i;
	if (!env)
		die("out of memory\n");
	snprintf(env, len+1, "%s=%s", name, val);
	for (i = 0; i < SIZE(p->env)-1; i++) {
		if (p->env[i])
			continue;
		p->env[i] = env;
		return;
	}
	fprintf(stderr, "Too many environment variables. $%s not set.\n", name);
}

void
proc_set_file(struct Proc *p, struct File *f)
{
	char name[PATHLEN];
	str_copy(name, f->name, sizeof(name));
	proc_setwd(p, dirname(name));
	proc_setenv(p, "file", f->name);
	proc_setenv(p, "%", f->name);
	file_ref(&p->term.file, f, -1, -1);
}

void
proc_setwd(struct Proc *p, const char *dir)
{
	if (strlen(dir) + 1 > sizeof(p->term.wdir)) {
		fprintf(stderr, "can't set working directory for process: path too long\n");
		return;
	}
	str_copy(p->term.wdir, dir, sizeof(p->term.wdir));
}

static int
fork_redir(int *input, int *output)
{
	int infd[2], outfd[2];
	int err, pid;
	if (pipe(outfd)) {
		err = errno;
		perror("failed to create pipe");
		return -err;
	}
	if (pipe(infd)) {
		err = errno;
		perror("failed to create pipe");
		close(outfd[0]);
		close(outfd[1]);
		return -err;
	}
	pid = fork();
	if (pid == -1) {
		err = errno;
		perror("failed to fork");
		close(outfd[0]);
		close(outfd[1]);
		close(infd[0]);
		close(infd[1]);
		return -err;
	}
	if (pid == 0) {
		if (close(infd[1]))
			die("fork_redir:");
		if (input)
			*input = infd[0];
		else
			close(infd[0]);
		
		if (close(outfd[0]))
			die("fork_redir:");
		if (output)
			*output = outfd[1];
		else
			close(outfd[1]);
		return 0;
	}
	
	if (close(infd[0]))
		perror("fork_redir");
	if (input)
		*input = infd[1];
	else if (close(infd[1]))
		perror("fork_redir");
	
	if (close(outfd[1]))
		perror("fork_redir");
	if (output)
		*output = outfd[0];
	else if (close(outfd[0]))
		perror("fork_redir");
	return pid;
}

static int
movefd(int old, int new)
{
	int fd;
	fd = dup2(old, new);
	if (fd < 0)
		return fd;
	if (old != new)
		if (close(old))
			return -errno;
	return 0;
}


static int
runcommand(struct Proc *cmd)
{
	int inf, outf, pid;
	pid = fork_redir(&inf, &outf);
	if (pid < 0) {
		cmd->error = -pid;
		printf("failed to start command: %s", strerror(-pid));
		return -1;
	}
	if (pid == 0) {
		int ignore = 0;
		char **var;
		if (movefd(inf, 0))
			perror("new process: can't connect stdin");
		dup2(outf, 1);
		dup2(outf, 2);
		if (outf != 1 && outf != 2)
			if (close(outf))
				perror("new process: can't close stdout");
		for (var = cmd->env; *var; var++)
			putenv(*var);
		if (cmd->term.wdir[0])
			ignore = chdir(cmd->term.wdir);	/* wdir might not exist if started from a virtual file */
		(void) ignore;
		execvp(cmd->argv[0], cmd->argv);
		die("exec failed: %s:", cmd->argv[0]);
	}
	fcntl(inf, F_SETFL, O_NONBLOCK);
	fcntl(inf, F_SETFD, fcntl(inf, F_GETFD) | FD_CLOEXEC);
	cmd->term.pid = pid;
	cmd->term.infd = inf;
	return outf;
}

static void
runtranslate(struct Proc *cmd, int cmdout)
{
	int pid;
	int outf;
	pid = fork_redir(NULL, &outf);
	if (pid < 0) {
		printf("failed to start command output sanitizer: %s", strerror(-pid));
		return;
	}
	if (pid == 0) {
		assert(outf != 0);
		assert(cmdout != 1);
		if (movefd(cmdout, 0))
			perror("ansi2loz: can't connect stdin");
		if (movefd(outf, 1))
			perror("ansi2loz: can't connect stdout");
		execlp("ansi2loz", "ansi2loz", (char*) NULL);
		die("exec failed: ansi2loz:");
	}
	if (close(cmdout))
		perror("runtranslate");
	cmd->term.sanitizer_pid = pid;
	cmd->term.outfd = outf;
	fcntl(outf, F_SETFL, O_NONBLOCK);
	fcntl(outf, F_SETFD, FD_CLOEXEC);
}

void
proc_start(struct Proc *cmd)
{
	int outfd;
	cmd->next = procs;
	procs = cmd;
	outfd = runcommand(cmd);
	assert(outfd >= 0);
	runtranslate(cmd, outfd);
}
