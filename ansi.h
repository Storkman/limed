struct ANSIState;
typedef void* (ANSIStateFn)(struct ANSIState*, char*, int);

typedef struct ANSIState {
	ANSIStateFn *fn;
	char seqbuf[BLOCKSIZE];
	int pos, seqpos;
	int outfd;
} ANSIState;

void	ansi_init(ANSIState*);
int	ansi_run(int out, int in);
int	ansi_translate(ANSIState*, int fd, char *input, int len);
