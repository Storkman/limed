enum {
	OT_OK = 0,
	OT_ENCODING,	/* Font name uses an unsupported encoding. */
	OT_INTERNAL,	/* Internal error. This is a bug in the OTF parser. */
	OT_INVALID,	/* Table field has an invalid value. */
	OT_LONG_STRING,	/* String too long. */
	OT_NAME,	/* File is a collection but no font name was given. */
	OT_NOMEM,	/* Out of memory. */
	OT_NOTFOUND,	/* Font file not found. */
	OT_OFFSET,	/* Offset out of bounds. */
	OT_SIGNATURE,	/* Unrecognized file or table signature. */
	OT_SYSTEM,	/* System error; check sys_error for errno. */
	OT_UNNAMED,	/* Font does not contain a name entry. */
	OT_VERSION,	/* Unsupported data structure version. */
};

typedef unsigned int OTGlyph;

typedef struct OTFeature OTFeature;
typedef struct OTFont OTFont;
typedef struct OTOptions OTOptions;

struct OTFeature {
	char	name[5];	/* OpenType feature name, like 'dlig' */
	unsigned short	alt;	/* variant glyph set index */
};

struct OTFont {
	const char *full_name;
	
	bool loaded;
	int error;	/* OT_* error code */
	int sys_error;	/* set to errno if error == OT_SYSTEM */
	
	void *internal;
};

struct OTOptions {
	/* A vector of OpenType features to enable. Must be terminated by {0, 0, 0, 0}. */
	OTFeature *features;
};

OTFeature	OT_feature_parse(const char*);
 /* parses a string describing an OpenType feature to enable.
  * This is 4 ASCII characters, optionally followed by a space and a decimal number
  * indicating the index of the alternative glyph set to use.
  */

OTFont	OT_load(const char *path, const char *name, OTOptions*);
/* loads a font with the full name 'name' from the given file.
 * Setting 'name' to NULL will load the only font in the file, or raise an error if it's a collection (.TTC).
 * The returned font's full_name will then be set to its actual full name, if any.
 */

OTFont	OT_load_FcPattern(FcPattern*);
/* loads the font specified by the given FontConfig pattern.
 * The pattern must already be matched to a specific file with e.g. XftFontMatch.
 * If the "fontfeatures" property is set, it will be parsed for font features to enable.
 */

int	OT_map_glyphs(OTFont *font, OTGlyph*, int cap, int len);

const char*	OT_strerror(int error);

void	OT_unload(OTFont*);
/* frees all memory allocated for the given font.
 */
