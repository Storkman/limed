#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <locale.h>
#include <poll.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <utf.h>

#include <X11/XF86keysym.h>
#include <X11/XKBlib.h>
#include <X11/Xatom.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/Xresource.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/Xdbe.h>
#include <X11/keysym.h>

#include "def.h"
#include "ansi.h"
#include "text.h"
#include "parse.h"
#include "typo.h"
#include "typo-xft.h"
#include "layout.h"
#include "file.h"
#include "term.h"
#include "proc.h"
#include "xim.h"
#include "panel.h"
#include "window.h"
#include "lua.h"

#define AltMask Mod1Mask
#define AllModMask (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask)

/* Unicode "Box drawings light vertical" │ */
#define RUNE_VLINE 0x2502

struct Arg {
	const char *s;
	int i;
};

enum {UI_NONE, UI_ICON, UI_TAG, UI_SCROLL, UI_BODY};

struct Command {
	const char *name;
	void (*cmd)(struct Win*, const char *args);
};

enum {
	APPEND_SEL=1,	/* append the selection content (expanded if empty) to the argument */
};

struct KeyCommand {
	unsigned int mod;
	KeySym ksym;
	void (*cmd)(struct Win*, struct Arg *arg, char *args);
	struct Arg arg;
	int flags;
};

struct ScrollBar {
	int x, y, w, h;
	int p, len, max;
	struct Panel *panel;
};

struct XContext {
	struct PasteTarget paste;
	
	Window win;
	Pixmap surf;
	XftDraw *ftdraw;
	GC gc;
	Xim xim;
};

static void	change_apply(struct Win*, struct Change*, struct Change *new_state);

static void	cmd_clear(struct Win*, const char*);
static void	cmd_del(struct Win*, const char*);
static void	cmd_delete(struct Win*, const char*);
static void	cmd_exit(struct Win*, const char*);
static void	cmd_follow(struct Win*, const char*);
static void	cmd_get(struct Win*, const char*);
static void	cmd_margin(struct Win*, const char*);
static void	cmd_ps(struct Win*, const char*);
static void	cmd_put(struct Win*, const char*);
static void	cmd_redo(struct Win*, const char*);
static void	cmd_spaces(struct Win*, const char*);
static void	cmd_trim(struct Win*, const char*);
static void	cmd_undo(struct Win*, const char*);
static void	cmd_clone(struct Win*, const char*);

void die(const char *fmt, ...);

void	do_nothing(int);

struct Win*	findwin(Window);

int	get_file(Text*, int fd, int isplain);
struct Win*	getwin_error(const char *wdir);

static void	icon_button(struct Win*, XButtonEvent*);

static void	kcmd_exec(struct Win*, struct Arg*, char*);	/* execute arg.s as if it was clicked with MMB */
static void	kcmd_font_size(struct Win*, struct Arg*, char*);	/* add arg.i to body font size */
static void	kcmd_ins_line(struct Win*, struct Arg*, char*);
static void	kcmd_makecmd(struct Win*, struct Arg*, char*);	/* insert an empty formatting tag */
static void	kcmd_mark_go(struct Win*, struct Arg*, char*);	/* go to the top of mark stack and pop it */
static void	kcmd_mark_push(struct Win*, struct Arg*, char*);	/* push current position to the mark stack */
static void	kcmd_mark_set(struct Win*, struct Arg*, char*);	/* replace top of mark stack with current position */
static void	kcmd_mono(struct Win*, struct Arg*, char*);	/* toggle between proportional and monospace display */
static void	kcmd_paste(struct Win*, struct Arg*, char*);	/* paste from clipboard */
static void	kcmd_pipe(struct Win*, struct Arg*, char*);	/* pipe selection through arg.s */
static void	kcmd_plain(struct Win*, struct Arg*, char*);	/* toggle plain display mode */
static void	kcmd_proc(struct Win*, struct Arg*, char*);	/* execute arg.s with parameter as argument */
static void	kcmd_sel_all(struct Win*, struct Arg*, char*);	/* select the entire file */
static void	kcmd_send_eof(struct Win*, struct Arg*, char*);
static void	kcmd_send_line(struct Win*, struct Arg*, char*);
static void	kcmd_warp(struct Win*, struct Arg*, char*);	/* warp the cursor between body and tag */

static void	load_fonts(void);

static void	scroll_button(struct ScrollBar*, XButtonEvent*);
static void	scroll_draw(struct ScrollBar*, XftDraw *drw);
static int	scroll_motion(struct ScrollBar*, XMotionEvent*);
static struct ScrollBar*	scroll_new(struct Panel*);

static char*	skip_runes(const char*, int (*)(Rune), bool);
static int	spawn_quiet(char *const arg[]);

static char*	trim(char*);

static void	win_button(struct Win*, XButtonEvent*);
static void	win_change(void*, struct Panel*, struct Change*);
static void	win_changetag(void*, struct Panel*, struct Change*);
static void	win_enter(struct Win*, XFocusChangeEvent*);
static void	win_focus_in(struct Win*, XFocusChangeEvent*);
static void	win_focus_out(struct Win*, XFocusChangeEvent*);
static void	win_focused(struct Win*);
static void	win_follow(struct Win*, struct Change *c0, struct Change *c1);
static int	win_file_close(struct Win*, bool force);
static void	win_free(struct Win*);
static void	win_init(struct Win*);
static void	win_key(struct Win*, XKeyEvent*);
static void	win_look(void *hdata, struct Panel *p, int64 p0, int64 p1, int mod);
static void	win_motion(struct Win*, XMotionEvent*);
static struct Win*	win_new(long embed_id);
static void	win_panel_exec(void *hdata, struct Panel *p, int64 p0, int64 p1, int mod);
static void	win_redraw(struct Win*);
static void	win_resize(struct Win*);
static void	win_run_cmd(struct Win*, struct KeyCommand*);
static void	win_send_panel(struct Win*, struct Panel*, int64, int64);
static void	win_show(struct Win*, bool);
static void	win_start_proc(struct Win*, const char *cmd, bool redir_in, bool redir_out);
static void	win_tag_rewrite(struct Win*);
static void	win_ui_propnotify(struct Win*, XPropertyEvent*);
static void	win_ui_selnotify(struct Win*, XSelectionEvent*);
static void	win_update(struct Win*);

static void	xevents(void);
static int	xinit(void);
static void	xselreq(XSelectionRequestEvent*);
static void	xsel_clear(Atom selection);
static Atom	xsel_request_respond(XSelectionEvent *resp, XSelectionRequestEvent *ev);

static struct Command commands[] = {
	{"Clear",	cmd_clear},
	{"Clone",	cmd_clone},
	{"Del",	cmd_del},
	{"Delete",	cmd_delete},
	{"Exit",	cmd_exit},
	{"Follow",	cmd_follow},
	{"Get",	cmd_get},
	{"Margin",	cmd_margin},
	{"Ps",	cmd_ps},
	{"Put",	cmd_put},
	{"Redo",	cmd_redo},
	{"Spaces",	cmd_spaces},
	{"Trim",	cmd_trim },
	{"Undo",	cmd_undo},
};

#include "config.h"

static struct Win *windows;

Display *disp;
Window root, xembed_root;
int screen;

Window cutwin;
char *cutbuf;
int cutlen;

static XClassHint xclass;

void
change_apply(struct Win *w, struct Change *c, struct Change *new_state)
{
	struct Win *cw;
	w->file->history.last = new_state;
	text_change(w->file->txt, c->p0, c->p1, c->to, c->len);
	
	cw = w;
	do {
		win_tag_rewrite(cw);
		panel_changed(cw->body, c);
		cw->cur_change = new_state;
		cw = cw->clones;
	} while (cw != w);
	
	w->body->p0 = c->p0;
	w->body->p1 = c->p0 + c->len;
}

struct Win*
findwin(Window xw)
{
	struct Win *win;
	for (win = windows; win; win = win->next)
		if (win->xcontext->win == xw)
			return win;
	return NULL;
}

void
cmd_clone(struct Win *w, const char *args)
{
	struct Win *clone = win_new(0);
	Text *ct, *wt;
	
	clone->clones = w->clones;
	w->clones = clone;
	clone->file = w->file;
	
	ct = clone->tag->txt;
	wt = w->tag->txt;
	text_change(ct, 0, text_len(ct), text_all(wt), text_len(wt));
	
	win_init(clone);
	clone->cur_change = w->cur_change;
	clone->body->orig = w->body->orig;
	clone->body->p0 = w->body->p0;
	clone->body->p1 = w->body->p1;
	clone->body->indent_spaces = w->body->indent_spaces;
	clone->body->indent_trim = w->body->indent_trim;
	clone->tag->p0 = clone->tag->p1 = text_len(clone->tag->txt);
	win_show(clone, true);
}

void
cmd_clear(struct Win *win, const char *args)
{
	file_change(win->file, 0, text_len(win->file->txt), "", 0, SOURCE_CMD);
}

void
cmd_del(struct Win *win, const char *args)
{
	win_del(win, win->del_confirm);
}

void
cmd_delete(struct Win *win, const char *args)
{
	win_del(win, true);
}

void
cmd_exit(struct Win *w, const char *args)
{
	exit(0);
}

void
cmd_follow(struct Win *w, const char *args)
{
	if (strcmp(args, "on") == 0)
		w->follow = true;
	else if (strcmp(args, "off") == 0)
		w->follow = false;
	else {
		char buf[1024];
		size_t len = snprintf(buf, sizeof(buf), "Follow mode is %s\n", w->follow ? "ON" : "OFF");
		win_popup_warning(w, buf, len);
	}
}

void
cmd_get(struct Win *w, const char *args)
{
	char buf[1024];
	struct stat stb;
	Text *txt, *otxt;
	struct File *f = w->file;
	char *nt, *ot;
	int64 i, len, olen, nlen;
	int fd, err;
	if (f->virtual)
		return;
	txt = text_new();
	if (!txt) {
		fputs("Couldn't allocate file text.\n", stderr);
		return;
	}
	fd = open(w->file->name, O_RDONLY);
	if (fd < 0) {
		win_popup_warning(w, buf,
			snprintf(buf, sizeof(buf), "Couldn't open file: %s\n", strerror(errno)));
		text_free(txt);
		return;
	}
	if (fstat(fd, &stb)) {
		fprintf(stderr, "couldn't stat file: %s\n", strerror(errno));
	} else {
		w->file->mtime = stb.st_mtim;
	}
	err = get_file(txt, fd, w->file->plain);
	close(fd);
	if (err) {
		win_popup_warning(w, buf,
			snprintf(buf, sizeof(buf), "Couldn't read file: %s\n", strerror(err)));
		text_free(txt);
		return;
	}
	otxt = w->file->txt;
	ot = text_all(otxt);
	olen = text_len(otxt);
	nt = text_all(txt);
	nlen = text_len(txt);
	len = MIN(olen, nlen);
	for (i = 0; i < len; i++) {
		if (nt[i] != ot[i])
			break;
	}
	while(text_at(txt, i) == Runeerror || text_at(otxt, i) == Runeerror) {
		if (i == 0)
			break;
		i--;
	}
	panel_replace(w->body, i, olen, nt + i, nlen - i);
	w->file->save_chg = w->file->history.last;
	text_free(txt);
}

void
cmd_margin(struct Win *w, const char *args)
{
	char *e;
	int n;
	errno = 0;
	n = strtol(args, &e, 10);
	if (errno) {
		w->margin = 0;
		return;
	}
	w->margin = n;
}

void
cmd_ps(struct Win *w, const char *args)
{
	struct Win *msg;
	struct Proc *p;
	const char *header = "◊C+BS C◊ PID\tCommand\n◊0◊";
	msg = win_popup_new(w, "+++ Running commands +++", header, -1);
	
	for (p = procs; p; p = p->next) {
		char buf[256];
		int len;
		if (p->term.pid > 0)
			len = snprintf(buf, sizeof(buf), "%d\t%s\n", p->term.pid, p->argv[0]);
		else
			len = snprintf(buf, sizeof(buf), "[dead]\t%s\n", p->argv[0]);
		text_append(msg->file->txt, buf, len);
	}
	text_append(msg->file->txt, ".\n\n", 3);
	win_popup_init(msg);
	win_shrink(msg);
}

int
_check_mtime(const char *name, struct timespec *mt, struct timespec *mt2)
{
	struct stat st;
	if (stat(name, &st))
		return 1;
	*mt2 = st.st_mtim;
	return mt->tv_sec != mt2->tv_sec || mt->tv_nsec != mt2->tv_nsec;
}

void
cmd_put(struct Win *w, const char *args)
{
	struct timespec newmtime;
	struct Win *cw;
	const char *name;
	
	if (args && !args[0])
		args = NULL;
	if (w->file->virtual && !args) {
		perror("can't write a virtual file without a new path");
		return;
	}
	
	if (args) {
		if (file_put(w->file, args))
			perror("failed to write file");
		return;
	}
	
	name = w->file->name;
	if (!w->put_confirm && _check_mtime(name, &w->file->mtime, &newmtime)) {
		static const char *time_format = "%H:%M:%S %Y-%m-%d";
		char buf[2048], mtbuf[24], mtbuf2[24];
		struct tm mt, mt2;
		int len;
		
		localtime_r(&w->file->mtime.tv_sec, &mt);
		strftime(mtbuf, sizeof(mtbuf), time_format, &mt);
		
		localtime_r(&newmtime.tv_sec, &mt2);
		strftime(mtbuf2, sizeof(mtbuf2), time_format, &mt2);
		
		len = snprintf(buf, sizeof(buf),
			"File %s modified since last write.\n\n"
			"Last known save:\t%s\n"
			"Externally modified:\t%s\n",
			name, mtbuf, mtbuf2);
		win_popup_warning(w, buf, len);
		w->put_confirm = true;
		return;
	}
	
	if (file_put(w->file, name)) {
		perror("failed to write file");
		return;
	}
	
	file_autosave_del(w->file);
	w->file->save_chg = w->file->history.last;
	w->file->save_chg->final = true;
	w->file->readonly = 0;
	for (cw = w->clones; cw != w; cw = cw->clones)
		win_tag_rewrite(cw);
	w->put_confirm = false;
}

void
cmd_redo(struct Win *w, const char *args)
{
	struct Change *c = w->file->history.last->next;
	if (!c)
		return;
	do {
		change_apply(w, c, c);
		if (!c->next)
			break;
		c = c->next;
	} while (change_is_trivial(c));
	panel_focus(w->body, c->p0);
}

void
cmd_spaces(struct Win *w, const char *args)
{
	char *e;
	int n;
	errno = 0;
	n = strtol(args, &e, 10);
	w->body->indent_spaces = errno ? 0 : n;
}

void
cmd_trim(struct Win *w, const char *args)
{
	if (strlen(args) == 0)
		w->body->indent_trim = indent_trim_default;
	else if (strcmp(args, "on") == 0)
		w->body->indent_trim = true;
	else if (strcmp(args, "off") == 0)
		w->body->indent_trim = false;
}

void
cmd_undo(struct Win *w, const char *args)
{
	struct Change c = *w->file->history.last;
	for (;;) {
		if (!c.prev)
			break;
		change_rev(&c);
		change_apply(w, &c, c.prev);
		if (!change_is_trivial(&c))
			break;
		c = *c.prev;
	}
	panel_focus(w->body, c.p0);
}

void
die(const char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}
	
	abort();
}

void
errorwin_append(const char *wdir, const char *buf, int64 len)
{
	struct Win *w = getwin_error(wdir);
	int64 txtlen = text_len(w->body->txt);
	panel_replace(w->body, txtlen, txtlen, buf, len);
	w->body->p1 = txtlen + len;
	panel_focus(w->body, txtlen + len);
}

static char*
_append_escaped(Text *txt, char *buf, int len)
{
	char cmdesc[2*UTFmax];
	int rlen, skip, cmdlen;
	Rune r, cr = CMD_RUNE;
	
	cmdlen = runetochar(cmdesc, &cr);
	cmdlen += runetochar(cmdesc + cmdlen, &cr);
	
	while (len > 0) {
		rlen = skip = 0;
		do {
			skip += rlen;
			rlen = charntorune(&r, buf + skip, len - skip);
		} while (rlen > 0 && r != CMD_RUNE);
		text_append(txt, buf, skip);
		buf += skip;
		len -= skip;
		
		if (rlen == 0)
			return buf;
		text_append(txt, cmdesc, cmdlen);
		buf += rlen;
		len -= rlen;
	}
	return NULL;
}

int
get_file(Text *txt, int fd, int isplain)
{
	char buf[8*1024];
	int n = 0, rd;
	for (;;) {
		rd = read(fd, buf + n, sizeof(buf) - n);
		if (rd == 0) {
			if (n > 0)
				fprintf(stderr, "warning: discarded %d invalid UTF8 byte(s) at end of file\n", n);
			return 0;
		} else if (rd < 0) {
			return errno;
		}
		n += rd;
		if (isplain) {
			char *rem = _append_escaped(txt, buf, n);
			if (rem) {
				n -= rem - buf;
				memmove(buf, rem, n);
			} else
				n = 0;
		} else {
			text_append(txt, buf, n);
			n = 0;
		}
	}
}

struct Win*
getwin_error(const char *wdir)
{
	char name[PATHLEN+64];
	struct Win *w;
	if (wdir && wdir[0])
		snprintf(name, sizeof(name), "%s/+++Error+++", wdir);
	else
		str_copy(name, "+++Error+++", sizeof(name));
	for (w = windows; w; w = w->next)
		if (strcmp(w->file->name, name) == 0)
			return w;
	w = win_new(0);
	w->file = file_new();
	w->file->virtual = 1;
	str_copy(w->file->name, name, sizeof(w->file->name));
	text_append(w->tag->txt, " ", 1);
	win_init(w);
	win_show(w, true);
	return w;
}

void
icon_button(struct Win *w, XButtonEvent *ev)
{
	if (ev->type != ButtonRelease)
		return;
	switch (ev->button) {
	case Button2:
		win_collapse(w, !w->collapsed);
		break;
	}
}

void
kcmd_exec(struct Win *w, struct Arg *arg, char *args)
{
	win_exec(w, arg->s);
}

void
kcmd_font_size(struct Win *w, struct Arg *arg, char *args)
{
	int i;
	static const int toresize[] = {FNT_MAIN, FNT_MONO};
	for (i = 0; i < SIZE(toresize); i++) {
		double s = fonts[toresize[i]].size + arg->i;
		if (s <= 1)
			continue;
		fonts[toresize[i]].size = s;
	}
	load_fonts();
}

void
kcmd_ins_line(struct Win *w, struct Arg *arg, char *args)
{
	struct Panel *b = w->body;
	b->p0 = b->p1 = panel_eol(b, b->p1);
	panel_newline(b);
}

void
kcmd_warp(struct Win *w, struct Arg *arg, char *args)
{
	if (w->focus == w->tag) {
		panel_focus(w->body, w->body->p0);
		win_warp(w, w->body, 1);
	} else if (w->focus == w->body) {
		win_warp(w, w->tag, 1);
	}
}

void
kcmd_makecmd(struct Win *w, struct Arg *arg, char *args)
{
	struct Panel *b = w->body;
	char cr[8];
	int64 pos;
	int crlen;
	Rune rune = CMD_RUNE;
	crlen = runetochar(cr, &rune);
	runetochar(cr + crlen, &rune);
	if (b->p0 == b->p1) {
		if (b->parse_opts.plain)
			panel_replace(b, b->p0, b->p1, cr, crlen);
		else
			panel_replace(b, b->p0, b->p1, cr, 2*crlen);
		return;
	}
	pos = b->p0;
	panel_replace(b, b->p0, b->p0, cr, crlen);
	panel_replace(b, b->p1, b->p1, cr, crlen);
	b->p0 = pos;
}

void
kcmd_mark_go(struct Win *w, struct Arg *arg, char *args)
{
	w->body->orig = w->marks[w->mark_idx];
	if (w->mark_idx > 0)
		--w->mark_idx;
}

void
kcmd_mark_push(struct Win *w, struct Arg *arg, char *args)
{
	if (w->mark_idx + 1 >= SIZE(w->marks))
		memmove(w->marks, w->marks+1, sizeof(w->marks) - sizeof(w->marks[0]));
	else
		++w->mark_idx;
	w->marks[w->mark_idx] = w->body->orig;
}

void
kcmd_mark_set(struct Win *w, struct Arg *arg, char *args)
{
	w->marks[w->mark_idx] = w->body->orig;
}

void
kcmd_mono(struct Win *w, struct Arg *arg, char *args)
{
	struct DocNode *r = w->body->doc;
	node_freelist(r->next);
	r->next = NULL;
	if (r->layout == LAYOUT_VERBATIM) {
		r->layout = LAYOUT_PLAIN;
		r->attr &= ~ATR_MONO;
		w->body->indent_trim = indent_trim_default;
		return;
	}
	r->layout = LAYOUT_VERBATIM;
	r->attr |= ATR_MONO;
	w->body->indent_trim = true;
	if (r->tabw <= 0)
		r->tabw = 4;
}

void
kcmd_paste(struct Win *w, struct Arg *arg, char *args)
{
	panel_paste(w->body, xatom("CLIPBOARD"));
	w->body->p0 = w->body->p1;
}

void
kcmd_pipe(struct Win *w, struct Arg *arg, char *args)
{
	win_start_proc(w, arg->s, true, true);
}

void
kcmd_plain(struct Win *w, struct Arg *arg, char *args)
{
	struct Win *cw;
	int plain = !w->body->parse_opts.plain;
	panel_showplain(w->body, plain);
	for (cw = w->clones; cw != w; cw = cw->clones) {
		panel_showplain(cw->body, plain);
		cw->dirty = 1;
		win_update(cw);
	}
}

void
kcmd_proc(struct Win *w, struct Arg *arg, char *args)
{
	struct Proc *p = proc_new(arg->s);
	if (args)
		proc_add_arg(p, args);
	proc_set_file(p, w->file);
	p->term.eof = 1;
	proc_start(p);
}

void
kcmd_sel_all(struct Win *w, struct Arg *arg, char *args)
{
	panel_select(w->body, 0, text_len(w->body->txt));
}

void
kcmd_send_eof(struct Win *w, struct Arg *arg, char *args)
{
	struct File *f = w->file;
	if (!f->client)
		return;
	f->client->term.eof = 1;
}

void
kcmd_send_line(struct Win *w, struct Arg *arg, char *args)
{
	struct DocNode *n;
	struct Panel *p = w->body;
	int64 p0, p1;
	if (p->p0 != p->p1)
		return win_send_panel(w, p, p->p0, p->p1);

	p0 = panel_bol(p, p->p0);
	n = panel_parse_at(p, p0);
	while (n->attr & (ATR_PROMPT) || n->type == DOC_PROMPT)
		n = panel_parse_next(p, n);
	p0 = n->n0;
	
	while (n->type != DOC_EOF && n->type != DOC_NL)
		n = panel_parse_next(p, n);
	p1 = n->n0;
	n = panel_parse_next(p, n);
	
	win_send_panel(w, p, p0, p1);
}

void
load_fonts(void)
{
	int i;
	for (i = 0; i < SIZE(fonts); i++) {
		typo_unload_fonts(&fonts[i]);
		typo_load_fonts_deferred(&fonts[i]);
	}
}

void
scroll_button(struct ScrollBar *sc, XButtonEvent *ev)
{
	int y;
	if (ev->type != ButtonPress)
		return;
	switch (ev->button) {
	case Button1:
		y = ev->y - sc->y;
		if (y < 0)
			y = 0;
		else if (y > sc->h)
			y = sc->h;
		panel_seek(sc->panel, ((double) y / sc->h) * sc->max);
		break;
	case Button4:
	case Button5:
		panel_button(sc->panel, ev);
		break;
	}
}

void
scroll_draw(struct ScrollBar *sc, XftDraw *drw)
{
	typo_draw_rect(drw, sc->x, sc->y, sc->w, sc->h, COL_SCR);
	typo_draw_rect(drw, sc->w-1, sc->y, 1, sc->h, COL_SCR);
	if (sc->len == sc->max)
		typo_draw_rect(drw, sc->x, sc->y, sc->w - 1, sc->h, COL_SCR_BAR);
	else {
		int barh = (sc->h * (double) sc->len) / sc->max;
		if (barh < 1)
			barh = 1;
		typo_draw_rect(drw, sc->x, sc->y + (sc->h * (double) sc->p) / sc->max,
			sc->w - 1, barh, COL_SCR_BAR);
	}
}

struct ScrollBar*
scroll_new(struct Panel *p)
{
	struct ScrollBar *s = malloc(sizeof(*s));
	if (!s)
		die("out of memory\n");
	memset(s, 0, sizeof(*s));
	s->panel = p;
	return s;
}

int
scroll_motion(struct ScrollBar *sc, XMotionEvent *ev)
{
	int y;
	if (ev->state & Button1Mask) {
		y = ev->y - sc->y;
		if (y < 0)
			y = 0;
		else if (y > sc->h)
			y = sc->h;
		panel_seek(sc->panel, ((double) y / sc->h) * sc->max);
		return 1;
	}
	return 0;
}

void
scroll_update(struct ScrollBar *sc)
{
	sc->p = sc->panel->orig;
	sc->len = sc->panel->lastvisible->p - sc->p;
	sc->max = text_len(sc->panel->txt);
}

/* skip_runes advances the given pointer past code points for which the given predicate returns cond. */
static char*
skip_runes(const char *str, int (*pred)(Rune), bool cond)
{
	int n;
	Rune r;
	while (*str) {
		n = chartorune(&r, str);
		if ((!!pred(r)) != cond)
			break;
		str += n;
	}
	return (char*) str;
}

int
spawn_quiet(char *const arg[])
{
	int drop, pid = fork();
	if (pid < 0) {
		perror("failed to fork");
		return pid;
	}
	if (pid > 0)
		return pid;
	drop = open("/dev/null", O_RDWR);
	dup2(drop, 0);
	dup2(drop, 1);
	dup2(drop, 2);
	execvp(arg[0], arg);
	die("exec failed:");
	return -1;
}

char*
str_append(char *dst, const char *src, size_t capacity)
{
	size_t dstlen, slen, remaining;
	dstlen = strnlen(dst, capacity);
	if (dstlen == capacity)
		die("str_append: destination buffer unterminated\n");
	slen = strlen(src);
	remaining = capacity - dstlen - 1;
	if (slen > remaining)
		slen = remaining;
	memmove(dst + dstlen, src, slen);
	dst[dstlen + slen] = 0;
	return dst;
}

char*
str_copy(char *dst, const char *src, size_t capacity)
{
	size_t sl = strlen(src);
	if (sl + 1 > capacity)
		sl = capacity - 1;
	memmove(dst, src, sl);
	dst[sl] = 0;
	return dst;
}

/* trim removes leading and trailing whitespace. */
char*
trim(char *str)
{
	char *e, *se;
	se = str = skip_runes(str, isspacerune, true);
	do {
		e = skip_runes(se, isspacerune, false);
		se = skip_runes(e, isspacerune, true);
	} while (*se);
	*e = 0;
	return str;
}

static int
_uiwhich(struct Win *w, int x, int y)
{
	if (y < w->tag->h) {
		if (x < SCROLLW)
			return UI_ICON;
		else
			return UI_TAG;
	} else if (y == w->tag->h)
		return UI_NONE;
	else if (x <= w->scroll->w)
		return UI_SCROLL;
	else
		return UI_BODY;
}

void
win_button(struct Win *win, XButtonEvent *ev)
{
	if (!win->mgrab)
		win->mgrab = _uiwhich(win, ev->x, ev->y);
	switch (win->mgrab) {
	case UI_ICON:
		icon_button(win, ev);
		break;
	case UI_TAG:
		panel_button(win->tag, ev);
		break;
	case UI_SCROLL:
		scroll_button(win->scroll, ev);
		break;
	case UI_BODY:
		panel_button(win->body, ev);
		break;
	default:
		return;
	}
	win->dirty = 1;
}

void
win_change(void *hdata, struct Panel *p, struct Change *c)
{
	struct Win *w = hdata, *cw;
	struct Change *newstate;
	newstate = file_add_change(w->file, c);
	w->cur_change = newstate;
	win_tag_rewrite(w);
	node_discard(p->doc, c->p0);
	for (cw = w->clones; cw != w; cw = cw->clones) {
		cw->cur_change = newstate;
		if (c)
			panel_changed(cw->body, c);
		win_tag_rewrite(cw);
	}
	w->del_confirm = w->put_confirm = false;
}

void
win_changetag(void *hdata, struct Panel *p, struct Change *c)
{
	struct Win *w = hdata;
	struct Win *cw;
	struct DocNode *n;
	char name[PATHLEN], *sep;
	int np = 0;
	
	text_change(p->txt, c->p0, c->p1, c->to, c->len);
	
	/* Find the first part of the tag (before the vertical line), and set it as the file name. */
	for (n = p->doc; n->type != DOC_EOF; n = panel_parse_next(p, n)) {
		if (n->attr & ATR_RDONLY)
			break;
		np += node_plain_text(name + np, sizeof(name) - np, p->txt, n);
	}
	
	sep = utfrune(name, RUNE_VLINE);
	if (sep)
		*sep = 0;
	str_copy(w->file->name, trim(name), sep - name);
	
	win_tag_rewrite(w);
	for (cw = w->clones; cw != w; cw = cw->clones)
		win_tag_rewrite(cw);
}

void
win_collapse(struct Win *w, int collapse)
{
	XSizeHints *xs = XAllocSizeHints();
	w->collapsed = collapse;
	if (!collapse) {
		xs->flags = PMaxSize;
		xs->max_width = xs->max_height = 9999999;
	} else {
		xs->flags = PMaxSize | PBaseSize;
		xs->max_width = 9999999;
		xs->base_width = 10;
		xs->base_height = xs->max_height = w->tag->h + 1;
	}
	XSetWMNormalHints(disp, w->xcontext->win, xs);
	XResizeWindow(disp, w->xcontext->win, w->w, xs->max_height);
	XFree(xs);
}

void
win_del(struct Win *win, bool force)
{
	struct Win *p;
	if (win->clones == win) {
		if (win_file_close(win, force))
			return;
	} else {
		for (p = win->clones; p->clones != win; p = p->clones) {}
		p->clones = win->clones;
	}
	
	if (!win->master) {
		p = windows;
		while (p) {
			struct Win *np = p->next;
			if (p->master == win)
				win_del(p, true);
			p = np;
		}
	}
	win->deleted = true;
}

void
win_enter(struct Win *w, XFocusChangeEvent *ev)
{
	win_focused(w);
}

static int
_internal_command(struct Win *w, const char *cmd)
{
	char name[64];
	int cn = 0;
	const char *args;
	cmd = skip_runes(cmd, isspacerune, true);
	args = skip_runes(cmd, isspacerune, false);
	if (args - cmd >= sizeof(name))
		return 0;
	memmove(name, cmd, args - cmd);
	name[args - cmd] = 0;
	args = skip_runes(args, isspacerune, true);
	for (cn = 0; cn < SIZE(commands); cn++) {
		if (strcmp(name, commands[cn].name) == 0) {
			commands[cn].cmd(w, args);
			return 1;
		}
	}
	return 0;
}

void
win_exec(struct Win *w, const char *str)
{
	bool shellout, shellin;
	shellout = shellin = false;
	switch (str[0]) {
	case '>':
		shellout = true;
		break;
	case '<':
		shellin = true;
		break;
	case '|':
		shellout = shellin = true;
		break;
	default:
		break;
	}
	
	if (!shellin && !shellout && _internal_command(w, str))
		return;
	
	if (shellin || shellout)
		str++;
	win_start_proc(w, str, shellin, shellout);
}

int
win_file_close(struct Win *win, bool force)
{
	struct File *file = win->file;
	char buf[PATHLEN+1024];
	int len;
	win->del_confirm = true;
	if (!force && file->client) {
		win_popup_warning(win, "Client still running.\n\n\tExit\tPs\n\n", -1);
		return 1;
	}
	if (file->virtual || file->readonly)
		return 0;
	if (file->save_chg == file->history.last) {
		file_autosave_del(file);
		return 0;
	}
	if (force)
		return 0;
	len = snprintf(buf, sizeof(buf), "%s modified\n\n", file->name);
	win_popup_warning(win, buf, len);
	return 1;
}

struct Win*
win_find_file(struct File *f)
{
	struct Win *sel = NULL;
	struct Win *w;
	for (w = windows; w; w = w->next) {
		if (w->file != f)
			continue;
		if (!sel || sel->last_focused < w->last_focused)
			sel = w;
	}
	return sel;
}

void
win_focus_in(struct Win *w, XFocusChangeEvent *ev)
{
	win_focused(w);
	xim_focus_in(&w->xcontext->xim);
}

void
win_focus_out(struct Win *w, XFocusChangeEvent *ev)
{
	xim_focus_out(&w->xcontext->xim);
}

void
win_focused(struct Win *w)
{
	struct timespec t;
	if (clock_gettime(CLOCK_MONOTONIC, &t))
		return;
	w->last_focused = t.tv_sec * 1000 + t.tv_nsec / 1000000;
	
	if (w->warp) {
		win_warp(w, w->body, 1);
		w->dirty = true;
		w->warp = false;
	}
	
	if (w->focus)
		panel_focus_in(w->focus);
}

void
win_follow(struct Win *w, struct Change *c0, struct Change *c1)
{
	struct Panel *b = w->body;
	if (!c0 || !c1)
		return;
	if (c0->p0 > b->lastvisible->p || c0->p1 < b->firstvisible->p)
		return;
	panel_focus(b, c1->p0 + c1->len);
}

static int
_user_tag_text_pos(struct Panel *t)
{
	struct DocNode *n;
	char *sep, *tt;
	Rune r;
	
	tt = text_all(t->txt);
	sep = utfrune(tt, RUNE_VLINE);
	if (!sep)
		return text_len(t->txt);
	sep += chartorune(&r, sep);
	
	n = panel_parse_at(t, sep - tt);
	while ((n->attr & ATR_RDONLY) || n->type == DOC_ATTR)
		n = panel_parse_next(t, n);
	return n->n0;
}

void
win_tag_rewrite(struct Win *w)
{
	char tags[4096];
	int taglen, newlen;
	struct Panel *t = w->tag;
	
	{ /* Compose a new tag line. */
		const char *undo = w->file->history.last->prev ? "Undo " : "";
		const char *redo, *put;
		if (w->file->history.last)
			redo = w->file->history.last->next ? "Redo " : "";
		else
			redo = w->file->history.first ? "Redo " : "";
		put = (w->file->virtual || w->file->readonly) ? "" : "Put ";
		newlen = snprintf(tags, sizeof(tags), "%s ◊+R◊│ Del %s%s%s│◊-R◊", w->file->name, undo, redo, put);
	}
	
	taglen = _user_tag_text_pos(t);
	if (t->p0 > taglen) {
		t->p0 += newlen - taglen;
		t->p1 += newlen - taglen;
	}
	text_change(t->txt, 0, taglen, tags, newlen);
	node_freelist(t->doc->next);
	t->doc->next = NULL;
	
	{ /* Set X11 properties */
		char buf[PATHLEN+16];
		char *title = buf;
		str_copy(buf, w->file->name, sizeof(buf));
		title = basename(buf);
		XStoreName(disp, w->xcontext->win, title);
		XChangeProperty(disp, w->xcontext->win,
			xatom("_EDITOR_PATH"), XA_STRING, 8, PropModeReplace,
			(unsigned char*) w->file->name, strlen(w->file->name));
	}
	w->dirty = 1;
}

void
win_free(struct Win *w)
{
	(void) text_free(w->tag->txt);
	panel_free(w->tag);
	panel_free(w->body);
	free(w->scroll);
	if (w->clones == w)
		file_free(w->file);
	xim_deinit(&w->xcontext->xim);
	XDestroyWindow(disp, w->xcontext->win);
	free(w);
}

static void
_init_xcontext(struct XContext *w, struct XContext *parent)
{
	Atom wmdel = xatom("WM_DELETE_WINDOW");
	
	w->gc = XCreateGC(disp, w->win, 0, NULL);
	XSetGraphicsExposures(disp, w->gc, True);
	
	w->paste.at = -1;
	w->paste.prop = 0;
	w->paste.win = w->win;
	w->paste.panel = NULL;
	
	XSetClassHint(disp, w->win, &xclass);
	XSetWMProtocols(disp, w->win, &wmdel, 1);
	if (parent)
		XSetTransientForHint(disp, w->win, parent->win);
	XMapWindow(disp, w->win);
	xim_init(&w->xim, w->win);
}

void
win_init(struct Win *w)
{
	if (!w->file)
		w->file = file_new();
	w->cur_change = w->file->history.last;
	if (!w->file->name[0])
		str_copy(w->file->name, "limed", sizeof(w->file->name));
	w->body = panel_new(w->file->txt, fonts+FNT_MAIN, fonts+FNT_MONO, COL_FG, COL_BG);
	w->body->doc = w->file->doc;
	w->body->ebg = COL_BG0;
	w->body->onchange = win_change;
	w->body->onlook = w->tag->onlook = win_look;
	w->body->onexec = w->tag->onexec = win_panel_exec;
	w->body->handler_data = w->tag->handler_data = w;
	w->body->indent_trim = indent_trim_default;
	panel_layout(w->body);
	w->scroll = scroll_new(w->body);
	w->focus = w->body;
	
	_init_xcontext(w->xcontext, w->master ? w->master->xcontext : NULL);
	w->body->paste = &w->xcontext->paste;
	w->body->xim = &w->xcontext->xim;
	w->tag->paste = &w->xcontext->paste;
	w->tag->xim = &w->xcontext->xim;
	
	panel_shrinkwrap(w->tag, w->h - 12);
	w->resize = 1;
	win_resize(w);
	
	win_tag_rewrite(w);
	w->tag->p0 = w->tag->p1 = text_len(w->tag->txt);
	
	w->next = windows;
	windows = w;
}

static struct KeyCommand term_keys[] = {
	{ ControlMask,	XK_d,	kcmd_send_eof,	{} },
	{ 0,	XK_Return,	kcmd_send_line,	{} },
};

int
_run_kcmd(struct Win *win, struct KeyCommand *cmds, int n, KeySym ksym, XKeyEvent *e)
{
	int i;
	for (i = 0; i < n; i++) {
		if (ksym != cmds[i].ksym
		|| (cmds[i].mod & AllModMask) != (e->state & AllModMask))
			continue;
		if (e->type == KeyRelease)
			continue;
		win_run_cmd(win, &cmds[i]);
		return 1;
	}
	return 0;
}

void
win_key(struct Win *win, XKeyEvent *e)
{
	char buf[64];
	KeySym ksym;
	XLookupString(e, buf, sizeof(buf), &ksym, NULL);
	if (_run_kcmd(win, winkeys, SIZE(winkeys), ksym, e)) {
		win->dirty = 1;
		return;
	}
	if (!win->focus)
		return;
	if (win->focus == win->body && win->file->client)
		if (_run_kcmd(win, term_keys, SIZE(winkeys), ksym, e)) {
			win->dirty = 1;
			return;
		}
	panel_key(win->focus, e);
	win->dirty = 1;
}

static int
_trysearch(	struct Panel *p, struct Panel *body,
	int64 p0, int64 p1,
	CharClass *expandfn,
	int (*searchfn)(struct Panel*, int64, int64, char*, int64))
{
	int64 s0, s1, b0, b1;
	if (p0 == p1)
		panel_expandselword(p, p0, &s0, &s1, expandfn);
	else {
		s0 = p0; s1 = p1;
	}
	b0 = p == body ? s0 : body->p0;
	b1 = p == body ? s1 : body->p1;
	return searchfn(body, b0, b1, text_ref(p->txt, s0, s1), s1 - s0);
}

static pid_t
waitpid_timeout(pid_t pid, int *wstat, int options, int waitms)
{
	struct itimerspec spec = {0};
	timer_t timer;
	pid_t rpid;
	if (timer_create(CLOCK_MONOTONIC, NULL, &timer)) {
		perror("couldn't create timer");
		return -1;
	}
	spec.it_value.tv_sec = waitms / 1000;
	spec.it_value.tv_nsec = (waitms % 1000) * 1e6;
	if (timer_settime(timer, 0, &spec, NULL)) {
		perror("couldn't set timer");
		timer_delete(timer);
		return -1;
	}
	rpid = waitpid(pid, wstat, options);
	if (rpid < 0) {
		if (errno == EINTR)
			return 0;
		perror("waitpid");
		timer_delete(timer);
		return -1;
	}
	timer_delete(timer);
	return rpid;
}

static bool
_tryplumb(struct File *file, struct Panel *p, int64 p0, int64 p1)
{
	char clickattr[64];
	char arg[2048];
	char dir[PATHLEN], name[PATHLEN];
	char *const plumbc[] = {"plumb", "-w", dir, "-a", clickattr, arg, NULL};
	char *const plumbnc[] = {"plumb", "-w", dir, arg, NULL};
	int64 b, e, n;
	int pid, rpid, wstat;
	str_copy(name, file->name, sizeof(name));
	str_copy(dir, dirname(name), sizeof(dir));
	if (p0 != p1) {
		n = text_read(p->txt, p0, arg, MIN(p1 - p0, sizeof(arg)-1));
		arg[n] = 0;
		pid = spawn_quiet(plumbnc);
	} else {
		panel_expandselword(p, p0, &b, &e, not_name_rune);
		if (b < p0 - (int64) sizeof(arg)/2)
			b = p0 - (int64) sizeof(arg)/2;
		n = text_read(p->txt, b, arg, MIN(e-b, sizeof(arg)-1));
		arg[n] = 0;
		snprintf(clickattr, sizeof(clickattr), "click=%ld", utfnlen(arg, p0 - b));
		pid = spawn_quiet(plumbc);
	}
	if (pid < 0)
		return false;
	rpid = waitpid_timeout(pid, &wstat, 0, 10);
	if (rpid == 0) {
		fprintf(stderr, "_tryplumb: timed out\n");
		return false;
	}
	if (rpid < 0 && waitpid(pid, &wstat, 0) < 0)
		return false;
	return WIFEXITED(wstat) && !WEXITSTATUS(wstat);
}

static int
_tryaddr(struct Panel *p, int64 p0, int64 p1, char *s, int64 len)
{
	if (len < 2 || s[0] != ':')
		return 0;
	s++;
	len--;
	panel_searchaddr(p, p0, p1, s, len);
	return 1;
}

void
win_look(void *hdata, struct Panel *p, int64 p0, int64 p1, int mod)
{
	struct Win *w = hdata;
	struct Panel *b = w->body;
	int64 cur = b->p0;
	if (_trysearch(p, b, p0, p1, not_name_rune, _tryaddr))
		goto FOUND;
	if (_tryplumb(w->file, p, p0, p1))
		return;
	if (_trysearch(p, b, p0, p1, not_word_rune, mod ? panel_search_rev : panel_search))
		goto FOUND;
	return;
FOUND:
	if (b->p0 == cur)
		return;
	panel_focus(b, b->p0);
	if (p == b)
		win_warp(w, b, 0);
	return;
}

void
win_motion(struct Win *win, XMotionEvent *ev)
{
	win->pointerx = ev->x;
	win->pointery = ev->y;
	if ((ev->state & (Button1Mask|Button2Mask|Button3Mask)) == 0)
		win->mgrab = UI_NONE;
	switch (win->mgrab ? win->mgrab : _uiwhich(win, ev->x, ev->y)) {
	case UI_TAG:
		if (win->focus != win->tag)
			panel_focus_in(win->tag);
		if (panel_motion(win->tag, ev))
			win->dirty = 1;
		win->focus = win->tag;
		break;
	case UI_SCROLL:
		if (scroll_motion(win->scroll, ev))
			win->dirty = 1;
		break;
	case UI_BODY:
		if (win->focus != win->body)
			panel_focus_in(win->body);
		if (panel_motion(win->body, ev))
			win->dirty = 1;
		win->focus = win->body;
		break;
	default:
		return;
	}
}

struct Win*
win_new(long parent_id)
{
	XSetWindowAttributes attr = {
		.event_mask = StructureNotifyMask |
			PointerMotionMask | ButtonPressMask | ButtonReleaseMask | FocusChangeMask |
			KeyPressMask | KeyReleaseMask | PropertyChangeMask | ExposureMask,
	};
	struct Win *w = malloc(sizeof(*w));
	if (!w)
		die("out of memory\n");
	memset(w, 0, sizeof(*w));
	w->w = popup_width;
	w->h = popup_height;
	w->tag = panel_new(NULL, fonts+FNT_UI, fonts+FNT_MONO, COL_FG, COL_TAG);
	w->tag->onchange = win_changetag;
	w->tag->disable_scrolling = 1;
	w->clones = w;
	
	w->xcontext = calloc(1, sizeof(*w->xcontext));
	if (!parent_id)
		parent_id = root;
	w->xcontext->win = XCreateWindow(disp, parent_id, 0, 0, w->w, w->h, 0,
		DefaultDepth(disp, screen), InputOutput, DefaultVisual(disp, screen),
		CWEventMask, &attr);
	return w;
}

void
win_panel_exec(void *hdata, struct Panel *p, int64 p0, int64 p1, int mod)
{
	/* mod currently ignored */
	int64 len;
	struct Win *w = hdata;
	char buf[4096];
	
	if (p0 == p1)
		panel_expandselword(p, p0, &p0, &p1, not_name_rune);
	
	if (p1 - p0 >= sizeof(buf)) {
		const char *err = "command ignored: selection too long\n";
		errorwin_append(NULL, err, strlen(err));
		return;
	}
	
	len = text_read(p->txt, p0, buf, p1 - p0);
	buf[len] = 0;
	win_exec(w, trim(buf));
}

void
win_popup_init(struct Win *popup)
{
	struct Win *parent = popup->master;
	Window dw;
	int x, y;
	XTranslateCoordinates(disp, parent->xcontext->win, root, 0, 0, &x, &y, &dw);
	XMoveWindow(disp, popup->xcontext->win,
		x + parent->pointerx + popup_x,
		y + parent->pointery + popup_y);
	win_init(popup);
	win_show(popup, true);
}

struct Win*
win_popup_new(struct Win *parent, const char *name, const char *text, int len)
{
	struct Win *msg;
	if (len < 0)
		len = strlen(text);
	msg = win_new(0);
	msg->master = parent;
	msg->file = file_new();
	msg->file->virtual = 1;
	str_copy(msg->file->name, name, sizeof(msg->file->name));
	text_append(msg->file->txt, text, len);
	return msg;
}

void
win_popup_warning(struct Win *parent, const char *text, int len)
{
	struct Win *msg = win_popup_new(parent, "+++ Warning +++", text, len);
	win_popup_init(msg);
	win_shrink(msg);
}

#define RUNE_GUILL 0x00ab
#define RUNE_GUILR 0x00bb
#define RUNE_BETA 0x03b2

void
win_redraw(struct Win *win)
{
	Rune icon = RUNE_GUILR;
	FontExtents ext;
	FontList *font = win->tag->fonts[FNT_MAIN];
	int isdirty = win->file->virtual || win->file->history.last != win->file->save_chg;
	Surface *typo_ctx = win->xcontext->ftdraw;
	
	panel_layout(win->tag);
	panel_shrinkwrap(win->tag, win->h - 24);
	win_resize(win);
	
	if (win->file->client)
		icon = RUNE_GUILL;
	else if (win->file->readonly)
		icon = 'R';
	else if (win->master && win->file->virtual)
		icon = RUNE_BETA;
	
	typo_draw_rect(typo_ctx,
		0, 0, win->tag->x, win->tag->h,
		isdirty ? COL_TAG_DIRTY : COL_TAG_CUR);
	typo_measure_rune(font, ATR_BOLD,  icon, &ext);
	typo_draw_rune(typo_ctx, SCROLLW/2 - ext.advance/2, win->tag->h/2 + ext.y - ext.h/2,
		font, ATR_BOLD, isdirty ? COL_TAG_CUR : COL_FG, icon);
	
	panel_draw(win->tag, typo_ctx);
	typo_draw_rect(typo_ctx, 0, win->tag->h, win->w, 1, isdirty ? COL_TAG_DIRTY : COL_TAG);
	
	if (win->collapsed)
		goto DONE;
	
	panel_layout(win->body);
	panel_draw(win->body, typo_ctx);
	scroll_update(win->scroll);
	scroll_draw(win->scroll, typo_ctx);
	
	if (win->margin) {
		typo_measure_rune(win->body->fonts[FNT_MONO], 0, '0', &ext);
		typo_draw_rect(typo_ctx,
			win->body->x + PANEL_MARGIN + win->margin * ext.advance, win->body->y,
			1, win->body->h, COL_MARGIN);
	}
	
DONE:
	XCopyArea(disp, win->xcontext->surf, win->xcontext->win, win->xcontext->gc, 0, 0, win->w, win->h, 0, 0);
	win->dirty = 0;
}

void
_resize_xcontext(struct XContext *win, int w, int h)
{
	if (win->surf)
		XFreePixmap(disp, win->surf);
	win->surf = XCreatePixmap(disp, root, w, h, DefaultDepth(disp, screen));
	if (win->ftdraw)
		XftDrawDestroy(win->ftdraw);
	win->ftdraw = XftDrawCreate(disp, win->surf, DefaultVisual(disp, screen), DefaultColormap(disp, screen));
}

void
win_resize(struct Win *win)
{
	win->tag->x = SCROLLW;
	win->tag->y = 0;
	panel_resize(win->tag, win->w, win->tag->h);
	if (win->resize)
		panel_shrinkwrap(win->tag, win->h - 24);
	
	win->scroll->x = 0;
	win->scroll->y = win->tag->h + 1;
	win->scroll->w = SCROLLW;
	win->scroll->h = win->h - win->tag->h;
	
	win->body->x = SCROLLW;
	win->body->y = win->tag->h + 1;
	panel_resize(win->body,
		win->w - SCROLLW,
		win->h - win->tag->h - 1);
	
	if (!win->resize)
		return;
	_resize_xcontext(win->xcontext, win->w, win->h);
	win->resize = 0;
}

void
win_run_cmd(struct Win *win, struct KeyCommand *cmd)
{
	char sarg[4096];
	int64 p0, p1, len;
	sarg[0] = 0;
	if (cmd->flags & APPEND_SEL) {
		p0 = win->focus->p0;
		p1 = win->focus->p1;
		if (p0 == p1)
			panel_expandselword(win->focus, p0, &p0, &p1, not_word_rune);
		if (p1 - p0 >= sizeof(sarg)) {
			const char *err = "key command ignored: selection too long\n";
			errorwin_append(NULL, err, strlen(err));
			return;
		}
		len = text_read(win->focus->txt, p0, sarg, p1 - p0);
		sarg[len] = 0;
	}
	cmd->cmd(win, &cmd->arg, sarg);
}

void
win_send_panel(struct Win *w, struct Panel *p, int64 p0, int64 p1)
{
	struct Panel *b = w->body;
	struct Proc *client = w->file->client;
	const char *data = text_ref(p->txt, p0, p1);
	int64 end = text_len(b->txt);
	if (p1 < end) {
		panel_replace(b, end, end, data, p1-p0);
		end += p1-p0;
	}
	panel_replace(b, end, end, "\n", 1);
	b->p0 = b->p1 = end + 1;
	
	if (!client)
		return;
	data = text_ref(p->txt, p0, p1);
	proc_send(client, data, p1-p0);
	proc_send(client, "\n", 1);
}

void
win_show(struct Win *w, bool s)
{
	if (s)
		XMapWindow(disp, w->xcontext->win);
	else
		XUnmapWindow(disp, w->xcontext->win);
}

void
_setenv_indent(struct Proc *proc, struct Panel *panel)
{
	char buf[16];
	int ind;
	ind = panel_indent_spaces(panel, panel->p0);
	if (ind == 0)
		return proc_setenv(proc, "indent", "\t");
	if (ind > sizeof(buf) - 1)
		return;
	memset(buf, ' ', ind);
	buf[ind] = 0;
	proc_setenv(proc, "indent", buf);
}

static int
_which_line(Text *t, int64 p)
{
	int64 len = text_len(t);
	const char *s = text_all(t);
	const char *n = s;
	int l = 1;
	for (;;) {
		n = memchr(n, '\n', len - (n - s));
		if (!n || p <= n - s)
			return l;
		l++;
		n++;
	}
}

void
win_start_proc(struct Win *w, const char *cmd, bool redir_in, bool redir_out)
{
	struct Proc *proc;
	const char *shell = getenv("SHELL");
	char linebuf[64];
	int line;
	
	line = _which_line(w->file->txt, w->body->p0);
	snprintf(linebuf, sizeof(linebuf), "%d", line);
	
	if (!shell)
		shell = "/bin/sh";
	proc = proc_new(getenv("SHELL"));
	proc_add_arg(proc, "-c");
	proc_add_arg(proc, cmd);
	proc_set_file(proc, w->file);
	proc_setenv(proc, "line", linebuf);
	_setenv_indent(proc, w->body);
	if (redir_in)
		proc_insert_at(proc, w->file, w->body->p0, w->body->p1);
	if (redir_out) {
		char *in = text_ref(w->body->txt, w->body->p0, w->body->p1);
		proc_send(proc, in, w->body->p1 - w->body->p0);
	}
	proc->term.eof = 1;
	proc_start(proc);
}

void
win_shrink(struct Win *w)
{
	XSizeHints *xs;
	if (!w)
		return;
	xs = XAllocSizeHints();
	panel_shrinkwrap(w->body, 16000);
	xs->flags = PMaxSize | PBaseSize;
	xs->max_width = 9999999;
	xs->base_width = w->w;
	xs->base_height = xs->max_height = w->tag->h + 1 + w->body->h;
	XSetWMNormalHints(disp, w->xcontext->win, xs);
	XResizeWindow(disp, w->xcontext->win, w->w, xs->max_height);
	XFree(xs);
}

void
win_ui_propnotify(struct Win *w, XPropertyEvent *ev)
{
	Atom type;
	int format;
	unsigned long n, rem;
	unsigned char *data;
	struct PasteTarget *paste = &w->xcontext->paste;
	
	Atom ustring = xatom("UTF8_STRING");
	if (ev->state == PropertyDelete)
		return;
	if (ev->atom == paste->prop) {
		long off = 0;
		do {
			XGetWindowProperty(disp, w->xcontext->win, ev->atom, off / 4, 1024,
				True, ustring, &type, &format, &n, &rem, (unsigned char**) &data);
			if (off == 0 && n == 0) {
				paste->panel = NULL;
				paste->at = -1;
				paste->prop = 0;
				return;
			}
			panel_replace(paste->panel, paste->at, paste->at, (char*) data + off % 4, n - off % 4);
			n -= off % 4;
			off += n;
			paste->at += n;
			paste->panel->p0 = paste->p0;
			paste->panel->p1 = paste->at;
			XFree(data);
		} while (rem > 0);
		return;
	}
	if (ev->atom == xatom("_EDITOR_GOTO")) {
		XEvent xev;
		XGetWindowProperty(disp, w->xcontext->win,
			ev->atom, 0, 1024, False, XA_STRING,
			&type, &format, &n, &rem, &data);
		if (type != XA_STRING || rem > 0)
			return;
		if (n > 0) {
			kcmd_mark_push(w, NULL, NULL);
			panel_searchaddr(w->body, 0, 0, (char*) data, n);
		}
		XFree(data);
		
		memset(&xev, 0, sizeof(xev));
		xev.xclient.type = ClientMessage;
		xev.xclient.send_event = True;
		xev.xclient.window = w->xcontext->win;
		xev.xclient.message_type = xatom("_NET_ACTIVE_WINDOW");
		xev.xclient.format = 32;
		XSendEvent(disp, xembed_root, False,
			SubstructureRedirectMask|SubstructureNotifyMask, &xev);
		
		win_collapse(w, 0);
		panel_focus(w->body, w->body->p0);
		w->warp = true;
		return;
	}
}

void
win_ui_selnotify(struct Win *w, XSelectionEvent *ev)
{
	struct PasteTarget *paste = &w->xcontext->paste;
	Atom type;
	Atom ustring = xatom("UTF8_STRING");
	int format;
	unsigned long n, rem;
	long off;
	unsigned char *data;
	if (!paste->panel)
		return;
	if (ev->property == None) {
		paste->panel = NULL;
		paste->at = -1;
		paste->prop = 0;
		return;
	}
	off = 0;
	do {
		XGetWindowProperty(disp, w->xcontext->win, ev->property, off / 4, 1024,
			False, ustring, &type, &format, &n, &rem, (unsigned char**) &data);
		if (n == 0)
			break;
		if (type == xatom("INCR")) {
			paste->prop = ev->property;
			XDeleteProperty(disp, w->xcontext->win, ev->property);
			return;
		}
		panel_replace(paste->panel, paste->at, paste->at, data + off % 4, n - off % 4);
		n -= off % 4;
		off += n;
		paste->at += n;
		paste->panel->p0 = paste->p0;
		paste->panel->p1 = paste->at;
		XFree(data);
	} while (rem > 0);
	paste->panel = NULL;
	paste->at = -1;
	paste->prop = 0;
}

void
win_update(struct Win *win)
{
	if (win->cur_change && win->cur_change != win->file->history.last) {
		struct Change *c = win->cur_change;
		if (!c->next)
			die("window on the wrong branch of history, %p != %p\n", win->cur_change, win->file->history.last);
		do {
			c = c->next;
			node_discard(win->file->doc, c->p0);
			panel_changed(win->body, c);
		} while (c->next);
		if (win->follow)
			win_follow(win, win->cur_change->next, c);
		win->cur_change = c;
		c->final = 1;
		win_tag_rewrite(win);
		win->dirty = 1;
	}
	if (win->resize) {
		win_resize(win);
		win->dirty = 1;
	}
	if (win->dirty)
		win_redraw(win);
	if (!win->file->virtual && !win->file->readonly)
		file_autosave(win->file);
}

void
win_warp(struct Win *w, struct Panel *p, int force)
{
	struct Frag *frag;
	int x, y;
	int64 p0 = p->p0;
	panel_layout(p);
	frag = frag_atpos(p->frags, p0);
	if (!frag)
		return;
	if (frag->p == p0 || !frag->font)
		x = frag->x;
	else if (frag->len == 1)
		x = frag->x + frag->w;
	else {
		int adv = typo_advance(frag->font, frag->attr,
			text_ref(p->txt, frag->p, p0), p0-frag->p);
		x = frag->x + adv;
	}
	x = p->x + PANEL_MARGIN + x + 2;
	y = p->y + frag->y + frag->h;
	XWarpPointer(disp, force ? None : w->xcontext->win, w->xcontext->win, 0, 0, 0, 0, x, y);
}

static int
_remote_search(const char *name, const char *addr, Window root_win, int depth_limit)
{
	Atom type;
	int format;
	unsigned long nws, rem;
	Window *wins;
	int i;
	if (depth_limit == 0)
		return 0;
	XGetWindowProperty(disp, root_win,
		xatom("_NET_CLIENT_LIST"), 0, 128, False, XA_WINDOW,
		&type, &format, &nws, &rem, (unsigned char**) &wins);
	if (type != XA_WINDOW)
		return 0;
	for (i = 0; i < nws; i++) {
		char *winpath;
		unsigned long len;
		if (_remote_search(name, addr, wins[i], depth_limit-1)) {
			XFree(wins);
			return 1;
		}
		XGetWindowProperty(disp, wins[i],
			xatom("_EDITOR_PATH"), 0, 4096, False, XA_STRING,
			&type, &format, &len, &rem, (unsigned char**) &winpath);
		if (type != XA_STRING || rem > 0 || strcmp(name, winpath) != 0) {
			XFree(winpath);
			continue;
		}
		XFree(winpath);
		XChangeProperty(disp, wins[i], xatom("_EDITOR_GOTO"),
			XA_STRING, 8, PropModeReplace,
			(unsigned char*) addr, strlen(addr));
		XFlush(disp);
		XFree(wins);
		return 1;
	}
	XFree(wins);
	return 0;
}

static int
remote(const char *name, const char *addr)
{
	if (!addr)
		addr = "";
	return _remote_search(name, addr, root, 5);
}

int
main(int argc, char *argv[])
{
	struct sigaction sact = {0};
	struct Win *win = NULL;
	struct Proc *client = NULL;
	char namebuf[PATHLEN];
	char **args, *name = NULL;
	int fd = 0;
	
	char *addr = NULL;
	xclass.res_name = strdup("limed");
	xclass.res_class = xclass.res_name;
	
	int8 formatted_in = 0, formatted_out = 0, noremote = 0, pipe = 0, readonly = 0, virtual = 0, foreground = 0;
	int spaces = 0;
	long embed_win = 0;
	const char *script = NULL;
	
	for (args = argv+1; *args && (*args)[0] == '-'; args++) {
		if ((*args)[1] == 0) {
			break;
		} else if (strcmp(*args, "-1") == 0) {
			noremote = foreground = 1;
		} else if (strcmp(*args, "-c") == 0) {
			args++;
			if (client)
				die("client program already specified\n");
			if (!*args) {
				fprintf(stderr, "missing argument for -c\n");
				return 1;
			}
			formatted_in = virtual = 1;
			client = proc_new(*args);
		} else if (strcmp(*args, "-e") == 0) {
			args++;
			if (!*args) {
				fprintf(stderr, "missing script for -e\n");
				return 1;
			}
			script = *args;
		} else if (strcmp(*args, "-f") == 0) {
			formatted_in = formatted_out = 1;
		} else if (strcmp(*args, "-name") == 0) {
			args++;
			if (!*args) {
				fprintf(stderr, "missing argument for -name\n");
				return 1;
			}
			xclass.res_name = *args;
		} else if (strcmp(*args, "-p") == 0) {
			pipe = formatted_in = 1;
		} else if (strcmp(*args, "-s") == 0) {
			spaces = 4;
		} else if (strcmp(*args, "-sh") == 0) {
			const char *shell = getenv("SHELL");
			if (client)
				die("client program already specified\n");
			if (!shell)
				shell = "/bin/sh";
			name = strdup(shell);
			formatted_in = virtual = 1;
			client = proc_new(shell);
			proc_add_arg(client, "-i");
		} else if (strcmp(*args, "-sn") == 0) {
			args++;
			if (!*args) {
				fprintf(stderr, "missing argument for -sn\n");
				return 1;
			}
			spaces = strtol(*args, NULL, 0);
			if (!spaces) {
				fprintf(stderr, "invalid argument for -sn\n");
				return 1;
			}
		} else if (strcmp(*args, "-v") == 0) {
			printf("Version: %s\n", VERSION);
			return 0;
		} else if (strcmp(*args, "-w") == 0) {
			args++;
			if (!*args) {
				fprintf(stderr, "missing argument for -w\n");
				return 1;
			}
			embed_win = strtol(*args, NULL, 0);
			if (!embed_win) {
				fprintf(stderr, "invalid argument for -w\n");
				return 1;
			}
		} else if (strcmp(*args, "-R") == 0) {
			readonly = 1;
		} else if (strcmp(*args, "-V") == 0) {
			virtual = 1;
		} else {
			printf("unrecognized flag %s\n", *args);
			return 1;
		}
	}
	
	if (pipe || virtual) {
		if (*args) {
			name = *args;
			args++;
		} else {
			virtual = 1;
			if (!getcwd(namebuf, sizeof(namebuf)))
				die("can't get current working directory:");
			str_append(namebuf, "/ +++ TMP +++", sizeof(namebuf));
			name = namebuf;
		}
	} else if (*args) {
		const char *ext;
		if ((*args)[0] == '/')
			name = *args;
		else
			name = realpath(*args, NULL);
		if (!name) {
			fprintf(stderr, "can't get absolute path: %s: %s\n", *args, strerror(errno));
			return 1;
		}
		ext = strrchr(name, '.');
		if (ext && strcmp(ext, ".loz") == 0)
			formatted_in = formatted_out = 1;
		args++;
	} else
		return 0;
	
	if (*args)
		addr = *args;
	
	setlocale(LC_CTYPE, "");
	XSetLocaleModifiers("");
	sigemptyset(&sact.sa_mask);
	sact.sa_handler = do_nothing;
	sigaction(SIGCHLD, &sact, NULL);
	sigaction(SIGALRM, &sact, NULL);
	
	/* We need an X connection to look for an open instance.
	 * Everything else gets initialized after we try to open the file, so we can fail as early as possible. */
	if (!(disp = XOpenDisplay(NULL)))
		die("couldn't open display\n");
	root = RootWindow(disp, screen);
	
	if (!noremote && !virtual && remote(name, addr))
		return 0;
	
	if (pipe)
		fd = 0;
	else if (!virtual) {
		fd = open(name, O_RDONLY);
		if (fd < 0) {
			perror("couldn't open file");
			return 1;
		}
		if (access(name, W_OK))
			readonly = 1;
	}
	
	xinit();
	xembed_root = embed_win ? embed_win : root;
	typo_load_colors(colors, SIZE(colors));
	load_fonts();
	XOpenIM(disp, NULL, NULL, NULL);
	
	win = win_new(embed_win);
	win->file = file_new();
	if (client) {
		proc_attach_term(client, win->file);
		proc_setenv(client, "AWN_TERM", "y");
		proc_start(client);
		win->file->client = client;
	} else if (pipe) {
		if (get_file(win->file->txt, fd, !formatted_in))
			die("couldn't read text from pipe:");
		close(fd);
	} else if (!virtual) {
		struct stat stb;
		if (fstat(fd, &stb))
			die("couldn't stat file:");
		win->file->mtime = stb.st_mtim;
		if (get_file(win->file->txt, fd, !formatted_in))
			die("couldn't read file:");
		close(fd);
	}
	
	win->file->readonly = readonly;
	win->file->virtual = virtual;
	win->file->plain = !formatted_out;
	str_copy(win->file->name, name, sizeof(win->file->name));
	
	win_init(win);
	win_show(win, true);
	win->file->save_chg = win->cur_change;
	text_append(win->tag->txt, win_new_tag_text, strlen(win_new_tag_text));
	win->tag->p0 = win->tag->p1 = text_len(win->tag->txt);
	win->body->indent_spaces = spaces;
	
	if (addr && panel_searchaddr(win->body, 0, 0, addr, strlen(addr)))
		panel_focus(win->body, win->body->p0);
	
	lua_init();
	if (script)
		lua_run(win, script);
	
	if (!foreground) {
		if (fork() > 0)
			return 0;
		setsid();
	}
	
	while (windows) {
		struct Win **nw;
		int xfd = ConnectionNumber(disp);
		struct pollfd fds[1024];
		int i = 0, n;
		i = proc_pollfds(fds, SIZE(fds)-1);
		fds[i].fd = xfd;
		fds[i].events = POLLIN;
		fds[i].revents = 0;
		i++;
		if (XPending(disp))
			n = poll(fds, i, 0);
		else
			n = poll(fds, i, -1);
		if (n < 0 && errno != EINTR)
			die("poll failed:");
		xevents();
		proc_handleall(fds, i);
		
		nw = &windows;
		while (*nw) {
			win = *nw;
			if (win->deleted) {
				*nw = win->next;
				win_free(win);
			} else {
				nw = &win->next;
			}
		}
		for (win = windows; win; win = win->next)
			win_update(win);
	}
	XCloseDisplay(disp);
	return 0;
}

Atom
xatom(const char *name)
{
	struct XAtomCache { Atom a; const char *n; };
	static struct XAtomCache atoms[32] = {{0,NULL}};
	static int natoms = 0;
	int i;
	for (i = 0; i < natoms; i++) {
		if (strcmp(atoms[i].n, name) == 0)
			return atoms[i].a;
		if (i >= SIZE(atoms)-1)
			return XInternAtom(disp, name, False);
	}
	atoms[i].a = XInternAtom(disp, name, False);
	atoms[i].n = name;
	natoms++;
	return atoms[i].a;
}

void
xevents(void)
{
	XEvent ev;
	int i, en = XPending(disp);
	struct Win *win;
	for (i = 0; i < en; i++) {
		XNextEvent(disp, &ev);
		if (XFilterEvent(&ev, None))
			continue;
		if (ev.xany.window == cutwin) {
			switch (ev.type) {
			case SelectionClear:
				xsel_clear(ev.xselectionclear.selection);
				break;
			case SelectionRequest:
				xselreq(&ev.xselectionrequest);
				break;
			default:
				break;
			}
			continue;
		}
		win = findwin(ev.xany.window);
		if (!win)
			continue;
		switch (ev.type) {
		case ButtonPress:
		case ButtonRelease:
			win_button(win, &ev.xbutton);
			break;
		case ClientMessage:
			if (ev.xclient.data.l[0] == xatom("WM_DELETE_WINDOW"))
				win_del(win, win->del_confirm);
			break;
		case ConfigureNotify:
			if (win->w == ev.xconfigure.width && win->h == ev.xconfigure.height)
				break;
			win->w = ev.xconfigure.width;
			win->h = ev.xconfigure.height;
			win->resize = 1;
			break;
		case EnterNotify:
			win_enter(win, &ev.xfocus);
			break;
		case Expose:
			win->dirty = 1;
			break;
		case FocusIn:
			win_focus_in(win, &ev.xfocus);
			break;
		case FocusOut:
			win_focus_out(win, &ev.xfocus);
			break;
		case KeyPress:
		case KeyRelease:
			win_key(win, &ev.xkey);
			break;
		case MotionNotify:
			win_motion(win, &ev.xmotion);
			break;
		case PropertyNotify:
			win_ui_propnotify(win, &ev.xproperty);
			break;
		case SelectionNotify:
			win_ui_selnotify(win, &ev.xselection);
			break;
		default:
			break;
		}
	}
}

void
do_nothing(int sn)
{
}

int
xinit(void)
{
	screen = DefaultScreen(disp);
	if (fcntl(ConnectionNumber(disp), F_SETFD, O_CLOEXEC) < 0)
		die("can't set CLOEXEC on X connection:");
	
	cutwin = XCreateWindow(disp, root, 0, 0, 1, 1, 0,
		0, InputOnly, DefaultVisual(disp, screen), 0, NULL);
	
	xim_open(disp, xclass.res_name, xclass.res_class);
	return 0;
}

void
xselreq(XSelectionRequestEvent *ev)
{
	XSelectionEvent resp;
	resp.type = SelectionNotify;
	resp.requestor = ev->requestor;
	resp.selection = ev->selection;
	resp.target = ev->target;
	resp.time = ev->time;
	resp.property = xsel_request_respond(&resp, ev);
	XSendEvent(disp, resp.requestor, True, NoEventMask, (XEvent*) &resp);
}

void
xsel_clear(Atom sel)
{
	if (sel == XA_PRIMARY) {
		primary_selection_panel = NULL;
	} else if (sel == xatom("CLIPBOARD")) {
		cutlen = 0;
		free(cutbuf);
		cutbuf = NULL;
	}
}

Atom
xsel_request_respond(XSelectionEvent *resp, XSelectionRequestEvent *ev)
{
	Atom dst_prop = ev->property != None ? ev->property : ev->target;
	Atom sel = ev->selection, type = ev->target;
	Atom clipboard = xatom("CLIPBOARD");
	Atom ustring = xatom("UTF8_STRING");
	if (sel != clipboard && sel != XA_PRIMARY)
		return None;
	if (type == xatom("TARGETS")) {
			XChangeProperty(disp, resp->requestor, dst_prop,
				XA_ATOM, 32, PropModeReplace,
				(unsigned char*) &ustring, 1);
			return dst_prop;
	}
	
	if (type != ustring)
		return None;
	
	if (ev->selection == XA_PRIMARY) {
		char buf[8192];
		struct Panel *p = primary_selection_panel;
		int64 len;
		if (!p)
			return None;
		resp->property = ev->property;
		len = p->p1 - p->p0;
		if (len > sizeof(buf))
			return None;
		text_read(p->txt, p->p0, buf, len);
		XChangeProperty(disp, resp->requestor, dst_prop,
			ustring, 8, PropModeReplace,
			(unsigned char*) buf, len);
		return dst_prop;
	}
	if (ev->selection == clipboard) {
		if (!cutbuf)
			return None;
		resp->property = ev->property;
		XChangeProperty(disp, resp->requestor, dst_prop,
			ustring, 8, PropModeReplace,
			(unsigned char*) cutbuf, cutlen);
		return dst_prop;
	}
	return None;
}
