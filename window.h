typedef struct MarkStack MarkStack;

struct Win {
	struct File *file;
	struct Change *cur_change;
	
	struct Panel *tag, *body, *focus;
	struct ScrollBar *scroll;
	
	int	w, h;
	bool	resize, dirty, deleted;
	int	mgrab;	/* mouse grab - UI_* element currently receiving pointer drag events */
	int	pointerx, pointery;
	
	int64	last_focused;	/* timestamp when last focused */
	bool	warp;
	
	bool	collapsed;
	bool	del_confirm, put_confirm;
	bool	follow;
	int	margin;
	
	int64	marks[16];
	int	mark_idx;;
	
	struct XContext *xcontext;
	
	struct Win *master, *next, *clones;
};

void	win_collapse(struct Win*, int);
void	win_del(struct Win*, bool force);
void	win_exec(struct Win*, const char*);
struct Win*	win_find_file(struct File*);
void	win_popup_init(struct Win*);
struct Win*	win_popup_new(struct Win*, const char *name, const char*, int);
void	win_popup_warning(struct Win*, const char*, int);
void	win_shrink(struct Win*);
void	win_warp(struct Win*, struct Panel*, int force);
